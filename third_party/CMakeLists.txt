if (NOX_RESOURCE_JSON)
	if (NOX_BUILD_SHARED)
		set(JSONCPP_LIB_BUILD_SHARED ON CACHE BOOL "Enable jsoncpp shared library" FORCE)
	else ()
		set(JSONCPP_LIB_BUILD_SHARED OFF CACHE BOOL "Enable jsoncpp shared library" FORCE)
	endif ()
	set(JSONCPP_WITH_TESTS OFF CACHE BOOL "Enable jsoncpp tests" FORCE)
	set(JSONCPP_WITH_POST_BUILD_UNITTEST OFF CACHE BOOL "Enable jsoncpp post build unit tests" FORCE)
	set(JSONCPP_WITH_PKGCONFIG_SUPPORT OFF CACHE BOOL "Enable jsoncpp pkgconfig support" FORCE)

	add_subdirectory(jsoncpp)
endif ()

set(POLY2TRI_DIR poly2tri/poly2tri)

set(POLY2TRI_HEADERS
	${POLY2TRI_DIR}/poly2tri.h
	${POLY2TRI_DIR}/common/shapes.h
	${POLY2TRI_DIR}/common/utils.h
	${POLY2TRI_DIR}/sweep/cdt.h
	${POLY2TRI_DIR}/sweep/advancing_front.h
	${POLY2TRI_DIR}/sweep/sweep_context.h
	${POLY2TRI_DIR}/sweep/sweep.h
)

set(POLY2TRI_SOURCES
	${POLY2TRI_DIR}/common/shapes.cc
	${POLY2TRI_DIR}/sweep/cdt.cc
	${POLY2TRI_DIR}/sweep/advancing_front.cc
	${POLY2TRI_DIR}/sweep/sweep_context.cc
	${POLY2TRI_DIR}/sweep/sweep.cc
)

if (NOX_BUILD_SHARED)
	add_library(poly2tri-shared SHARED ${POLY2TRI_HEADERS} ${POLY2TRI_SOURCES})
	target_include_directories(poly2tri-shared PUBLIC ${POLY2TRI_DIR})
	set_target_properties(poly2tri-shared PROPERTIES
		OUTPUT_NAME poly2tri
	)
endif ()

if (NOX_BUILD_STATIC)
	add_library(poly2tri-static STATIC ${POLY2TRI_HEADERS} ${POLY2TRI_SOURCES})
	target_include_directories(poly2tri-static PUBLIC ${POLY2TRI_DIR})
	set_target_properties(poly2tri-static PROPERTIES
		OUTPUT_NAME poly2tri
	)
endif ()

if (NOX_BUILD_TEST)
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
	
	add_subdirectory(gtest)
	target_include_directories(gtest PUBLIC "gtest/include")
endif ()

if (NOX_PHYSICS_BOX2D)
	if (NOX_BUILD_SHARED)
		set(BOX2D_BUILD_SHARED ON CACHE BOOL "" FORCE)
	else ()
		set(BOX2D_BUILD_SHARED OFF CACHE BOOL "" FORCE)
	endif ()

	if (NOX_BUILD_STATIC)
		set(BOX2D_BUILD_STATIC ON CACHE BOOL "" FORCE)
	else ()
		set(BOX2D_BUILD_STATIC OFF CACHE BOOL "" FORCE)
	endif ()

	set(BOX2D_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
	set(BOX2D_INSTALL OFF CACHE BOOL "" FORCE)

	add_subdirectory(box2d/Box2D)

	if (NOX_BUILD_SHARED)
		target_include_directories(Box2D_shared PUBLIC "box2d/Box2D")
		set_target_properties(Box2D_shared PROPERTIES
			OUTPUT_NAME Box2D
		)
	endif ()
	if (NOX_BUILD_STATIC)
		target_include_directories(Box2D PUBLIC "box2d/Box2D")
		set_target_properties(Box2D PROPERTIES
			OUTPUT_NAME Box2D
		)
	endif ()
endif ()


if (NOX_PHYSICS_BULLET)
	if (NOX_BUILD_SHARED)
		set(BUILD_SHARED_LIBS ON CACHE BOOL "" FORCE)
	else ()
		set(BUILD_SHARED_LIBS OFF CACHE BOOL "" FORCE)
	endif ()

	set(BUILD_BULLET2_DEMOS OFF CACHE BOOL "" FORCE)
	set(BUILD_CPU_DEMOS OFF CACHE BOOL "" FORCE)
	set(BUILD_OPENGL3_DEMOS OFF CACHE BOOL "" FORCE)
	set(BUILD_UNIT_TESTS OFF CACHE BOOL "" FORCE)
	#set(BUILD_EXSTRAS OFF CACHE BOOL "" FORCE)
	set(USE_MSVC_RUNTIME_LIBRARY_DLL ON CACHE BOOL "" FORCE)

	add_subdirectory(bullet3)

	if (NOX_BUILD_SHARED)
	#TODO: Update later
		target_include_directories(Bullet2FileLoader_shared PUBLIC "bullet3/src")
		set_target_properties(Bullet2FileLoader_shared PROPERTIES
			OUTPUT_NAME Bullet2FileLoader
		)
	endif ()
	if (NOX_BUILD_STATIC)
		target_include_directories(Bullet2FileLoader PUBLIC "bullet3/src")
		target_include_directories(Bullet3Collision PUBLIC "bullet3/src")
		target_include_directories(Bullet3Common PUBLIC "bullet3/src")
		target_include_directories(Bullet3Dynamics PUBLIC "bullet3/src")
		target_include_directories(Bullet3Geometry PUBLIC "bullet3/src")
		target_include_directories(Bullet3OpenCL_clew PUBLIC "bullet3/src")
		target_include_directories(BulletCollision PUBLIC "bullet3/src")
		target_include_directories(BulletDynamics PUBLIC "bullet3/src")
		target_include_directories(BulletFileLoader PUBLIC "bullet3/Extras")
		target_include_directories(BulletSoftBody PUBLIC "bullet3/src")
		target_include_directories(BulletWorldImporter PUBLIC "bullet3/Extras")
		target_include_directories(BulletXmlWorldImporter PUBLIC "bullet3/Extras")
		target_include_directories(ConvexDecomposition PUBLIC "bullet3/Extras")
		target_include_directories(GIMPACTUtils PUBLIC "bullet3/Extras")
		target_include_directories(LinearMath PUBLIC "bullet3/src")
		
		set_target_properties(Bullet2FileLoader PROPERTIES OUTPUT_NAME Bullet2FileLoader )
		set_target_properties(Bullet3Collision PROPERTIES OUTPUT_NAME Bullet3Collision )
		set_target_properties(Bullet3Common PROPERTIES OUTPUT_NAME Bullet3Common )
		set_target_properties(Bullet3Dynamics PROPERTIES OUTPUT_NAME Bullet3Dynamics )
		set_target_properties(Bullet3Geometry PROPERTIES OUTPUT_NAME Bullet3Geometry )
		set_target_properties(Bullet3OpenCL_clew PROPERTIES OUTPUT_NAME Bullet3OpenCL_clew )
		set_target_properties(BulletCollision PROPERTIES OUTPUT_NAME BulletCollision )
		set_target_properties(BulletDynamics PROPERTIES OUTPUT_NAME BulletDynamics )
		set_target_properties(BulletFileLoader PROPERTIES OUTPUT_NAME BulletFileLoader )
		set_target_properties(BulletSoftBody PROPERTIES OUTPUT_NAME BulletSoftBody )
		set_target_properties(BulletWorldImporter PROPERTIES OUTPUT_NAME BulletWorldImporter )
		set_target_properties(BulletXmlWorldImporter PROPERTIES OUTPUT_NAME BulletXmlWorldImporter )
		set_target_properties(ConvexDecomposition PROPERTIES OUTPUT_NAME ConvexDecomposition )
		set_target_properties(GIMPACTUtils PROPERTIES OUTPUT_NAME GIMPACTUtils )
		set_target_properties(LinearMath PROPERTIES OUTPUT_NAME LinearMath )
	endif ()
endif ()


if (NOX_ASSET_LOADER)
	
	set(BUILD_SHARED_LIBS ON CACHE BOOL "" FORCE)

	set(ASSIMP_BUILD_SAMPLES OFF CACHE BOOL "" FORCE)
	set(ASSIMP_BUILD_TESTS OFF CACHE BOOL "" FORCE)
	set(ASSIMP_BUILD_ASSIMP_TOOLS OFF CACHE BOOL "" FORCE)
	
	# TODO: fix boost
	set(ASSIMP_ENABLE_BOOST_WORKAROUND ON CACHE BOOL "" FORCE)
	
	add_subdirectory(assimp)
	target_include_directories(assimp PUBLIC "assimp/include")
	set_target_properties(assimp PROPERTIES OUTPUT_NAME assimp)
	
endif ()

add_subdirectory(cppformat)
target_include_directories(format PUBLIC "cppformat")

