/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <nox/util/process/Manager.h>
#include <nox/app/IContext.h>
#include <nox/app/Application.h>

#include <nox/logic/Logic.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/physics/box2d/BulletSimulation3d.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/timeManipulation/TimeManipulationManager3d.h>
#include <nox/logic/world/timeManipulation/DefaultITimeConflictSolver3d.h>
#include <nox/logic/world/timeManipulation/DefaultIWolrdLogger3d.h>

#include <nox/app/storage/DataStorageBoost.h>

#include <memory>
#include <string>


class ApplicationTest : public nox::app::Application
{
public:
	ApplicationTest() :
		Application("Test", "Noxplus")
	{}

	nox::logic::world::timemanipulation::TimeManipulationManager3d* timeManager;
	nox::logic::world::timemanipulation::DefaultWorldLogger3d* wolrdLogger;
	nox::logic::Logic* logic;
	nox::process::Manager processManagerTwo;


private:
	bool onInit() override;
};

bool ApplicationTest::onInit()
{
	auto logic = std::make_unique<nox::logic::Logic>();
	this->logic = logic.get();

	this->logic->setContext(this);
	this->processManagerTwo.startProcess(std::move(logic));

	auto physics = std::make_unique<nox::logic::physics::BulletSimulation3d>(this->logic);
	physics->setLogger(this->createLogger());
	this->logic->setPhysics(std::move(physics));

	auto worldHandler = std::make_unique<nox::logic::world::Manager>(this->logic);
	this->logic->setWorldManager(std::move(worldHandler));

	auto dataStorage = std::make_unique<nox::app::storage::DataStorageBoost>();

	auto worldLogger = std::make_unique<nox::logic::world::timemanipulation::DefaultWorldLogger3d>(this->logic->getWorldManager(), dataStorage.get(), this->logic);
	this->wolrdLogger = worldLogger.get();
	this->logic->getPhysics3d()->setWorldLogger(worldLogger.get());

	auto conflictSolver = std::make_unique<nox::logic::world::timemanipulation::DefaultTimeConflictSolver3d>();

	auto timeManager = std::make_unique<nox::logic::world::timemanipulation::TimeManipulationManager3d>(std::move(worldLogger), std::move(conflictSolver));
	this->timeManager = timeManager.get();
	this->logic->setTimeManager(std::move(timeManager));

	return true;
}

class TestTimeManipulation: public ::testing::Test
{
protected:
	virtual void SetUp()
	{
		this->application = std::make_unique<ApplicationTest>();
		this->application->init(0, nullptr);
	}

	virtual void TearDown()
	{
		this->application->shutdown();
	}

	std::unique_ptr<nox::logic::Logic> logic;
	std::unique_ptr<ApplicationTest> application;
};

TEST_F(TestTimeManipulation, TestFrameCounter)
{
	this->application->timeManager->play();
	this->application->timeManager->setPause(false);

	while (true)
	{
		int i2 = this->application->wolrdLogger->getNumberOfFrames();
		this->application->processManagerTwo.updateProcesses(nox::Duration(50));
		int i1 = this->application->wolrdLogger->getNumberOfFrames();

		int i = 0;
	}


}

