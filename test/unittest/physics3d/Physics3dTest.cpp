/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <gtest/gtest.h>

#include <nox/app/IContext.h>
#include <nox/app/Application.h>

#include <nox/logic/Logic.h>
#include <nox/logic/physics/box2d/BulletSimulation3d.h>
#include <nox/logic/world/Manager.h>

#include <nox/logic/physics/physics_utils.h>

#include <nox/logic/actor/Actor.h>
#include <memory>
#include <string>



class Physics3dTest: public ::testing::Test
{

protected:
	virtual void SetUp()
	{
		this->logic = std::make_unique<nox::logic::Logic>();

		auto physics = std::make_unique<nox::logic::physics::BulletSimulation3d>(this->logic.get());
		this->physics = physics.get();
		this->logic->setPhysics(std::move(physics));

		auto manager = std::make_unique< nox::logic::world::Manager >(this->logic.get());
		this->worldManager = manager.get();
		
		this->logic->setWorldManager(std::move(manager));
	}

	virtual void TearDown()
	{
		this->logic.reset();
	}

	std::unique_ptr<nox::logic::Logic> logic;
	nox::logic::physics::BulletSimulation3d* physics;
	nox::logic::world::Manager * worldManager;
};

TEST_F(Physics3dTest, TestCreateShape)
{

	nox::logic::physics::BulletBodyDefinition bodyDefinition;
	bodyDefinition.shape.type = nox::logic::physics::ShapeType3d::NONE;
	bodyDefinition.shape.size = glm::vec3(2, 2, 2);
	ASSERT_TRUE(physics->createShape(bodyDefinition, nullptr) != nullptr);
}

//createActorBody(actor::Actor* actor, const  BulletBodyDefinition& bodyDefinition)

TEST_F(Physics3dTest, TestCreateActorBody)
{

	nox::logic::actor::Actor * actor = new nox::logic::actor::Actor(logic.get(), nox::logic::actor::Identifier(1), std::string("TestActor"));
	nox::logic::physics::BulletBodyDefinition bodyDefinition;
	bodyDefinition.shape.type = nox::logic::physics::ShapeType3d::BOX;
	bodyDefinition.shape.size = glm::vec3(2, 2, 2);
	bodyDefinition.mass = 5;
	bodyDefinition.bodyType = nox::logic::physics::PhysicalBodyType::DYNAMIC;
	ASSERT_TRUE(physics->createActorBody(actor, bodyDefinition));
}

TEST_F(Physics3dTest, TestChangephysicProperties)
{
	
	nox::logic::actor::Actor * actor = new nox::logic::actor::Actor(logic.get(), nox::logic::actor::Identifier(1), std::string("TestActor"));
	nox::logic::physics::BulletBodyDefinition bodyDefinition;
	bodyDefinition.shape.type = nox::logic::physics::ShapeType3d::BOX;
	bodyDefinition.shape.size = glm::vec3(2, 2, 2);
	bodyDefinition.mass = 5;
	bodyDefinition.bodyType = nox::logic::physics::PhysicalBodyType::DYNAMIC;
	physics->createActorBody(actor, bodyDefinition);


	glm::vec3 forcesBeforeChange = physics->getTotalForces(nox::logic::actor::Identifier(1));
	physics->applyCentralForce(nox::logic::actor::Identifier(1), glm::vec3(100, 100, 100));


	ASSERT_TRUE(physics->getTotalForces(nox::logic::actor::Identifier(1)) == glm::vec3(100, 100, 100));
}