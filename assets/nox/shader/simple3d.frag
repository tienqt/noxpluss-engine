//? required 130

in vec4 outVertex;
in vec4 outNormal;
in vec4 outTangent;
in vec4 outColor;
in vec2 outUv;

uniform vec3 lightPos;
uniform vec3 cameraPos;


uniform sampler2D texUnit;
uniform int numTextures;

out vec4 output_color;

void main() 
{	

	vec4 textureColor = texture(texUnit, outUv);

	if(numTextures == 0)
	{
		output_color = outColor;
	}
	else
	{
		output_color = textureColor;
	}	
}
