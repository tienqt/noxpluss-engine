//? required 130

in vec3 vertex;
in vec3 vertexColor;

uniform mat4 mvpMatrix;

out vec3 vColor;

void main()
{
	vColor = vertexColor;
	gl_Position = mvpMatrix * vec4(vertex, 1.0);
}
