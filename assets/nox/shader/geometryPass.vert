//? required 330
//Reused code from: http://ogldev.atspace.co.uk/

layout (location = 0) in vec3 vertex;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec4 color;
layout (location = 3) in vec2 uv; 
layout (location = 4) in ivec4 boneIDs;
layout (location = 5) in vec4 weights;

uniform mat4 mvpMatrix;
uniform mat4 modelMatrix;
uniform mat4 bones[100];

uniform int numTextures;
uniform bool hasAnimation;

out vec2 TexCoord0; 
out vec4 Normal0; 
out vec4 WorldPos0; 
out vec4 Color0;

void main()
{ 
	mat4 boneTransform = mat4(1.0);
	
	if(hasAnimation)
	{
		boneTransform = bones[boneIDs[0]] *  weights[0];
		boneTransform += bones[boneIDs[1]] * weights[1];
		boneTransform += bones[boneIDs[2]] * weights[2];
		boneTransform += bones[boneIDs[3]] * weights[3];
	}
	
	vec4 skinVertex	= boneTransform * vec4(vertex, 1.0);
    gl_Position  	= mvpMatrix * skinVertex;
    TexCoord0    	= uv; 
	vec4 skinNormal	= boneTransform * vec4(normal, 0.0);
    Normal0      	= (modelMatrix * skinNormal);
    WorldPos0    	= (modelMatrix * skinVertex); 
	Color0       	= color;
}