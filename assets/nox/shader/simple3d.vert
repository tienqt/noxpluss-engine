//? required 130

in vec4 vertex;
in vec4 normal;
in vec4 tangent;
in vec4 color;
in vec2 uv;

uniform mat4 mvpMatrix;
uniform vec3 lightPos;
uniform vec3 cameraPos;

// Send this to fragment shader:
out vec4 outVertex;
out vec4 outNormal;
out vec4 outTangent;
out vec4 outColor;
out vec2 outUv;


void main()
{
	gl_Position = mvpMatrix * vertex;
	
	outNormal = normal;
	outTangent = tangent;
	outColor = color;
	outUv = uv;
	
}

// https://www.youtube.com/watch?v=ClqnhYAYtcY  (1:18:00)