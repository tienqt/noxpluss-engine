//? required 330
//Reused code from: http://ogldev.atspace.co.uk/

in vec2 TexCoord0;
in vec4 Normal0;
in vec4 WorldPos0;
in vec4 Color0;

layout (location = 0) out vec4 WorldPosOut; 
layout (location = 1) out vec4 DiffuseOut; 
layout (location = 2) out vec4 NormalOut; 
layout (location = 3) out vec4 TexCoordOut; 

uniform sampler2D texUnit;
uniform int numTextures;
uniform vec3 overlayColor;

void main()	
{	
    WorldPosOut = WorldPos0;
	
	if(numTextures > 0)
		DiffuseOut = texture(texUnit, TexCoord0);
	else
		DiffuseOut = Color0;
		
	DiffuseOut = DiffuseOut + vec4(overlayColor, 0.0);
		
	NormalOut = normalize(Normal0);	
    
	TexCoordOut = vec4(TexCoord0, 0.0, 1.0);	
}
