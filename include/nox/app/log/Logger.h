/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_LOG_LOGGER_H_
#define NOX_APP_LOG_LOGGER_H_

#include <nox/app/log/Message.h>
#include <nox/common/api.h>

#include <format.h>
#include <string>

namespace nox { namespace app { namespace log {

class OutputManager;
class LogClient;

class NOX_API Logger
{
public:
	class NOX_API Output
	{
	public:
		static const std::size_t MAX_FORMAT_MESSAGE_SIZE;

		Output(const std::string& loggerName, OutputManager* manager, const Message::Level logLevel);

		/**
		 * Log a message as a raw string.
		 *
		 * @param string String to log.
		 */
		void raw(const std::string& string);

		void raw(const std::wstring& string);

		/**
		 * Log a message as a formatted string using printf style formatting.
		 *
		 * POSIX extension for positional arguments is supported.
		 *
		 * See http://cppformat.readthedocs.org/en/stable/reference.html#printf-formatting-functions
		 * for more information about formatting.
		 *
		 * @param format Format for string. See printf/cppformat documentation.
		 * @param args Arguments to the format string. See printf/cppformat documentation.
		 */
		template<class String, class... Args>
		void format(String&& format, Args&&... args);

	private:
		std::string loggerName;
		OutputManager* outputManager;
		Message::Level logLevel;
	};

	Logger();
	Logger(OutputManager* manager);
	Logger(const std::string& name, OutputManager* manager);

	Logger(Logger&& other);
	Logger& operator=(Logger&& other);

	Logger(const Logger& other) = delete;
	Logger& operator=(const Logger& other) = delete;

	void setName(const std::string& name);
	void setOutputManager(OutputManager* manager);

	Output info();
	Output verbose();
	Output warning();
	Output error();
	Output fatal();
	Output debug();

private:
	std::string name;
	OutputManager* outputManager;
};

namespace detail {

FMT_VARIADIC_W(std::wstring, sprintf, fmt::WStringRef)

inline std::wstring sprintf(fmt::WStringRef format, fmt::ArgList args) {
	fmt::WMemoryWriter w;
	printf(w, format, args);
	return w.str();
}

}

template<class String, class... Args>
inline void Logger::Output::format(String&& format, Args&&... args)
{
	using detail::sprintf;
	using fmt::sprintf;

	this->raw(sprintf(std::forward<String>(format), std::forward<Args>(args)...));
}

} } }

#endif
