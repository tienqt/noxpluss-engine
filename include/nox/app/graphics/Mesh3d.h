/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef NOX_APP_GRAPHICS_MESH3D_H
#define NOX_APP_GRAPHICS_MESH3D_H

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>
#include <glm/gtx/transform.hpp>

#include <nox/app/graphics/RenderData.h>
#include <nox/app/graphics/ITexture3d.h>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define INVALID_MATERIAL 0xFFFFFFFF
#define NUM_BONES_PER_VERTEX 4

namespace nox { namespace app { namespace graphics {

class Mesh3d
{
public:
	Mesh3d();

	~Mesh3d();

	/**
	*   Loads the mesh datas from a file after it has been created. 
	*/
	bool loadMesh(const std::string& Filename);


	//void render(const RenderData* renderData);
	void render(RenderData& renderData);

	int getNumBones() const
	{
		return numBones;
	}

	/**
	*   Used for stepping the animation forward
	*   @param animationIndex The index of the animation
	*   @param timeInSeconds should increase each time this function is called
	*   @param transforms the set of bone transformation matrixes to update
	*/
	void boneTransform(unsigned int animationIndex, float timeInSeconds, std::vector<glm::mat4>& Transforms);


	std::vector<glm::vec3> & getVertices();
	std::vector<unsigned int> getIndices();

	/**
	*   @return number of animations for this mesh/scene.
	*/
	int numAnimations();

	/**
	*   Buffers latest vertexpositions to OpenGL. Called after vertex data is changed.
	*   @return false if OpenGL didn't like it
	*/
	bool updateVertices();


	/**
	*  Calculates a set of bone transoformations for each animation in the mesh.
	*  The generated data is accessed using boneTransform(..).
	*  @param framesPerSecond Number of frames to generate per aimation-time-second
	*/
	void generateAnimationFrames(float framesPerSecond = 60);

	//float getAnimatonDuration(unsigned int animationIndex);
	int getAnimationIndexByName(std::string name);
	std::string getAnimationNameByIndex(unsigned int i);
	std::string getMeshName();

private:


	struct BoneInfo
	{
		glm::mat4 boneOffset;
		glm::mat4 finalTransformation;

		BoneInfo()
		{
			boneOffset = glm::mat4(0);
			finalTransformation = glm::mat4(0);
		}
	};

	struct VertexBoneData
	{
		unsigned int ids[NUM_BONES_PER_VERTEX];
		float weights[NUM_BONES_PER_VERTEX];

		VertexBoneData()
		{
			reset();
		};

		void reset()
		{
			memset(ids, 0, sizeof(ids));
			memset(weights, 0, sizeof(weights));
		}

		void addBoneData(unsigned int boneId, float weights);
	};

	glm::mat4 mulitplyMatrix(const glm::mat4& m, glm::mat4& right);
	void calcInterpolatedScaling(aiVector3D& out, float animationTime, const aiNodeAnim* nodeAnim);
	void calcInterpolatedRotation(aiQuaternion& out, float animationTime, const aiNodeAnim* nodeAnim);
	void calcInterpolatedPosition(aiVector3D& out, float animationTime, const aiNodeAnim* nodeAnim);
	int findScaling(float animationTime, const aiNodeAnim* nodeAnim);
	int findRotation(float animationTime, const aiNodeAnim* nodeAnim);
	int findPosition(float animationTime, const aiNodeAnim* nodeAnim);
	const aiNodeAnim* findNodeAnim(const aiAnimation* animation, const std::string nodeName);
	void readNodeHeirarchy(float animationTime, aiAnimation * animation, const aiNode* node, const glm::mat4& parentTransform);
	bool initFromScene(const aiScene* scene, const std::string& filename);
	void initMesh(int meshIndex,
		const aiMesh* paiMesh,
		std::vector<glm::vec3>& positions,
		std::vector<glm::vec3>& normals,
		std::vector<glm::vec4>& colors,
		std::vector<glm::vec2>& texCoords,
		std::vector<VertexBoneData>& bones,
		std::vector<unsigned int>& indices);
	void loadBones(int meshIndex, const aiMesh* paiMesh, std::vector<VertexBoneData>& bones);
	bool initMaterials(const aiScene* scene, const std::string& filename);
	void clear();

	enum VB_TYPES {
		INDEX_BUFFER,
		POS_VB,
		NORMAL_VB,
		COLOR_VB,
		TEXCOORD_VB,
		BONE_VB,
		NUM_VBs
	};

	GLuint VAO;
	GLuint buffers[NUM_VBs];

	struct MeshEntry {
		MeshEntry()
		{
			numIndices = 0;
			baseVertex = 0;
			baseIndex = 0;
			materialIndex = INVALID_MATERIAL;
		}

		unsigned int numIndices;
		unsigned int baseVertex;
		unsigned int baseIndex;
		unsigned int materialIndex;
	};

	std::vector<glm::vec3> positions;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec4> colors;
	std::vector<glm::vec2> texCoords;
	std::vector<VertexBoneData> bones;
	std::vector<unsigned int> indices;

	std::vector<MeshEntry> entries;
	std::vector<std::shared_ptr<ITexture3d>> textures;

	std::map<std::string, int> boneMapping; // maps a bone name to its index
	int numBones;
	std::vector<BoneInfo> boneInfo;
	glm::mat4 globalInverseTransform;

	const aiScene * meshAiScene;
	Assimp::Importer importer;
	
	glm::mat4 & convertToGlmMatrix(const aiMatrix4x4 m);
	glm::mat4 & convertToGlmMatrix(const aiMatrix3x3 m);

	std::vector<unsigned int> allIndices;

	GLuint boneLocation[100];

	unsigned int numIndx;

	//aiAnimation * currentAnimation;
	

	std::string fileName;

	std::map<aiAnimation*, std::vector<std::vector<glm::mat4>>> animationBoneFrames;
	float animationFramesPerSecond;
};

} } }
#endif
