/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//Reused code from: http://ogldev.atspace.co.uk/

#ifndef NOX_APP_GRAPHICS_POINTLIGHT3D_H_
#define NOX_APP_GRAPHICS_POINTLIGHT3D_H_

#include <nox/app/graphics/BaseLight3d.h>
#include <nox/app/graphics/Mesh3d.h>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <memory>

namespace nox { namespace app { namespace graphics
{

//	class Mesh3d;
class RenderData;

class PointLight3d : public BaseLight3d
{
public:
	PointLight3d();
	PointLight3d(std::shared_ptr<Mesh3d> sphereLightVolume, glm::vec3 color = glm::vec3(1), float ambientIntensity = 1, float diffuseIntensity = 1,
		glm::vec3 position = glm::vec3(), float constantAttenuation = 0, float linearAttenuation = 1, float exponentialAttenuation = 0);
	virtual ~PointLight3d();


	virtual void init(GLuint shaderProgram, GLuint stenchilProgram) override;
	virtual void render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection) override;
	virtual void render(RenderData* renderData, const glm::mat4x4& mvp) override;

	glm::vec3 getPosition();
	float getConstantAttention();
	float getLinearAttention();
	float getExponentialAttention();

	void setColor(glm::vec3 color) override;
	void setPosition(glm::vec3 position);
	void setConstantAttention(float constantAttention);
	void setLinearAttention(float linearAttention);
	void setExponentialAttention(float exponentialAttention);
	

protected:
	float calculateLightVolumeRadiuse();


	glm::vec3 position;
	float constantAttenuation;
	float linearAttenuation;
	float exponentialAttenuation;

	float sphereRadiuse;

//	std::shared_ptr<Mesh3d> sphereLightVolume;

	GLuint pointLight_base_color;
	GLuint pointLight_base_ambientIntensity;
	GLuint pointLight_base_diffuseIntensity;
	GLuint pointLight_position;
	GLuint pointLight_base_atten_constant;
	GLuint pointLight_base_atten_linear;
	GLuint pointLight_base_atten_exp;

	GLuint eyeWorldPos;
	GLuint specularIntensity;
	GLuint specularPower;
	GLuint lightType;
	GLuint screenSize;

	GLuint posSampler;
	GLuint colorSampler;
	GLuint normalSampler;

	GLuint mvpHandle;
	GLuint stencilMvpHandle;
	

};

}
} }

#endif
