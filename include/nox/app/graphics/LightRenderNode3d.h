/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_LIGHTRENDERNODE3D_H_
#define NOX_APP_GRAPHICS_LIGHTRENDERNODE3D_H_

#include <nox/logic/graphics/actor/ActorLight3d.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/app/graphics/BaseLight3d.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>

namespace nox { namespace app { namespace graphics
{

class IRenderer3d;

/**
 *  Gets the quad containing information about the transformed objects
 *  vertices and puts it in the correct level and textureAtlas in the array.
 */
class LightRenderNode3d : public SceneGraphNode
{
public:
	LightRenderNode3d();
	void setActorTransform(nox::logic::actor::Transform3d* actorTransform);
	void setLight(std::shared_ptr<BaseLight3d>& light, int type);

	std::shared_ptr<BaseLight3d>& getLight();
	int getType();

	void updatePosition();
		
private:
	void onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix) override;
	void onAttachToRenderer(IRenderer3d* renderer) override;
	void onDetachedFromRenderer() override;

	nox::logic::actor::Transform3d* actorTransform;
	std::shared_ptr<nox::app::graphics::BaseLight3d> light;
	int type;
	bool attachedToRender;

	
};

}
} }

#endif
