/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_RENDERERNODE3D_H_
#define NOX_APP_GRAPHICS_RENDERERNODE3D_H_

#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <string>
#include <nox/common/types.h>



enum RenderPass
{
	RenderPass_0, 
	RenderPass_Actor_noAlpha = RenderPass_0,
	RenderPass_Actor_Alpha,
	RenderPass_Last 
};

namespace nox 
{ 	

namespace logic
{

namespace actor
{ 
class Actor;
}

namespace physics
{
class ActorPhysics3d;
}

namespace graphics
{ 

class ActorGraphics3d;
class IRenderer3d;
	
}

}

namespace app { namespace graphics
{

class RendererNode3d : public SceneGraphNode
{
public:
	RendererNode3d(nox::logic::actor::Actor * owner);
	void updateAnimation(const nox::Duration& deltaTime);

private:
	void onNodeEnter3d(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes) override;
	void onAttachToRenderer(IRenderer3d* renderer) override;

	void onDetachedFromRenderer() override;

	std::string name;
	nox::logic::graphics::ActorGraphics3d * actorGraphics;
	nox::logic::physics::ActorPhysics3d* actorPhysics;

	glm::mat4 previousMatrix;
};

}
} }

#endif
