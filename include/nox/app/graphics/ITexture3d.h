#ifndef NOX_APP_GRAPHICS_ITEXTURE3D_H
#define	NOX_APP_GRAPHICS_ITEXTURE3D_H

#include <string>

namespace nox { namespace app { namespace graphics {


	/**
	*	Loads and 
	*
	*/
class ITexture3d
{
public:
	virtual ~ITexture3d();
	virtual void init(unsigned int textureTarget, const std::string& fileName) = 0;
	virtual bool load() = 0;
	virtual void bind(unsigned int textureUnit) = 0;
	virtual bool hasAlpha() = 0;

};

} } }

#endif	