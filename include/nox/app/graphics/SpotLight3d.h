/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//Reused code from: http://ogldev.atspace.co.uk/

#ifndef NOX_APP_GRAPHICS_SPOTLIGHT3D_H
#define NOX_APP_GRAPHICS_SPOTLIGHT3D_H

#include <nox/app/graphics/PointLight3d.h>
#include <memory>

namespace nox { namespace app
{
namespace graphics
{

class SpotLight3d : public PointLight3d
{
public:
	SpotLight3d();
	SpotLight3d(std::shared_ptr<Mesh3d> sphereLightVolume, glm::vec3 color = glm::vec3(1), float ambientIntensity = 1, float diffuseIntensity = 1,
		glm::vec3 position = glm::vec3(), float constantAttention = 0, float linearAttention = 1, float exponentialAttention = 0, float cutoffAngle = 20, glm::vec3 direction = glm::vec3(0, 0, -1));
	
	~SpotLight3d();

	void init(GLuint shaderProgram, GLuint stenchilProgram) override;
	void render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection) override;
	void render(RenderData* renderData, const glm::mat4x4& mvp) override;

	void setDirection(glm::vec3 direction);
	void setConeAngle(float angle);

	glm::vec3 getDirection();
	float getConeAngle();

private:

	float coneAngle;
	glm::vec3 coneDirection;

	GLuint spotLight_coneAngle;
	GLuint spotLight_coneDirection;


};

}
} }

#endif
