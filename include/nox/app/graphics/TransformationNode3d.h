/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_TRANSFORMATIONNODE3D_H_
#define NOX_APP_GRAPHICS_TRANSFORMATIONNODE3D_H_

#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/GraphicsAssetManager3d.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <string.h>

namespace nox { namespace app { namespace graphics {

/**
 * Transforms the modelMatrix for it's children, restoring
 * the original when leaving.
 */
class TransformationNode3d : public SceneGraphNode
{
public:
	TransformationNode3d();
	TransformationNode3d(const nox::logic::actor::Transform3d* transformation, std::string name);

	~TransformationNode3d();
    
	void setTransform(const glm::mat4& matrix);
	glm::mat4& getTransform();
	

private:
	void onNodeEnter3d(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes) override;
	void onNodeLeave3d(RenderData& renderData, glm::mat4x4& modelMatrix) override;

	const nox::logic::actor::Transform3d* transformationComponent;
    
	glm::mat4x4 transformationMatrix;
	glm::mat4x4 savedMatrix;

	std::string name;

};

}}}

#endif
