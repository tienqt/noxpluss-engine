/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//Reused code from: http://ogldev.atspace.co.uk/

#ifndef NOX_APP_GRAPHICS_DIRECTIONALLIGHT3D_H_
#define NOX_APP_GRAPHICS_DIRECTIONALLIGHT3D_H_

#include <nox/app/graphics/BaseLight3d.h>
#include <nox/app/graphics/Mesh3d.h>

#include <GL/glew.h>
#include <glm/mat4x4.hpp>
#include <memory>

namespace nox { namespace app { namespace graphics
{

class RenderData;

class DirectionalLight3d : public BaseLight3d
{
public:
	DirectionalLight3d();
	DirectionalLight3d(std::shared_ptr<Mesh3d> quadLightVolume, glm::vec3 color = glm::vec3(1), float ambientIntensity = 1, float diffuseIntensity = 1,
		glm::vec3 direction = glm::vec3(0, -1, 0));
	~DirectionalLight3d();

	void init(GLuint shaderProgram, GLuint stenchilProgram) override;
	void render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection = glm::mat4(1)) override;

	void setDirection(glm::vec3 direction);

private:
	glm::vec3 direction;

	std::shared_ptr<Mesh3d> quadLightVolume;

	GLuint directionalLight_base_color;
	GLuint directionalLight_base_ambientIntensity;
	GLuint directionalLight_base_diffuseIntensity;
	GLuint directionalLight_direction;

	GLuint eyeWorldPos;
	GLuint specularIntensity;
	GLuint specularPower;
	GLuint lightType;
	GLuint screenSize;

	GLuint posSampler;
	GLuint colorSampler;
	GLuint normalSampler;

	GLuint mvpHandle;

	glm::mat4 identityMatrix;

};

}
} }

#endif
