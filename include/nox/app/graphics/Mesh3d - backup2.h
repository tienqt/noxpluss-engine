#ifndef TRDMESH_H
#define TRDMESH_H

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>
#include <nox/app/graphics/opengl/RenderData.h>
#include <nox/app/graphics/TrdTexture.h>

#include "TrdMeshData.h"
#include <assimp/Importer.hpp>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

namespace nox {
namespace app {
namespace graphics {

	class TrdMesh
	{
	public:

		TrdMesh();

		/**
			* Draws the mesh based on the passed render data.
			* @param the renderdata, telling what shader program is in use, etc.
			*/
		void draw(RenderData * renderData);

		/**
			* Adds and returns all vertices. Can be used for calculating collision mesh.
			* @return vertices for the entire mesh (all sub-meshes combined)
			*/
		std::vector<VertexData> getVertices();

		/**
			* Adds and returns all indices. Used together with return value from getVertices().
			* @return indices for the entire mesh (all sub-meshes combined)
			*/
		std::vector<unsigned int> getIndices();


		/**
			* Load a mesh/scene from file. For example an .obj file from Blender.
			* @param path to the file
			* @return true if loaded sucessfully
			*/
		bool loadMesh(const std::string& filename);

		/**
			* @return true if the mesh contains alpha textures.
			*/
		bool hasAlphaTextures();

		~TrdMesh();


	private:

		GLuint VAO;
		GLuint VBO;
		GLuint IBO;

		struct MeshEntry {
			MeshEntry();

			~MeshEntry();

			void init(const std::vector<VertexData>& vertices, const std::vector<unsigned int>& indices);

			int numIndices;
			int baseIndex;
			int baseVertex;

			unsigned int materialIndex;
		};

		std::vector<TrdTexture*> textures;
		std::vector<MeshEntry> meshEntries;

		bool hasAlpha;

		bool initFromScene(const aiScene* scene, const std::string& filename);
		void initMesh(unsigned int index, const aiMesh* paiMesh);
		bool initMaterials(const aiScene* pScene, const std::string& Filename);
		void clear();

				

		Assimp::Importer Importer;

		std::vector<VertexData> vertices;
		std::vector<unsigned int> indices;

		int numIndices;
	};

}
}
}
#endif