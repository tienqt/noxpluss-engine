#ifndef NOX_APP_GRAPHICS_TEXTURE3D_H
#define	NOX_APP_GRAPHICS_TEXTURE3D_H

#include <string>

#include <GL/glew.h>
#include <SDL_image.h>

#include <nox/app/log/Logger.h>
#include <nox/app/graphics/ITexture3d.h>

namespace nox { namespace app { namespace graphics {


/**
*	Loads and handles a SDL-based texture from an image file
*/
class SdlTexture3d : public ITexture3d
{
public:
	SdlTexture3d();
	~SdlTexture3d() override;

	void init(unsigned int TextureTarget, const std::string& FileName) override;
	bool load() override;
	void bind(GLenum TextureUnit) override;
	bool hasAlpha() override;


private:

	std::string fileName;
	GLenum textureTarget;
	GLuint textureObj;
	SDL_Surface* image;

	bool alpha;
};

} } }

#endif	