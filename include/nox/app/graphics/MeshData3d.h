/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef NOX_APP_GRAPHICS_MESHDATA3D_H
#define NOX_APP_GRAPHICS_MESHDATA3D_H

#include <glm/glm.hpp>

namespace nox { namespace app { namespace graphics
{
struct VertexData3d
{
	glm::vec4 position;
	glm::vec4 normal;
	glm::vec4 tangent;
	glm::vec4 color;
	glm::vec2 uvPosition;
};

struct TextureData3d
{
	unsigned int id;
	unsigned int type;
};

struct BoneInfo3d
{
	glm::mat4 boneOffset;			// A bone's offset to the 
	glm::mat4 finalTransformation;	// The final tranformation of the bone (in game's world space)
};

const int MAX_BONES = 4;			// Standard
			
struct VertexBoneData3d
{
	int ids[MAX_BONES];				// The IDs to the bones that affect this vertex
	float weights[MAX_BONES];		// The weight of each bone (how much they affect)

	VertexBoneData3d()
	{
		reset();
	};

	void reset()
	{
		for (int i = 0; i < MAX_BONES; i++)
		{
			ids[i] = 0;
			weights[i] = 0;
		}
	}

	void addBoneData(int boneID, float weight)
	{
		for (int i = 0; i < MAX_BONES; i++) {
			if (weights[i] == 0.0) {
				ids[i] = boneID;
				weights[i] = weight;
				return;
			}
		}

		// should never get here - more bones than we have space for
		assert(0);
	}
};
} } }
#endif