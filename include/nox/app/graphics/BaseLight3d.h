/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

//Reused code from: http://ogldev.atspace.co.uk/

#ifndef NOX_APP_GRAPHICS_BASELIGHT3D_H_
#define NOX_APP_GRAPHICS_BASELIGHT3D_H_

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/mat4x4.hpp>
#include <string>
#include <GL/glew.h>

namespace nox { namespace app { namespace graphics
{
class Mesh3d;
class RenderData;

class BaseLight3d  
{
public:

enum LightType
{
	DIRECTIONAL,
	POINT,
	SPOT
};

	BaseLight3d();
	BaseLight3d(Mesh3d* lightVolume, glm::vec3 color, float ambientIntensity, float diffuseIntensity);
	~BaseLight3d();

	virtual void setLightVolume(Mesh3d* lightVolume);
	virtual void setColor(glm::vec3 color);
	virtual void setAmbientIntensity(float ambientIntensity);
	virtual void setDiffuseIntensity(float diffuseIntensity);
	virtual void setType(int type);
	void setName(std::string name);
	void setId(int id);

	virtual Mesh3d* getLightVolume();
	virtual glm::vec3 getColor();
	virtual float getAmbientIntensity();
	virtual float getDiffuseIntensity();
	virtual int getType();
	std::string getName();
	int getId();

	virtual void init(GLuint shaderProgram, GLuint stenchilProgram);
	virtual void render(RenderData* renderData, const glm::mat4x4& mvp);
	virtual void render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection = glm::mat4(1));

private:
	glm::vec3 color;
	float ambientIntensity;
	float diffusseIntensity;
	int lightId;

	int type;
	Mesh3d* lightVolume;
	std::string name;
};

}
} }

#endif
