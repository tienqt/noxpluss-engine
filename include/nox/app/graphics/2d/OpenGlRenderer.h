/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_APP_GRAPHICS_OPENGLRENDERER_H_
#define NOX_APP_GRAPHICS_OPENGLRENDERER_H_

#include "IRenderer.h"
#include "GeometrySet.h"

#include <nox/app/log/Logger.h>
#include <nox/app/graphics/TextureManager.h>
#include <nox/app/graphics/TextureRenderer.h>
#include <nox/app/graphics/opengl_utils.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/util/thread/ThreadBarrier.h>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <map>
#include <mutex>
#include <thread>
#include <vector>
#include <memory>
#include <chrono>

//#define TIME_BLUR //Used for timing the blur rendering.

namespace nox { namespace app
{

namespace resource
{

class IResourceAccess;

}

namespace graphics
{

class SceneGraphNode;
class DebugRenderer;
class SubRenderer;

struct ShaderInfo
{
	GLuint shaderProgram;
	GLint ViewProjectionMatrixUniform;
	GLint TextureUniform;
	GLint ColorUnifrom;
};

class OpenGlRenderer: public IRenderer
{
public:
	OpenGlRenderer();

	~OpenGlRenderer();

	bool init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize) override;

	void onRender() override;

	void setRootSceneNode(const std::shared_ptr<SceneGraphNode>& rootNode) override;
	void setCamera(const std::shared_ptr<Camera>& camera) override;
	const TextureManager& getTextureManager() const override;
	TextureRenderer& getTextureRenderer() override;
	void addDebugGeometrySet(const std::shared_ptr<GeometrySet>& set) override;
	void removeDebugGeometrySet(const std::shared_ptr<GeometrySet>& set) override;

	void addStenciledTiledTextureLayer(
			const std::string& layerName,
			std::unique_ptr<StenciledTiledTextureGenerator> generator,
			std::unique_ptr<StenciledTiledTextureRenderer> renderer,
			unsigned int renderLevel) override;
	void removeTiledTextureLayer(const std::string& layerName) override;

	void addLight(std::shared_ptr<Light> light) override;
	void removeLight(std::shared_ptr<Light> light) override;

	void resizeWindow(const glm::uvec2& windowSize) override;

	bool toggleDebugRendering() override;

	bool isDebugRenderingEnabled() override;

	SubRenderer* addSubRenderer(std::unique_ptr<SubRenderer> renderer) override;
	void removeSubRenderer(SubRenderer* renderer) override;

	void lightUpdate(const std::shared_ptr<Light>& light) override;

	void setAmbientLightLevel(const float lightLevel) override;

	void organizeRenderSteps() override;

	void setLightStartRenderLevel(const unsigned int litRenderLevel) override;

	void setBackgroundGradient(std::unique_ptr<BackgroundGradient> background) override;

	void loadTextureAtlases(const resource::Descriptor& graphicsResourceDescriptor, resource::IResourceAccess* resourceAccess) override;

	void setWorldTextureAtlas(const std::string& atlasName) override;

private:
	struct StenciledTiledTextureLayer
	{
		StenciledTiledTextureLayer();
		~StenciledTiledTextureLayer();

		std::unique_ptr<StenciledTiledTextureRenderer> renderer;
		std::unique_ptr<StenciledTiledTextureGenerator> generator;
		unsigned int renderLevel;
	};

	class RenderStep
	{
	public:
		RenderStep(const unsigned int startLevel, const unsigned int endLevel);
		virtual ~RenderStep();

		virtual void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix) = 0;

	protected:
		unsigned int getStartRenderLevel() const;
		unsigned int getEndRenderLevel() const;

	private:
		const unsigned int startLevel;
		const unsigned int endLevel;
	};

	class TextureRenderStep: public RenderStep
	{
	public:
		TextureRenderStep(
			TextureRenderer& renderer,
			const unsigned int startLevel,
			const unsigned int endLevel,
			const GLuint shader,
			const GLint stencilRef,
			const bool affectedByLight);

		void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix) override;

	private:
		TextureRenderer& renderer;
		GLuint shader;
		GLint stencilRef;
		bool affectedByLight;
	};

	class StenciledTiledTextureStencilStep: public RenderStep
	{
	public:
		StenciledTiledTextureStencilStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLint ref);
		void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix) override;

	private:
		StenciledTiledTextureRenderer* renderer;
		GLint ref;
	};

	class StenciledTiledTextureRenderStep: public RenderStep
	{
	public:
		StenciledTiledTextureRenderStep(StenciledTiledTextureRenderer* renderer, const unsigned int renderLevel, const GLuint shader, const GLint ref);
		void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix) override;

	private:
		StenciledTiledTextureRenderer* renderer;
		GLuint shader;
		GLint ref;
	};

	class BackgroundGradientRenderStep: public RenderStep
	{
	public:
		BackgroundGradientRenderStep(BackgroundGradient* gradient, const unsigned int renderLevel, const GLint stencilRef);
		void render(RenderData& renderData, const glm::mat4& viewProjectionMatrix) override;

	private:
		BackgroundGradient* gradient;
		GLint stencilRef;
	};

	/**
	 *  Will sort the objects for rendering and put them in the (@link #renderLevels).
	 *  The function is also responsible for culling the objects that will not be drawn
	 */
	void parseSceneGraph();

	/**
	 *  Responsible for doing the render calls on the objects in (@link #renderLevels).
	 *  The renderer will do one draw call for each textureAtlas at each level.
	 */
	void renderObjects();

	void renderWorld();	

	void renderLitWorld();

	void renderEffectWorldToScreen();
	
	void renderLightHorizontalBlur();
	
	void renderLightVerticalBlur();

	void onLightMapRender();

	void onEffectMapRender();

	bool initOpenGL(const glm::uvec2& windowSize);

	GlslShader createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const;
	GlslVertexAndFragmentShader createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const;

	bool setupLitWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupEffectWorldRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupSpriteRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupDebugRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupLightMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupEffectMapRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);
	
	bool setupLightHorizontalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);
	
	bool setupLightVerticalBlur(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	bool setupBackgroundGradientRendering(resource::IResourceAccess* resourceCache, const std::string& shaderDirectory);

	void lightRenderingDataPrepare();
	void lightRenderingIo();

	void effectRenderingDataPrepare();
	void effectRenderingIo();

	void debugDataPreparation();
	void textureDataPreparation();
	void lightingDataPreparation();
	void effectDataPreparation();
	void subRenderersDataPreparation();

	const unsigned int NUM_RENDER_LEVELS;

	IContext* context;
	mutable log::Logger log;
	bool initialized;

	std::unique_ptr<DebugRenderer> debugRenderer;
	std::map<std::string, StenciledTiledTextureLayer> stenciledTiledTextureLayers;

	std::unique_ptr<BackgroundGradient> backgroundGradient;

	std::vector<std::unique_ptr<RenderStep>> renderSteps;
	unsigned int lightStartRenderLevel;

	std::shared_ptr<SceneGraphNode> sceneRoot;
	std::shared_ptr<Camera> renderCamera;

	std::unordered_map<std::string, ShaderInfo> shaders;

	bool debugRenderingEnabled;
	bool windowSizeChanged;
	glm::uvec2 windowSize;

	float lightLevel;

	glm::vec4 currentClearColor;
	glm::vec4 lightClearColor;
	glm::vec4 renderClearColor;

	FrameBuffer gameplayFBO;
	GLuint renderTexture;
	GLuint lightLuminanceTexture;
	
	float superSampleMultiplier;
	unsigned int lightMapMultisamplingSamples;
	float lightTextureSizeMultiplier;
	float effectTextureSizeMultiplier;

	GLuint litGameplayPresentVAO;
	GLuint litGameplayPresentVBO;
	GLuint litGameplayPresentProgram;
	FrameBuffer litGameplayPresentFBO;
	GLuint litGameplayPresentTexture;

	GLint litGameplayPresentMatrixUniform;
	GLint litGameplayPresentRenderUniform;
	GLint litGameplayPresentLightUniform;
	GLint litGameplayPresentLightLuminanceUniform;
	GLint litGameplayPresentDayLightCoefficiencyUniform;

	GLuint effectGameplayPresentVAO;
	GLuint effectGameplayPresentVBO;
	GLuint effectGameplayPresentProgram;

	GLint effectGameplayPresentMatrixUniform;
	GLint effectGameplayPresentRenderUniform;
	GLint effectGameplayPresentLightUniform;

	GLuint effectMapVAO;
	GLuint effectMapVBO;
	FrameBuffer effectMapFBO;
	GLuint effectMapTexture;

	GLuint lightMapVAO;
	GLuint lightMapVBO;
	FrameBuffer lightMapFBO;
	GLuint lightMapTexture;
	FrameBuffer lightMapMultiSampleFBO;

	GLuint lightHorizontalBlurVAO;
	GLuint lightHorizontalBlurVBO;
	GLuint lightHorizontalBlurProgram;
	FrameBuffer lightHorizontalBlurFBO;
	GLuint lightHorizontalBlurTexture;

	GLint lightHorizontalBlurMatrixUniform;
	GLint lightHorizontalBlurTextureUniform;
	GLint lightHorizontalBlurSizeUniform;
	
	GLuint lightVerticalBlurVAO;
	GLuint lightVerticalBlurVBO;
	GLuint lightVerticalBlurProgram;
	FrameBuffer lightVerticalBlurFBO;
	GLuint lightVerticalBlurTexture;

	GLint lightVerticalBlurMatrixUniform;
	GLint lightVerticalBlurTextureUniform;
	GLint lightVerticalBlurSizeUniform;

	GLuint lightTexture;

	bool blurEnabled;
	float pixelsPerBlurStep;
	
	std::vector<std::shared_ptr<Light>> staticLights;
	std::vector<ObjectCoordinate> staticLightCoords;
	size_t numberOfStaticLightCoords;
	bool staticLightsUpdated;

	std::vector<std::shared_ptr<Light>> dynamicLights;
	std::vector<ObjectCoordinate> dynamicLightCoords;
	size_t numberOfDynamicLightCoords;
	bool dynamicLightsUpdated;

	GLsizeiptr lightDataSize;
	GLsizei totalLightCoords;

	std::vector<std::shared_ptr<Light>> lightEffects;
	std::vector<ObjectCoordinate> effectCoords;
	size_t numberOfEffectCoords;
	bool effectsUpdated;

	GLsizeiptr effectDataSize;
	GLsizei totalEffectCoords;

	TextureManager textureManager;
	TextureRenderer textureRenderer;
	GLuint worldAtlasTextureBuffer;

	// The new way of setting up different renderings.
	std::vector<std::unique_ptr<SubRenderer>> subRenderers;
	RenderData renderData;

	std::mutex lightRenderMutex;
	std::mutex debugRenderMutex;

	thread::ThreadBarrier debugDataPreparationStartBarrier;
	thread::ThreadBarrier debugDataPreparationFinishedBarrier;
	std::thread debugDataPreparerThread;
	bool quitDebugDataPreparation;

	thread::ThreadBarrier textureDataPreparationStartBarrier;
	thread::ThreadBarrier textureDataPreparationFinishedBarrier;
	std::thread textureDataPreparerThread;
	bool quitTextureDataPreparation;

	thread::ThreadBarrier lightingDataPreparationStartBarrier;
	thread::ThreadBarrier lightingDataPreparationFinishedBarrier;
	std::thread lightingDataPreparerThread;
	bool quitLightingDataPreparation;

	thread::ThreadBarrier effectDataPreparationStartBarrier;
	thread::ThreadBarrier effectDataPreparationFinishedBarrier;
	std::thread effectDataPreparerThread;
	bool quitEffectDataPreparation;

	thread::ThreadBarrier subRenderersDataPreparationStartBarrier;
	thread::ThreadBarrier subRenderersDataPreparationFinishedBarrier;
	std::thread subRenderersDataPreparerThread;
	bool quitSubRenderersDataPreparation;

#ifdef TIME_BLUR
	std::chrono::high_resolution_clock::duration accumulatedBlurTime;
	unsigned int frameCount = 0;
	const unsigned int NUM_AVERAGING_FRAMES = 60;
#endif
	
};

}
} }

#endif
