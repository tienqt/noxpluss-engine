#ifndef TRDMESH_H
#define TRDMESH_H

#include <iostream>
#include <GL/glew.h>
#include <vector>
#include <string>
#include <nox/app/graphics/opengl/RenderData.h>
#include <nox/app/graphics/TrdTexture.h>

#include "TrdMeshData.h"
#include <assimp/Importer.hpp>
#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <glm/gtx/transform.hpp>

namespace nox {
namespace app {
namespace graphics {



class TrdMesh
{
public:
	TrdMesh();

	/**
	* Draws the mesh based on the passed render data.
	* @param the renderdata, telling what shader program is in use, etc.
	*/
	void draw(RenderData * renderData);

	/**
	* Adds and returns all vertices. Can be used for calculating collision mesh.
	* @return vertices for the entire mesh (all sub-meshes combined)
	*/
	std::vector<VertexData> getVertices();

	/**
	* Adds and returns all indices. Used together with return value from getVertices().
	* @return indices for the entire mesh (all sub-meshes combined)
	*/
	std::vector<unsigned int> getIndices();


	/**
	* Load a mesh/scene from file. For example an .obj file from Blender.
	* @param path to the file
	* @return true if loaded sucessfully
	*/
	bool loadMesh(const std::string& filename);

	/**
	* @return true if the mesh contains alpha textures.
	*/
	bool hasAlphaTextures();

	~TrdMesh();


private:
	GLuint VAO;

	struct MeshEntry {
		MeshEntry();
		~MeshEntry();

		int materialIndex;
		int numIndices;
		int baseVertex;
		int baseIndex;
	};

	struct BoneInfo
	{
		glm::mat4 boneOffset;
		glm::mat4 finalTransformation;

		BoneInfo()
		{
			boneOffset = glm::mat4(0);
			finalTransformation = glm::mat4(0);
		}
	};

	struct VertexBoneData
	{
		static const int NUM_BONES_PER_VEREX = 4;

		GLuint ids[NUM_BONES_PER_VEREX];
		float weights[NUM_BONES_PER_VEREX];

		void addBoneData(int boneId, float weight);
	};

	std::vector<VertexData> vertexData;
	std::vector<VertexBoneData> boneData;
	std::vector<unsigned int> indices;

	std::vector<TrdTexture*> textures;
	std::vector<MeshEntry> meshEntries;

	GLuint boneVBO;

	bool hasAlpha;

	bool initFromScene(const aiScene* scene, const std::string& filename);

	void initMesh(int meshIndex,
		const aiMesh* paiMesh,
		std::vector<VertexData>& vertexDta,
		std::vector<VertexBoneData>& boneData,
		std::vector<unsigned int>& indice);

	bool initMaterials(const aiScene* pScene, const std::string& Filename);
	void clear();

	const aiScene* scene;

	aiMatrix4x4 globalInverseTransform;

	int numVertecies;
	
	// BONES //////////////////////////



	int numBones;
	void LoadBones(int MeshIndex, const aiMesh* paiMesh, std::vector<VertexBoneData>& Bones);
	std::map<std::string, int> boneMapping;
	std::vector<BoneInfo> boneInfos;
	glm::mat4 convertToGlmMatrix(const aiMatrix4x4 matrix);
	glm::mat4 convertToGlmMatrix(const aiMatrix3x3 matrix);

	void boneTransform(float TimeInSeconds, std::vector<glm::mat4>& Transforms);

	void readNodeHeirarchy(float animationTime, const aiNode* pNode, const glm::mat4& parentTransform);

	const aiNodeAnim* findNodeAnim(const aiAnimation* animation, const std::string nodeName);

	int findRotation(float animationTime, const aiNodeAnim* nodeAnim);
	int findScaling(float animationTime, const aiNodeAnim* nodeAnim);
	int findPosition(float animationTime, const aiNodeAnim* nodeAnim);

	void calcInterpolatedRotation(aiQuaternion& out, float animationTime, const aiNodeAnim* nodeAnim);
	void calcInterpolatedScaling(aiVector3D& out, float animationTime, const aiNodeAnim* nodeAnim);
	void calcInterpolatedPosition(aiVector3D& out, float animationTime, const aiNodeAnim* nodeAnim);

	
};

}
}
}
#endif