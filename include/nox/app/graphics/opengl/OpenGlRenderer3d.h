/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//Reused code from: http://ogldev.atspace.co.uk/

#ifndef NOX_APP_GRAPHICS_OPENGLRENDERER3D_H_
#define NOX_APP_GRAPHICS_OPENGLRENDERER3D_H_

#include <GL/glew.h>
#include <vector>
#include <memory>
#include <glm/glm.hpp>
#include <string>
#include <nox/util/Booker.h>
#include <nox/app/graphics/IRenderer3d.h>

#include <nox/app/graphics/DirectionalLight3d.h>
#include <nox/app/graphics/opengl/GBuffer.h>
#include <nox/app/graphics/SpotLight3d.h>
#include <nox/app/graphics/PointLight3d.h>



namespace nox { namespace app
{

namespace resource
{

class IResourceAccess;

}

namespace graphics
{

class Mesh3d;
class RenderData;
class TransformationNode3d;

class OpenGlRenderer3d : public IRenderer3d
{
public:
	OpenGlRenderer3d();

	~OpenGlRenderer3d();

	bool init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize) override;
	void onRender() override;
	void setCamera(const std::shared_ptr<Camera3d>& camera) override;
	void resizeWindow(const glm::uvec2& windowSize) override;
	bool toggleDebugRendering() override;
	bool isDebugRenderingEnabled() override;
	void setRootSceneNode(const std::shared_ptr<TransformationNode3d>& rootSceneNode) override;
	void setGraphicsAssetManager(const std::shared_ptr<GraphicsAssetManager3d>& assetManager) override;

	const std::shared_ptr<GraphicsAssetManager3d> getGraphicsAssetManager() override;
	void addLight(std::shared_ptr<BaseLight3d> light, int type) override;
	void removeLight(std::shared_ptr<BaseLight3d> light, int type) override;
	nox::logic::physics::BulletDebugDraw3d* getDebugRenderer() override;

	std::shared_ptr<Mesh3d> sphere;
	std::shared_ptr<Mesh3d> quad;

private:
	struct VertexAndFragmentShader
	{
		bool isValid() const;

		GLuint vertex = 0u;
		GLuint fragment = 0u;
	};

	struct StenciledTiledTextureLayer
	{
		StenciledTiledTextureLayer();
		~StenciledTiledTextureLayer();

		unsigned int renderLevel;
	};

	bool initOpenGL(const glm::uvec2& windowSize);

	GLuint createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const;
	VertexAndFragmentShader createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const;
	
	void directionalLightPass();
	void finalPass();

	void renderLightVolume(BaseLight3d* p);
	void renderPointLight(BaseLight3d* p);
	void renderSpotLight(BaseLight3d* p);

	void renderWorld();
	void renderLitWorld();

	mutable log::Logger log;

	RenderData renderData;

	std::shared_ptr<GraphicsAssetManager3d> assetManager;
	std::shared_ptr<Camera3d> renderCamera;

	bool windowSizeChanged;
	glm::uvec2 windowSize;


	enum ShaderType {
		GEOMETRY_PASS,
		DIRLIGHT_PASS,
		POINTLIGHT_PASS,
		STENCIL_PASS,
		SPOTLIGHT_PASS,
		DEBUG_PASS,

		NUM_SHADERS
	};

	enum UniformType
	{
		MVP,
		MODEL,
		LIGHT_POSITION,
		CAMERA_POSITION,
		TEXTURE_COORD,
		NUM_HANDELS
	};

	GLuint shaderPrograms[NUM_SHADERS];
	GLuint uniformHandels[UniformType::NUM_HANDELS];

	GLuint sphereMvpHandle;

	std::unique_ptr<nox::logic::physics::BulletDebugDraw3d> debugRenderer;

	std::shared_ptr<TransformationNode3d> rootSceneNode;
	
	std::vector <alphaNode*> alphaNodes;

	std::unique_ptr<GBuffer> gBuffer;

	bool compareZvalues(alphaNode * a, alphaNode *b);

	std::vector<SpotLight3d*> spotLights;
	std::vector<PointLight3d*> pointLights;

	std::vector<std::shared_ptr<BaseLight3d>> dynamicLights;
	std::vector<std::shared_ptr<BaseLight3d>> directionalLights;

	DirectionalLight3d* d1;

	nox::util::Booker<int, std::shared_ptr<BaseLight3d>> lightBooker;

	int size;
};

}
} }

#endif
