#ifndef NOX_APP_GRAPHICS_ANIMATION3D_H
#define	NOX_APP_GRAPHICS_ANIMATION3D_H

#include <string>


namespace nox { namespace logic { namespace graphics 
{

	class ActorGraphics3d;

class Animation3d
{
public:
	Animation3d(nox::logic::graphics::ActorGraphics3d * owner);

	int getAnimationIndex();
	std::string getAnimationName();

	//void init(int index, float speed, float startTime);

	/**
	*   Set current animation by specifying its index
	*   @param index Animation index, starting from 0
	*   @param startTime How many seconds into the animation it should start rendering
	*   @return false if the animation was not found
	*/
	bool setAnimation(int index, bool broadcast, float speed = 1.0f, float startTime = 0.0f);


	float getAnimationSpeed();
	float getAnimationStartTime();

	/**
	*	Set animation speed. 0.0 to pause, 1.0 for normal speed, 2.0 for double speed, etc.
	*	Negative value makes animation play backwards.
	*/
	void setAnimationSpeed(float speed);

private:
	nox::logic::graphics::ActorGraphics3d * owner;

	std::string animationName;
	int animationIndex;
	float animationSpeed;
	float animationStartTime;

};

} } } 

#endif	