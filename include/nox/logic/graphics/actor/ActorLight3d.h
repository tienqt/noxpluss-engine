/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_GRAPHICS_ACTORLIGHT3D_H_
#define NOX_LOGIC_GRAPHICS_ACTORLIGHT3D_H_

#include <nox/logic/actor/Component.h>
#include <glm/glm.hpp>
#include <unordered_map>

namespace nox {

namespace app
{

namespace graphics
{ 
class BaseLight3d;
class LightRenderNode3d;
}

}

namespace logic
{

namespace actor
{

class Transform;

}

namespace physics
{

class Simulation;

}

namespace graphics
{

/**
 * Renders a series of lights relative to the actor.
 */
class ActorLight3d: public actor::Component
{
public:
	static const IdType NAME;

	~ActorLight3d();

	const ActorLight3d::IdType& getName() const override;
	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	void onActivate() override;
	void onDeactivate() override;

	void disableLights();
	void disableLight(const std::string& lightName);
	void enableLights();
	void enableLight(const std::string& lightName);

	std::shared_ptr<nox::app::graphics::BaseLight3d> getLight(std::string lightName);

private:
	void broadcastLightCreation(const std::shared_ptr<app::graphics::LightRenderNode3d>& lightNode);
	void broadcastLightRemoval(const std::shared_ptr<app::graphics::LightRenderNode3d>& lightNode);

	physics::Simulation* physics;
	actor::Transform* transformComponent;
	std::unordered_map<std::string, std::shared_ptr<nox::app::graphics::LightRenderNode3d>> lightMap;
	
};

} } }

#endif
