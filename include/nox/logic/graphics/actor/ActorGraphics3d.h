/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_GRAPHICS_ACTORGRAPHICS3D_H_
#define NOX_LOGIC_GRAPHICS_ACTORGRAPHICS3D_H_

#include <nox/logic/actor/Component.h>
#include <nox/app/graphics/RenderNode3d.h>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/event/IBroadcaster.h>


namespace nox
{

namespace app { namespace graphics
{
class TransformationNode3d;

class Mesh3d;

} }

namespace logic
{

namespace actor
{

class Transform3d;

}

namespace graphics
{

	class Animation3d;

class ActorGraphics3d: public actor::Component, public nox::logic::event::IListener
{
public:
	static const IdType NAME;

	ActorGraphics3d();
	~ActorGraphics3d();

	const IdType& getName() const override;

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onUpdate(const Duration& deltaTime) override;
	void onCreate() override;
	void onDestroy() override;
	void onDeactivate() override;
	void onActivate() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;

	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void setMeshData(std::shared_ptr<nox::app::graphics::Mesh3d> meshData);

	std::shared_ptr<app::graphics::RendererNode3d> getRendererNode();

	std::string getModelName();

	std::shared_ptr<nox::app::graphics::Mesh3d> getMeshdatas();
	std::shared_ptr<app::graphics::TransformationNode3d> getActorTransformNode();

	//Animation3d * getAnimation();

	int getAnimationIndex();
	int getPreviousAnimationIndex();

	float getPreviousAnimationSpeed();
	float getPreviousAnimationTime();
	std::string getAnimationName();

	/**
	*   Set current animation by specifying its name
	*   @param name Animation name
	*   @param logIt True if you want the change to be visible during rewind/replay
	*   @param speed How fast to play the animation. Negative value to play it backwards
	*   @param startTime How many seconds into the animation it should start rendering
	*   @return false if the animation was not found
	*/
	bool setAnimation(std::string name, bool logIt, float speed = 1.0f, float startTime = 0.0f);


	/**
	*   Set current animation by specifying its index
	*   @param index Animation index, starting from 0
	*   @param logIt True if you want the change to be visible during rewind/replay
	*   @param speed How fast to play the animation. Negative value to play it backwards
	*   @param startTime How many seconds into the animation it should start rendering
	*   @return false if the animation was not found
	*/
	bool setAnimation(int index, bool logIt, float speed = 1.0f, float startTime = 0.0f);


	float getAnimationSpeed();
	float getAnimationStartTime();

	/**
	*	Set animation speed. 0.0 to pause, 1.0 for normal speed, 2.0 for double speed, etc.
	*	Negative value makes animation play backwards.
	*/
	void setAnimationSpeed(float speed);

	void broadcastAnimationChanged();

	float getAnimationTime();
	void stepAnimation(const nox::Duration& deltaTime);

	glm::vec3 overLayColor;

protected:


private:
	void broadcastAiSceneCreation();

	std::string name;
	std::string dataPath;

	std::shared_ptr<app::graphics::Mesh3d> meshdatas;

	void updateRenderTransform();
	void updateActorTransform();

	void broadcastSceneNodeCreation();
	void broadcastSceneNodeRemoval();

	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;

	actor::Transform3d* transformComponent;

	std::shared_ptr<app::graphics::TransformationNode3d> actorTransformNode;
	std::shared_ptr<app::graphics::TransformationNode3d> renderTransformNode;
	std::shared_ptr<app::graphics::RendererNode3d> rendererNode;

	// Animation:
	std::string animationName;
	int currentAnimationIndex;
	int previousAnimation;
	float currentAnimationSpeed;
	float previousAnimationSpeed;
	float currentAnimationTime;
	float previousAnimationTime;

	nox::logic::event::ListenerManager listener;
};

} } }

#endif
