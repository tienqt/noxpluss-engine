#ifndef NOX_LOGIC_GRAPHICS_SCENENODEEDITED_H_
#define NOX_LOGIC_GRAPHICS_SCENENODEEDITED_H_

#include <nox/logic/event/Event.h>
#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/logic/actor/Identifier.h>

namespace nox { namespace logic { namespace graphics
{

/**
 * A scene graph node was either added or removed.
 */
class SceneNodeEdited: public event::Event
{
public:
	enum class Action
	{
		CREATE,
		REMOVE,
		TIME_REMOVE,
		TIME_ADD
	};

	static IdType ID;

	SceneNodeEdited(const std::shared_ptr<app::graphics::SceneGraphNode>& sceneNode, Action action);
	SceneNodeEdited(const std::shared_ptr<app::graphics::SceneGraphNode>& sceneNode, Action action, const nox::logic::actor::Identifier& actorId);

	const std::shared_ptr<app::graphics::SceneGraphNode>& getSceneNode() const;
	Action getEditAction() const;
	const nox::logic::actor::Identifier getActorId() const;

private:
	std::shared_ptr<app::graphics::SceneGraphNode> sceneNode;
	Action action;
	nox::logic::actor::Identifier actorId;
};

} } }

#endif /* SCENENODECREATEEVENT_H_ */
