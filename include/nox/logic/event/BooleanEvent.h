/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_EVENT_BOOLEANEVENT_H_
#define NOX_LOGIC_EVENT_BOOLEANEVENT_H_

#include <nox/logic/event/Event.h>

namespace nox { namespace logic { namespace event {

class BooleanEvent: public Event
{
public:
	BooleanEvent(const Event::IdType id, const bool boolean);
	virtual ~BooleanEvent();

	BooleanEvent(BooleanEvent&& other) NOX_NOEXCEPT;
	BooleanEvent& operator=(BooleanEvent&& other);

	BooleanEvent(const BooleanEvent& other) = default;
	BooleanEvent& operator=(const BooleanEvent& other) = default;


	bool isTrue() const NOX_NOEXCEPT;

private:
	bool boolean;
};

} } }

#endif
