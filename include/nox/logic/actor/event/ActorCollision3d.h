/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/



#ifndef NOX_LOGIC_ACTOR_ACTORCOLLISION3D_H_
#define NOX_LOGIC_ACTOR_ACTORCOLLISION3D_H_

#include <nox/logic/actor/event/Event.h>
#include <nox/logic/actor/Actor.h>
#include <glm/vec3.hpp>
namespace nox { namespace logic { namespace actor {

class Actor;


/**
* Sent when a collision start or end.
*/
class ActorCollision3d final : public Event
{
public:
	static const event::Event::IdType ID;
	
	ActorCollision3d(Actor* transformedActor, Actor* actorOne, Actor* actorTwo, std::vector<glm::vec3> collisionPoints,
		glm::vec3 sumNormalForce, glm::vec3 sumFrictionForce, bool started );
	
	ActorCollision3d(Actor* transformedActor, Actor* actorOne, Actor* actorTwo, bool started);
	
	~ActorCollision3d();
	
	bool isStartingCollision();
	
	std::vector<glm::vec3> getCollisionPoints();
	
	glm::vec3 getSumNormalForce();
	
	glm::vec3 getSumFrictionForce();
	
	Actor* getActorOne();
	
	Actor* getActorTwo();

private:
	
	bool isStarting;
	
	std::vector<glm::vec3> collisionPoints;
	
	glm::vec3 sumNormalForce;
	glm::vec3 sumFrictionForce;
	
	Actor * firstActor;
	Actor * secondActor;
};

}}}

#endif
