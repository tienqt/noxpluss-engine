#ifndef NOX_LOGIC_ACTOR_TRANSFORMCHANGE3D_H_
#define NOX_LOGIC_ACTOR_TRANSFORMCHANGE3D_H_

#include <glm/mat4x4.hpp>
#include <nox/logic/actor/event/Event.h>
#include <glm/gtc/quaternion.hpp>

namespace nox {	namespace logic { namespace actor
{

class Actor;

/**
* The transformation of an actor changed.
*/
class TransformChange3d final : public Event
{
public:
	static const event::Event::IdType ID;

	TransformChange3d(Actor* transformedActor, const glm::vec3& position, const glm::vec3 rotation, const glm::quat qRotation, const glm::vec3& scale);
	~TransformChange3d();

	glm::mat4 getTransformMatrix() const;
	const glm::vec3& getPosition() const;
	const glm::vec3& getScale() const;
	const glm::vec3& getRotation() const;
	const glm::quat getQRotation() const;

private:
	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;
	glm::quat qRotation;
};

} } }

#endif