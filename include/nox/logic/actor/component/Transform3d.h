/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_ACTOR_TRANSFORM3D_H_
#define NOX_LOGIC_ACTOR_TRANSFORM3D_H_

#include "../Component.h"
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>

namespace nox { namespace logic { namespace actor
{

/**
 * Place an Actor3d with a position, rotation, and scale.
 * An important component used by many other components.
 *
 * # JSON Description
 * ## Name
 * %Transform3d
 *
 * ## Properties
 * - __position__:vec3 - Position of the Actor. Default vec3(0, 0, 0).
 * - __rotation__:vec3 - Rotation of the Actor. Default vec3(0, 0, 0).
 * - __scale__:vec3 - Scale of the actor. Default vec3(1, 1, 1).
 */
class Transform3d: public Component
{
public:
	//! Overloaded functions using this tag do not broadcast the transform change.
	struct NoBroadcast_t {};

	const static IdType NAME;

	virtual ~Transform3d();

	bool initialize(const Json::Value& componentJsonObject) override;
	void serialize(Json::Value& componentObject) override;
	void onCreate() override;
	const IdType& getName() const override;

	/**
	 * Get the transform matrix representing the operations scale->rotate->translate.
	 */
	const glm::mat4 getTransformMatrix() const;

	void setPosition(const glm::vec3& position);
	void setPosition(const glm::vec3& position, NoBroadcast_t);

	void setScale(const glm::vec3& scale);
	void setScale(const glm::vec3& scale, NoBroadcast_t);

	void setRotation(const glm::vec3& rotation);
	void setRotation(const glm::vec3& rotation, NoBroadcast_t);

	void setRotation(const glm::quat& rotation);
	void setRotation(const glm::quat& rotation, NoBroadcast_t);

	void setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale);
	void setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale, NoBroadcast_t);

	const glm::vec3& getPosition() const;
	const glm::vec3& getRotation() const;
	const glm::quat& getQuaternion() const;
	const glm::vec3& getScale() const;

	/**
	 * Broadcast a transformation change to other components,
	 * and globally to the EventManager.
	 */
	void broadcastTransformChange();
private:

	glm::vec3 position;
	glm::vec3 scale;
	glm::vec3 rotation;
	glm::quat qRotation;
};


inline void Transform3d::setPosition(const glm::vec3& position, NoBroadcast_t)
{
	this->position = position;
}

inline void Transform3d::setScale(const glm::vec3& scale, NoBroadcast_t)
{
	this->scale = scale;
}

inline void Transform3d::setRotation(const glm::vec3& rotation, NoBroadcast_t)
{
	this->rotation = rotation;
}

inline void Transform3d::setRotation(const glm::quat& rotation, NoBroadcast_t)
{
	this->qRotation = rotation;
}

inline void Transform3d::setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale, NoBroadcast_t)
{
	this->position = position;
	this->scale = scale;
	this->rotation = rotation;
}

inline const glm::vec3& Transform3d::getPosition() const
{
	return this->position;
}

inline const glm::vec3& Transform3d::getRotation() const
{
	return this->rotation;
}

inline const glm::quat& Transform3d::getQuaternion() const
{
	return this->qRotation;
}

inline const glm::vec3& Transform3d::getScale() const
{
	return this->scale;
}


} } }

#endif
