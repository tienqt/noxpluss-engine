/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_CONTROL_ACTION_H_
#define NOX_LOGIC_CONTROL_ACTION_H_

#include <nox/logic/actor/event/Event.h>

#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

namespace nox { namespace logic
{

namespace actor
{

class Actor;

}

namespace control
{

/**
 * A control action to apply to an Actor.
 *
 * The action is identified by a control name, and carries data about what the action wants to do and on which Actor
 * to do this action on.
 *
 * An action can either be a vector, a strength, a switch or a toggle.
 *
 * - A vector is a 2D vector with a direction and length. The length is between 0 and 1. Use getControlVector().
 * - A strength is a linear value between 0 and 1. Use getControlStrength().
 * - A switch is a boolean action. Use isSwitchedOn().
 * - A toggle is just the action.
 */
class Action: public actor::Event
{
public:
	static const IdType ID;

	/**
	 * Create an action with a vector.
	 *
	 * @param controledActor Actor to apply action on.
	 * @param controlName Name of the action.
	 * @param controlVector Vector of the action.
	 */
	Action(actor::Actor* controledActor, const std::string& controlName, const glm::vec3& controlVector);
	//Action(actor::Actor* controledActor, const std::string& controlName, const glm::vec2& controlVector);

	/**
	 * Create an action with a strength.
	 *
	 * @param controledActor Actor to apply action on.
	 * @param controlName Name of the action.
	 * @param controlStrength Strength of the action.
	 */
	Action(actor::Actor* controledActor, const std::string& controlName, float controlStrength);

	/**
	 * Create a switch action..
	 *
	 * @param controledActor Actor to apply action on.
	 * @param controlName Name of the action.
	 * @param on If the switch is on or off.
	 */
	Action(actor::Actor* controledActor, const std::string& controlName, bool on);

	/**
	 * Create a toggle action.
	 *
	 * @param controledActor Actor to apply action on.
	 * @param controlName Name of the action.
	 */
	Action(actor::Actor* controledActor, const std::string& controlName);

	/**
	 * Get the name of the action.
	 */
	const std::string& getControlName() const;

	/**
	 * Get the vector of the action.
	 *
	 * - A vector action will have its vector returned.
	 * - A strength action will have vec2(0, strength) returned.
	 * - A switch action will have vec2(0, 0) return if off and vec2(0, 1) returned if on.
	 * - A toggle action will return vec2(0, 0).
	 */
	const glm::vec2& getControlVector() const;
	const glm::vec3& getControlVector3d() const;

	/**
	 * Get the force of the control action.
	 *
	 * - A vector action will have its length returned.
	 * - A strength action will have its strength returned.
	 * - A switch action will be 1 if on, and 0 if off.
	 * - A toggle action will return 0
	 */
	float getControlStrength() const;

	/**
	 * Check if the control action is on or off.
	 *
	 * - A vector action will return true if it's longer than 0.
	 * - A strength action will return true if it's stronger than 0.
	 * - A switch action will return true if on and false if off.
	 * - A toggle action will return false.
	 */
	bool isSwitchedOn() const;

private:
	std::string controlActionName;
	glm::vec3 controlVector;
};

} } }

#endif
