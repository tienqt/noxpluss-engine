/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ISIMULATION3D_H_
#define NOX_LOGIC_PHYSICS_ISIMULATION3D_H_

#include "physics_utils.h"
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>
#include <nox/common/types.h>
#include <btBulletDynamicsCommon.h>
#include <glm/glm.hpp>
#include <glm/gtx/quaternion.hpp>

namespace nox 
{ 

namespace app
{

namespace graphics
{
class GraphicsAssetManager3d;
}

}
	
namespace logic 
{ 

namespace world { namespace timemanipulation 
{

class ITimeManager3d;

}}

namespace actor
{

class Actor;

}

namespace physics
{

class Simulation3d
{
public:

	/**
	*  The parameters for an actor and its body
	*/
	struct TrdActorData
	{
		TrdActorData() :
			actor(nullptr),
			mainBody(nullptr)
		{}
		actor::Actor* actor;
		btRigidBody* mainBody;
	};
	/**
	*  The parameters that are sent to developer when collision happens
	*/
	struct CollisionParameters
	{
		CollisionParameters() :
			actorOne(nullptr),
			actorTwo(nullptr)
		{};
		std::vector<glm::vec3> collisionPoints;
		glm::vec3 sumNormalForce;
		glm::vec3 sumFrictionForce;
		TrdActorData* actorOne;
		TrdActorData* actorTwo;
	};
	/**
	*  Enum for what callback type it is.
	*  A callback for when a collision starts or stops
	*/
	enum CALLBACKTYPE
	{
		STOPCALLBACK,
		STARTCALLBACK
	};

	virtual ~Simulation3d();

	/**
	*  Enables or disable collision event broadcasting instead of std::functions  
	*
	*  @param enabled or disabled
	*/
	virtual void enableCollisionEvent(bool enabled) = 0;

	/**
	 *  Handles the updating of the world.
	 *  By stepping the physics using the deltaTime.
	 *  @param deltaTime The time since the last update.
	 */
	virtual void onUpdate(const Duration& deltaTime) = 0;
	
	/**
	*  Create the physics body for an actor
	*  @param the actor that will recieve the body
	*  @param the parameters for the body, comes from the json file
	*  @return if it failed or not
	*/
	virtual bool createActorBody(actor::Actor* actor, const BulletBodyDefinition& bodyDefinition) = 0;

	/**
	*
	*  @param
	*  @param
	*  @param
	*  @param
	*/
	virtual actor::Actor *findActorRayIntersectingPoint(glm::vec2 screenPosition, float zDistance, glm::vec2 windoWSize, glm::mat4x4& viewProjectionMatrix) = 0;

	/**
	*  Check if degbug rendering is enabled or not
	*/
	virtual bool getDebugRenderingEnabled() = 0;

	/**
	*  Clear the force and torque for an actor
	*  @param the id for the actor
	*/
	virtual void clearForces(const actor::Identifier& actor) = 0;

	/**
	*
	*  @param
	*  @param
	*/
	virtual void setSleepingThresholds(const actor::Identifier& actorId, glm::vec2 angAndLin) = 0;

	/**
	*  Set new linear velocity for an actor
	*  @param The id for the actor
	*  @param the new linear velocity
	*/
	virtual void setLinearVelocity(const actor::Identifier& actorId, glm::vec3 linearVelocity) = 0;

	/**
	*  Set the new angular velocity
	*  @param the id for the actor
	*  @param the new angular velocity
	*/
	virtual void setAngularVelocity(const actor::Identifier& actorId, glm::vec3 angularVelocity) = 0;
	
	/**
	*  Set how much the angle is to change on an actor
	*  @param the actor id
	*  @param the angle factor
	*/
	virtual void setAngularFactor(const actor::Identifier& actorId, float factor) = 0;
	
	/**
	*  Set new transformation for an actor
	*  @param the actor id
	*  @param the new position
	*  @param the new rotation
	*  @param the new scale
	*/
	virtual void setTransform(const actor::Identifier& actorId, glm::vec3 position, glm::quat rotation, glm::vec3 scale) = 0;
	
	/**
	*  Set the position of an actor
	*  @param the actor ID
	*  @param the new position
	*/
	virtual void setPosition(const actor::Identifier& actorId, const glm::vec3 position) = 0;
	
	/**
	* Set the time manipulation manager. Required to for manipulation.
	*  @param the time manager
	*/
	virtual void setTimeManager(world::timemanipulation::ITimeManager3d* timeManager) = 0;

	/**
	*  set anisotropic friction for an actor
	*  @param the actor ID
	*  @param the new anisotropic friction to apply
	*/
	virtual void setAnisotropicFriction(const actor::Identifier& actorId, const glm::vec3& anisotropicFriction) = 0;
	
	/**
	*  set transformation on the center of mass for an actor
	*  @param the actor ID
	*  @param the new transformation
	*/
	virtual void setCenterOfMassTransform(const actor::Identifier& actorId, const btTransform& xform) = 0;
	
	/**
	*  Set flags for collisions for an actor
	*  @param the actor ID
	*  @param the flags
	*/
	virtual void setCollisionFlags(const actor::Identifier& actorId, int flags ) = 0;
	
	/**
	*  Set damping for an actor
	*  @param the actor ID
	*  @param the linear damping to be set
	*  @param the angular damping to be set
	*/
	virtual void setDamping(const actor::Identifier& actorId, float linearDamping, float angularDamping) = 0;
	
	/**
	*  Set friction for an actor
	*  @param the actor ID
	*  @param the friction to be set
	*/
	virtual void setFriction(const actor::Identifier& actorId, float friction) = 0;
	
	/**
	*  Set gravity for the actor, there allready is gravity in the world however
	*  @param the actor ID
	*  @param the gravity to be set
	*/
	virtual void setGravity(const actor::Identifier& actorId, const glm::vec3& acceleration) = 0;
	
	/**
	*  set to enable or disable collision detection for an actor
	*  @param the actor ID
	*  @param to ignore or not
	*/
	virtual void setIgnoreCollisionCheck(const actor::Identifier& actorId, bool ignore ) = 0;
	
	/**
	*  Set interpolation angular velocity for an actor
	*  @param the actor ID
	*  @param the angular velocity
	*/
	virtual void setInterpolationAngularVelocity(const actor::Identifier& actorId, glm::vec3& angularVelocity) = 0;
	
	/**
	*  Set interpolation linear velocity for an actor
	*  @param the actor ID
	*  @param the linear velocity
	*/
	virtual void setInterpolationLinearVelocity(const actor::Identifier& actorId, glm::vec3& linearVelocity) = 0;
	
	/**
	*  Set the interpolated transform for an actor
	*  @param the actor ID
	*  @param the transform
	*/
	virtual void setInterpolationWorldTransform(const actor::Identifier& actorId, btTransform &transform) = 0;
	
	/**
	*  Set the linear factor for an actor
	*  @param the actor ID
	*  @param the linear factor
	*/
	virtual void setLinearFactor(const actor::Identifier& actorId, glm::vec3& linearFactor) = 0;
	
	/**
	*  Set the rolling friction for an actor
	*  @param the actor ID
	*  @param the friction
	*/
	virtual void setRollingFriction(const actor::Identifier& actorId, float friction) = 0;
	
	/**
	*  Set restitution for an actor
	*  @param the actor ID
	*  @param the restitution
	*/
	virtual void setRestitution(const actor::Identifier& actorId, float restitution) = 0;
	
	/**
	*  Add actor to the physics
	*  @param the actor ID
	*/
	virtual void startSimulation(const actor::Identifier& actorId) = 0;

	/**
	*  Remove actor to the physics
	*  @param the actor ID
	*/
	virtual void stopSimulation(const actor::Identifier& actorId) = 0;

	/**
	*  Apply a central force to an actor
	*  @param the actor ID
	*  @param the force to be applied
	*/
	virtual void applyCentralForce(const actor::Identifier& actorId, const glm::vec3& force) = 0;
	
	/**
	*  applies an impulse to the linear velocity of an actor
	*  @param the actor ID
	*  @param the impulse to be added
	*/
	virtual void applyCentralImpulse(const actor::Identifier& actorId, const glm::vec3& impulse) = 0;
	
	/**
	*  apply damping to the velocity of an actor
	*  @param the ID for actor
	*  @param  amount of time to step
	*/
	virtual void applyDamping(const actor::Identifier& actorId, float timeStep) = 0;
	
	/**
	*  Applies gravity to an actor. Be warned! There is allready gravity in the world
	*  @param the actor ID
	*/
	virtual void applyGravity(const actor::Identifier& actorId) = 0;

	/**
	*  Apply force to an actor
	*  @param ID for actor
	*  @param amount of force
	*  @param the relative position
	*/
	virtual void applyForce(const actor::Identifier& actorId, const glm::vec3& force, const glm::vec3& relativePosition) = 0;
	
	/**
	*  Apply impulse to an actor
	*  @param ID for actor
	*  @param amount of impulse
	*  @param the relative position 
	*/
	virtual void applyImpulse(const actor::Identifier& actorId, const glm::vec3& impulse, glm::vec3& relativePosition) = 0;
	
	/**
	*  Apply torque to an actor
	*  @param ID for actor
	*  @param amount of torque
	*/
	virtual void applyTorque(const actor::Identifier& actorId, const glm::vec3& torque) = 0;
	
	/**
	*  Apply a torque impulse on an actor
	*  @param ID for an actor
	*  @param the torque impulse
	*/
	virtual void applyTorqueImpulse(const actor::Identifier& actorId, const glm::vec3& torque) = 0;

	/**
	*  Retrieve the total force affecting an actor
	*  @param the ID for an actor
	*  @return the total force
	*/
	virtual glm::vec3 getTotalForces(const actor::Identifier& actorId) = 0;

	/**
	*  Retrieve the torque affecting an actor
	*  @param the ID for an actor
	*  @return torque
	*/
	virtual glm::vec3 getTotalTorque(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the linear velocity affecting an actor
	*  @param the ID for an actor
	*  @return the linear velocity
	*/
	virtual glm::vec3 getLinearVelocity(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the angular velocity affecting an actor
	*  @param the ID for an actor
	*  @return the angular velocity
	*/
	virtual glm::vec3 getAngularVelocity(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the angular damping affecting an actor
	*  @param the ID for an actor
	*  @return the angular damping
	*/
	virtual float getAngularDamping(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the angular factor for an actor
	*  @param the ID for an actor
	*  @return the angular factor for an actor
	*/
	virtual glm::vec3 getAngularFactor(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the angular sleeping threshold for an actor
	*  @param the ID for an actor
	*  @return the angular treshold for an actor
	*/
	virtual float getAngularSleepingThreshold(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the anisotropic friction affecting an actor
	*  @param the ID for an actor
	*  @return the anisotropic friction for an actor
	*/
	virtual glm::vec3 getAnisotropicFriction(const actor::Identifier& actorId) = 0;

	/**
	*  Retrieve the position of the center of mass for an actor
	*  @param the ID for an actor
	*  @return the position of the center of mass for an actor
	*/
	virtual glm::vec3 getCenterOfMassPosition(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the friction affecting an actor
	*  @param the ID for an actor
	*  @return returns the friction for an actor
	*/
	virtual float getFriction(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the transform object for the center of mass for an actor. 
	*  @param the ID for an actor
	*  @return returns the transformation object for the center of mass for an actor.
	*/
	virtual btTransform getCenterOfMassTransform(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the different collisionflags for an actor
	*  @param the ID for an actor
	*  @return the collisionflags for an actor
	*/
	virtual int getCollisionFlags(const actor::Identifier& actorId) = 0;
	/**
	*  Retrieve the gravity affecting an actor, remember there is a gravity set in the world too not returned here.
	*  @param the ID for an actor
	*  @return the gravity affecting an actor
	*/
	virtual glm::vec3 getGravity(const actor::Identifier& actorId) = 0;

	/**
	*  Retrieve the interpolated angular velocity for an actor
	*  @param the ID for an actor
	*  @return the interpolated angular velocity for an actor
	*/
	virtual glm::vec3 getInterpolationAngularVelocity(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the interpolated linear velocity for an actor
	*  @param the ID for an actor
	*  @return the interpolated linear velocity for an actor
	*/
	virtual glm::vec3 getInterpolationLinearVelocity(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the interpolated transform for an actor
	*  @param the ID for an actor
	*  @return the interpolated transform for an actor
	*/
	virtual btTransform getInterpolationWorldTransform(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the linear damping for an actor
	*  @param the ID for an actor
	*  @return the linear damping for an actor
	*/
	virtual float getLinearDamping(const actor::Identifier& actorId) = 0;

	/**
	*  Retrieve the linear factor for an actor
	*  @param the ID for an actor
	*  @return the linear factor for an actor
	*/
	virtual glm::vec3 getLinearFactor(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the orientation of an actor
	*  @param the ID for an actor
	*  @return the orientation of an actor
	*/
	virtual glm::quat getOrientation(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the restitution of an actor
	*  @param the ID for an actor
	*  @return the restitution an actor has.
	*/
	virtual float getRestitution(const actor::Identifier& actorId) = 0;
	
	/**
	*  Retrieve the rolling friction
	*  @param the ID for an actor
	*  @return the rolling friction
	*/
	virtual float getRollingFriction(const actor::Identifier& actorId) = 0;

	/**
	*  Check if the actor has been added to the physics world
	*  @param the ID for an actor
	*  @return true or false if it is in the world
	*/
	virtual bool isInWorld(const actor::Identifier& actorId) = 0;
	
	/**
	*  Check if the actor is a kinematic object (not implemented for nox yet)
	*  @param the ID for an actor
	*  @return true or false if it is a kinematic object
	*/
	virtual bool isKinematicObject(const actor::Identifier& actorId) = 0;
	
	/**
	*  Check if the object is a static object
	*  @param the ID for an actor
	*  @return true or false if it is a static object
	*/
	virtual bool isStaticObject(const actor::Identifier& actorId) = 0;
	
	/**
	*  Check if the object is static or a kinematic object
	*  @param the ID for an actor
	*  @return true or false if it is a static or kinematic object
	*/
	virtual bool isStaticOrKinematicObject(const actor::Identifier& actorId) = 0;
	
	/**
	*  
	*  @param the ID for an actor
	*  @return
	*/
	virtual float distanceAboveGround(const actor::Identifier& actorId) = 0;

	/**
	*  Log the physics of an actor
	*  @param the ID for an actor
	*  @param the struct used for filling the information
	*/
	virtual void serializePhysicsBody(const nox::logic::actor::Identifier &actorId, nox::logic::world::timemanipulation::PhysicalProperties* physicsState) = 0;

	/**
	*  Set the graphics asset manager for this class
	*  @param the asset manager
	*/
	virtual void setGraphicsAssetManager(nox::app::graphics::GraphicsAssetManager3d* assetManager) = 0;
	
	/**
	*  Retrieve the asset manager
	*  @return the asset manager
	*/
	virtual nox::app::graphics::GraphicsAssetManager3d* getAssetManager() = 0;
	
	/**
	*  Set a function that is to be run when an actor collides
	*  @param the ID for an actor
	*  @param the function
	*  @param the callback type
	*  @return id the function recieved in the map for the passed actor
	*/
	virtual int setCallbackForActor(const nox::logic::actor::Identifier &id, const std::function<void(const CollisionParameters&)> CollisionCallback, CALLBACKTYPE type) = 0;
	
	/**
	*  Remove a callback for an actor
	*  @param the ID for an actor
	*  @param the callback type
	*  @param the id recieved when callback was added
	*  @return if it was removed or not
	*/
	virtual bool removeCallbackForActor(const nox::logic::actor::Identifier id, CALLBACKTYPE type, int callbackNr) = 0;

	
};

} } }

#endif
