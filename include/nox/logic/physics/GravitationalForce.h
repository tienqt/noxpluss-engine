/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_GRAVITATIONALFORCE_H_
#define NOX_LOGIC_PHYSICS_GRAVITATIONALFORCE_H_

#include <functional>
#include <glm/glm.hpp>

namespace nox { namespace logic { namespace physics
{

/**
 * A gravitational force pulling other objects.
 */
class GravitationalForce
{
public:
	/**
	 * Body with a mass an position.
	 */
	struct Body
	{
		Body():
			mass(0.0f)
		{}

		Body(const glm::vec2& position, const float mass):
			position(position),
			mass(mass)
		{}

		glm::vec2 position;
		float mass;
	};

	/**
	 * Limit the distance between two bodies.
	 */
	class DistanceLimit
	{
	public:
		//! How to limit the distance when distance is outside limit.
		enum class Type
		{
			CLAMP,	//! Clamp the distance to the limit.
			BORDER	//! Don't apply gravity.
		};

		DistanceLimit();

		/**
		 * Check if a distance is inside the border of the limit.
		 *
		 * Does not have anything to say if both min and max limit Type is Type::CLAMP.
		 */
		bool insideBorder(const float distance) const;

		/**
		 * Get the limited distance.
		 */
		float limitDistance(const float distance) const;

		/**
		 * Disable minimum limit.
		 * The distance won't be limited by a minimum range.
		 */
		void setMinLimitOff();

		/**
		 * Enable minimum limit.
		 * @param distance Distance to limit at.
		 * @param limitType What to do when outside limit.
		 */
		void setMinLimitOn(const float distance, const Type limitType);

		/**
		 * Disable maximum limit.
		 * The distance won't be limited by a maximum range.
		 */
		void setMaxLimitOff();

		/**
		 * Enable maximum limit.
		 * @param distance Distance to limit at.
		 * @param limitType What to do when outside limit.
		 */
		void setMaxLimitOn(const float distance, const Type limitType);

	private:
		bool limitMin;
		float minDistance;
		Type minLimitType;

		bool limitMax;
		float maxDistance;
		Type maxLimitType;
	};

	/**
	 * How to calculate the force applied on a body.
	 */
	enum class CalculationType
	{
		NEWTON,		//!< Newton's law of universal gravitation.
		CUSTOM		//!< Set a custom function with setCustomForceCalculationFunction().
	};

	/**
	 * Function called to calculate force.
	 * param1: Body pulling.
	 * param2: Body being pulled.
	 */
	typedef std::function<float (const Body&, const Body&, const float)> CalculationFunction;

	GravitationalForce();

	void setPosition(const glm::vec2& position);
	void setMass(const float mass);
	void setForceCalculationType(const CalculationType& type);
	void setCustomForceCalculationFunction(const CalculationFunction& function);
	void setDistanceLimit(const DistanceLimit& limit);

	float getMass() const;
	const glm::vec2& getPosition() const;
	glm::vec2 calculateForceOnBody(const Body& otherBody, const float deltaTime = 1.0f) const;

private:
	Body body;

	CalculationType calculationType;
	CalculationFunction calculationFunction;

	DistanceLimit distanceLimit;
};

} } }

#endif
