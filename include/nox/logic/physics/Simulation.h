/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ISIMULATION_H_
#define NOX_LOGIC_PHYSICS_ISIMULATION_H_

#include "physics_utils.h"
#include "joint.h"
#include <nox/logic/event/Event.h>
#include <nox/app/graphics/2d/GeometrySet.h>
#include <nox/util/Mask.h>
#include <nox/util/boost_utils.h>
#include <nox/common/types.h>

#include <memory>
#include <glm/glm.hpp>
#include <vector>
#include <unordered_set>
#include <functional>

namespace nox
{

namespace app
{
namespace graphics
{

class Light;

}
}

namespace logic
{

namespace actor
{

class Actor;

}

namespace physics
{

class GravitationalForce;

/**
 * Describes the way the physics should handle its world.
 */
enum class SimulationMode
{
	SIMULATED,      	//!< Physics will simulate the world and update the actor's state.
	REPRESENTATIONAL	//!< Physics will represent the world, but not simulate it.
};

struct CollisionData
{
	glm::vec2 collisionPoint;
	glm::vec2 collisionSurfaceNormal;
	float collisionImpulse;
	actor::Identifier collidingActorId;
};

class Simulation
{
public:
	/**
	 * Callback used for sensors.
	 * First parameter says if it is currently touching the body or not.
	 * Second parameter is the id of the actor that it is touching.
	 */
	typedef std::function<void (bool, actor::Identifier, unsigned int, glm::vec2)> SensorCallback;

	/**
	 * Callback used for collisions.
	 */
	typedef std::function<void (const CollisionData&)> CollisionCallback;

	/**
	 * Data about an intersection
	 */
	struct RayIntersection
	{
		RayIntersection():
			intersected(false)
		{}

		glm::vec2 position;		//!< Position of the intersection.
		glm::vec2 surfaceNormal;	//!< Normal of the surface where the intersection happened.
		bool intersected;
	};

	struct Contact
	{
		Contact():
			touchingActor(actor::Identifier())
		{}

		std::vector<glm::vec2> collisionPoints;
		glm::vec2 contactDirection;
		actor::Identifier touchingActor;
	};

	virtual ~Simulation();

	/**
	 *  Handles the updating of the world.
	 *  By stepping the physics using the deltaTime.
	 *  @param deltaTime The time since the last update.
	 */
	virtual void onUpdate(const Duration& deltaTime) = 0;

	/**
	 *  Synchronize the state of the physics with the rest of the system.
	 *  This basically updates each actor's transformation and broadcasts
	 *  and event for each.
	 */
	virtual void onSyncState(const Duration& updateAlpha) = 0;

	/**
	 * Update the lighting geometry based on the current world state.
	 */
	virtual void updateLighting() = 0;

	/**
	 * Completely reset the physics world.
	 * @pre Physics is initialized with IGamePhysics::init
	 */
	virtual void reset() = 0;

	/**
	 * Adds a callback to be called when a specific actor collides.
	 * @param actor The callback will be called when this actor collides.
	 * @param callBack the callbackfunction we want to add. The ActorId parameter is the actor collided with.
	 */
	virtual void setActorCollisionCallback(const actor::Identifier& actor, CollisionCallback callBack) = 0;
	/**
	 * Adds the actor to the map of actors that are sticky if they hit a wall with enough force.
	 * How easy the actor will stick also depends on the mass of the body.
	 * @param actor The ID of the actor who's body we are adding to the map.
	 * @param stickiness How sticky the actor should be in range [0,1] where 0 is never stick, and 1 is always stick.
	 */
	virtual void setSticky(const actor::Identifier& actor, float stickiness) = 0;
	
	/**
	 * make an actor no longer stick.
	 * @param actor ID of actor to remove.
	 */
	virtual void unsetSticky(const actor::Identifier& actor) = 0;
	
	/**
	 * Removes the actors body and callback from the bullet map.
	 * @param actor the actor to be removed.
	 */
	virtual void removeActorCollisionCallback(const actor::Identifier& actor) = 0;
	
	/**
	* Sets the sensor state of the actors bodies fixtures.  
	*/
	virtual void setSensor(const actor::Identifier& actor, bool state) = 0;

	/**
	 * Applies the passed in force to the actor's body.
	 * @param actor Id of actor to apply on.
	 * @param force Force to apply.
	 */
	virtual void applyForce(const actor::Identifier& actor, const glm::vec2& force) = 0;

	virtual void applyForce(const actor::Identifier& actor, const glm::vec2& force, const glm::vec2& position) = 0;

	/**
	 * Applies the passed in impulse to the actor's body.
	 * @param actor Id of actor to apply on.
	 * @param impulse Impulse to apply.
	 */
	virtual void applyImpulse(const actor::Identifier& actor, const glm::vec2& impulse) = 0;

	/**
	 * Applies the passed in torque to the actor's body.
	 * @param actor Id of actor to apply on.
	 * @param torque Torque to apply.
	 */
	virtual void applyTorque(const actor::Identifier& actor, const float torque) = 0;

	/**
	 * Add a body for an actor.
	 * If bodyName is not specified, this body will be the actor's main body.
	 * The main body is used to represent the transformation of the actor, and transform events
	 * broadcasted to the system will contain the transformation of this body.
	 * @param actor Id of actor to add body to. Must already exist in physics (see addActor).
	 * @param bodyDefinition Definition of the body to create.
	 */
	virtual void createActorBody(actor::Actor* actor, const BodyDefinition& bodyDefinition) = 0;

	/**
	 * Remove body from an actor.
	 * @param actor Id of actor to remove body from.
	 */
	virtual void removeActorBody(const actor::Identifier& actor) = 0;

	/**
	 * Add a sensor to the world.
	 * Make sure to remove sensor when done with it, using removeSensor.
	 * @param actor ID of actor to add sensor for.
	 * @param sensorShape The shape of the sensor.
	 * @param callback Function called when sensor gains contact, or loses contact.
	 * @return ID of the sensor.
	 */
	virtual int addActorSensor(const actor::Identifier& actor, const BodyShape& sensorShape, SensorCallback callback) = 0;

	/**
	 * Remove a sensor from an actor.
	 * @param sensorId ID of the sensor to remove.
	 */
	virtual void removeActorSensor(int sensorId) = 0;

	/**
	 * Set the shape of an actor's body.
	 * The body must already exist.
	 * @param actor Id of actor to set shape on.
	 * @param shape Shape to set.
	 */
	virtual void setActorBodyShape(const actor::Identifier& actor, const BodyShape& shape) = 0;

	/**
	 * Set the position of an actor.
	 * @param actorId Id of actor to set position for.
	 * @param actorPosition Position to set.
	 */
	virtual void setActorPosition(const actor::Identifier& actorId, const glm::vec2& actorPosition) = 0;
	
	/**
	 * Set the velocity of the actor directly.
	 * @param actorId Id of actor to set velocity.
	 * @param actorVelocity the velocity to set.
	 */
	virtual void setActorVelocity(const actor::Identifier& actorId, const glm::vec2& actorVelocity) = 0;
	
	/**
	 * Set the multiple that the gravity affects the actor
	 * @param actorId Id of actor to set the multiple.
	 * @param gravityMultiplier the multiple to set
	 */
	virtual void setActorGravityMultiplier(const actor::Identifier& actorId, const float gravityMultiplier) = 0;
	
	/**
	 * Set the linear dampening of the actor
	 * @param actorId the id of the actor.
	 * @param linearDampening the new dampening.
	 */
	virtual void setActorLinearDampening(const actor::Identifier& actorId, const float linearDampening) = 0;
	
	/**
	 * Set the angular dampening of the actor
	 * @param actorId the id of the actor.
	 * @param angularDampening the new dampening.
	 */
	virtual void setActorAngularDampening(const actor::Identifier& actorId, const float angularDampening) = 0;
	
	/**
	 * Set the rotation of an actor.
	 * @param actorId the actor whos rotation to set.
	 * @param actorRotation the rotation to set.
	 */
	virtual void setActorRotation(const actor::Identifier& actorId, const float actorRotation) = 0;

	/**
	 * Set the restitution of the actor.
	 * @param actorId, the actor whos restitution to set.
	 * @param restitution, how much energy the actor should conserve.
	 */
	virtual void setRestitution(const actor::Identifier& actorId, const float actorRestitution) = 0;

	/**
	 * Applies a impulse in a radius from a point.
	 * Will affect all the objects that are in the radius of the explosive.
	 * @param position the positon of the explosion;
	 * @param radius the radi of the explosion;
	 * @param impulse the impulse that is applied, decreases with the distance.
	 */
	virtual void applyExplosiveImpulseInRadius(const glm::vec2& position, const float radius, const float impulse) = 0;
	
	/**
	 * Sets the body to a fixed rotation.
	 * @param actor ID of actor to apply on.
	 * @param status If the rotation should be fixed or not.
	 */
	virtual void setFixedRotation(const actor::Identifier& actor, bool status) = 0;
	
	virtual void activateActorBody(const actor::Identifier& actorId) = 0;

	virtual void deactivateActorBody(const actor::Identifier& actorId) = 0;

	/**
	 * Check if a body has fixed rotation.
	 * @param actor ID of actor to apply on.
	 */
	virtual bool hasFixedRotation(const actor::Identifier& actor) const = 0;

	virtual void setActorFriendGroup(const actor::Identifier& actor, const actor::Identifier& actorToBefriend) = 0;
	
	virtual void setActorContactCallback(const actor::Identifier& actor, SensorCallback callback) = 0;

	virtual bool removeJoint(int jointId) = 0;
	
	virtual int createWeldJoint(const WeldJointDefinition& jointDef) = 0;
	
	virtual int createRevoluteJoint(const RevoluteJointDefinition& jointDef) = 0;

	virtual int createPrismaticJoint(const PrismaticJointDefinition& jointDef) = 0;

	virtual void setPrismaticMotorSpeed(const int jointId, const float motorSpeed) = 0;

	virtual void setPrismaticLimits(const int jointId, const float lower, const float upper) = 0;

	virtual float getPrismaticJointTranslation(const int jointId) const = 0;

	virtual int createTargetJoint(const TargetJointDefinition& definition) = 0;
	virtual void setTargetJointTargetPosition(const int jointId, const glm::vec2& target) = 0;

	virtual float getMassOfStrongestGravityPullBody(const actor::Identifier& actorId) = 0;
	
	virtual float getDistanceToStrongestGravityPullBody(const actor::Identifier& actorId) = 0;
	
	/**
	 * Returns the mass of the actor
	 * @param actorId the actor to find the mass for.
	 * @returns the mass of the actor.
	 */
	virtual float getMassOfActor(const actor::Identifier& actorId) = 0;
	
	/**
	 * Used to set the actor to awake or sleeping
	 * @param actorId the actor
	 * @param state true if the actor should be set to awake.
	 */
	virtual void setActorAwakeState(const actor::Identifier& actorId, bool state) = 0;
	
	/**
	 * Set the mode that the physics should run in.
	 * @param mode The mode.
	 */
	virtual void setMode(SimulationMode mode) = 0;

	/**
	 *  Adds a light to the light calculation.
	 */
	virtual void addLight(std::shared_ptr<app::graphics::Light> light) = 0;

	/**
	 *  Removes a light from the light calculation.
	 */
	virtual void removeLight(std::shared_ptr<app::graphics::Light> light) = 0;

	/**
	 * Create a gravitational force pulling world bodies.
	 * @return Pointer to force created. Valid as long as it is not removed and the physics is not destroyed.
	 */
	virtual GravitationalForce* createGravitationalForce() = 0;

	/**
	 * Destroy a gravitational force created with createGravitationalForce().
	 * @param force Pointer to force to remove.
	 * @return true if it was removed, false if it was not found.
	 */
	virtual bool removeGravitationalForce(const GravitationalForce* force) = 0;

	/**
	 * Get the gravitational pull force applied to an actor.
	 * @param actorId ID of actor to get pull on.
	 */
	virtual glm::vec2 getGravitationalPullOnActor(const actor::Identifier& actorId) const = 0;

	/**
	 * Get the strongest gravitational pull affecting an actor.
	 * @param actorId ID of actor to get pull for.
	 */
	virtual glm::vec2 getStrongestGravitationalPullOnActor(const actor::Identifier& actorId) const = 0;

	/**
	 * Find the direction of the strongest gravitational pull affecting a point in space.
	 *
	 * @param point Point in space affected by pull.
	 * @param pointMass Mass of point being affected.
	 * @return The direction of the strongest pull.
	 */
	virtual float findStrongestGravitationalPullDirectionFromPoint(const glm::vec2& point, const float pointMass) const = 0;

	virtual glm::vec2 findGravitationalForceFromPoint(const glm::vec2& point, const float pointMass) const = 0;

	/**
	 * Get the velocity of an actor.
	 * @param actor Id of actor to get velocity of.
	 * @return The velocity of the actor. Velocity is 0 if actor not found.
	 */
	virtual glm::vec2 getVelocity(const actor::Identifier& actor) const = 0;
	
	virtual glm::vec2 getActorCenterOfMass(const actor::Identifier& actorId) const = 0;

	virtual glm::vec2 getPostition(const actor::Identifier& actorId) const = 0;

	virtual float getRotation(const actor::Identifier& actorId) const = 0;

	/**
	 * Get the angular velocity of an actor
	 * @param actor Id of actor to get angular velocity of.
	 * @return The angular velocity of the actor. The Angular velocity is 0 if actor not found.
	 */
	virtual float getAngularVelocity(const actor::Identifier& actor) const = 0;
	
	virtual std::vector<Contact> getActorContacts(const actor::Identifier& actorId) const = 0;

	/**
	 * Find the actor that overlaps a point.
	 * @param point Point to check.
	 * @param type, the body type we are checking for.
	 * @param ignoreActorId, actor(s) the intersection should ignore.
	 * @return Pointer to actor overlapping point, or nullptr if nothing overlaps.
	 */
	virtual actor::Actor* findActorIntersectingPoint(const glm::vec2& point, const util::Mask<PhysicalBodyType>& type, const std::vector<actor::Identifier>& ignoreActorId = std::vector<actor::Identifier>()) const = 0;

	/**
	 * Find all actors whose body AABB intersects a specified AABB.
	 *
	 * @note For actors without a body, use findEmptyActorsWithinAabb(const Box<glm::vec2>&, const Mask<BodyState>&)
	 *
	 * @param aabbRect AABB box to look for actor intersections.
	 * @param bodiesToFind What type of bodies to find.
	 * @return Actors found intersecting.
	 */
	virtual std::vector<actor::Actor*> findActorsIntersectingAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const = 0;

	/**
	 * Find all actors whose body position is within a specified AABB.
	 *
	 * @note For actors with a body, use findActorsIntersectingAabb(const Box<glm::vec2>&, const Mask<BodyState>&)
	 *
	 * @param aabbRect AABB box to look for actor intersections.
	 * @param bodiesToFind What type of bodies to find.
	 * @return Actors found within.
	 */
	virtual std::vector<actor::Actor*> findEmptyActorsWithinAabb(const math::Box<glm::vec2>& aabbRect, const util::Mask<BodyState>& bodiesToFind) const = 0;

	virtual std::vector<actor::Actor*> findActorsIntersectingShape(const BodyShape& shape, const glm::vec2& position, const float rotation) const = 0;

	/**
	 *  Checks if there is an object intersecting the line between two points.
	 *  Casts a ray between the two points to check if there is anything blocking
	 *  the straight path/line between two points.
	 */
	virtual bool checkForIntersectionBetween(const glm::vec2& point1, const glm::vec2& point2) = 0;

	/**
	 *
	 * @param rayStart Point where ray is cast from.
	 * @param rayEnd Point where ray is cast to.
	 * @param includeInactive If inactive bodies should be included in ray casting.
	 * @return The intersection.
	 */
	virtual RayIntersection findFirstRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive = false) = 0;
	virtual RayIntersection findFirstStaticRayIntersection(const glm::vec2& rayStart, const glm::vec2& rayEnd, bool includeInactive = false) = 0;


	virtual bool checkForStaticIntersectionBetween(const glm::vec2& point1, const glm::vec2& point2) = 0;
	virtual float checkForIntersectionAndGetMassOfObjectsBetween(const glm::vec2& point1, const glm::vec2& point2, const std::unordered_set<actor::Identifier>* actorsIgnored = nullptr) = 0;
	virtual bool isPointInsideFixture(const glm::vec2& point) const = 0;
	virtual float getMassOfObjectOverlappingPoint(const glm::vec2& point, const std::unordered_set<actor::Identifier>* actorsIgnored = nullptr) const = 0;
	virtual bool isPointShadowed(const glm::vec2& lightPoint, const glm::vec2& pointToCheck) const = 0;
	virtual float findLengthOfLineBeforeHitObject(const glm::vec2& lineStart, const::glm::vec2& lineEnd, std::vector<actor::Identifier> ignoreActorId, const util::Mask<PhysicalBodyType>& mask) const = 0;
	virtual bool actorAabbIntersectsAabb(const actor::Identifier& actorId, const math::Box<glm::vec2>& aabb) const = 0;
	virtual bool isActorOverlappingActor(const actor::Identifier& actorId) = 0;

	virtual bool isActorOverlappingAnotherActor(const actor::Identifier& firstActorId, const actor::Identifier& secondActorId) = 0;
	/**
	 * Get all the actors that overlaps a specific actor. 
	 * @param actorId of the actor that wants to check for other actors.
	 * @return vector of the actors that are overlapping.
	 */
	virtual std::vector<actor::Actor*> getActorsOverlapping(const actor::Identifier& actorId) = 0;
};

} } }

#endif
