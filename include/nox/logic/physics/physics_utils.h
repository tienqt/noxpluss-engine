/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_
#define NOX_LOGIC_PHYSICS_PHYSICSUTILS_H_

#include <json/json.h>
#include <glm/glm.hpp>
#include <nox/util/boost_utils.h>
#include <nox/util/math/Box.h>
#include <nox/util/Mask.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/app/graphics/Mesh3d.h>
#include <vector>

namespace nox { namespace logic { namespace physics {

enum class PhysicalBodyType
{
	STATIC = (1 << 0),
	KINEMATIC = (1 << 1),
	DYNAMIC = (1 << 2)
};

enum class BodyState
{
	ACTIVE = (1 << 0),
	INACTIVE = (1 << 1)
};

enum class ShapeType
{
	CIRCLE,
	POLYGON,
	RECTANGLE,
	NONE
};

struct BodyShape
{
	BodyShape():
		circleRadius(0.0f),
		type(ShapeType::NONE)
	{}

	math::Box<glm::vec2> box;
	glm::vec2 position;
	util::BoostMultiPolygon polygons;
	float circleRadius;
	ShapeType type;
};

enum class ShapeType3d
{
	PLANE,
	SPHERE,
	BOX,
	CYLINDER,
	CONE,
	CAPSULE,
	CONVEXHULLSHAPE,
	CONCAVEHULLSHAPE,
	COMPOUNDSHAPE,
	GIMPACTTRIANGLEMESHSHAPE,
	HEIGHTFIELD,
	SCALEDBVHTRIANGLE,
	BVHTRIANGLE,
	NONE

};

struct BodyShape3d
{
	BodyShape3d() :
		circleRadius(0.0f),
		type(ShapeType3d::NONE),
		size(0.0f, 0.0f, 0.0f),
		planeOffset(0.0f, 1.0f, 0.0f),
		planeConstant(1.0f),
		height(0),
		enableDynamicAabbTree(true),
		useQuantizedAabbCompression(false),
		optimizedBVH(true)
	{}
	float circleRadius;
	float planeConstant;
	float height;
	bool useQuantizedAabbCompression;
	bool optimizedBVH;
	bool enableDynamicAabbTree;

	ShapeType3d type;
	glm::vec3 planeOffset;
	glm::vec3 size;
};

struct BodyDefinition
{
	BodyDefinition():
		linearVelocity(glm::vec2(0.0f)),
		angularVelocity(0.0f),
		angle(0.0f),
		angularDamping(0.0f),
		linearDamping(0.0f),
		density(0.0f),
		friction(0.2f),
		restitution(0.0),
		bullet(false),
		sensor(false),
		depth(9),
		bodyType(PhysicalBodyType::DYNAMIC),
		fixedRotation(false),
		gravityMultiplier(1.0f),
		active(true),
		collisionCategory("")
	{}

	glm::vec2 position;
	glm::vec2 linearVelocity;

	float angularVelocity;
	float angle;
	float angularDamping;
	float linearDamping;
	float density;
	float friction;
	float gravityMultiplier;

	float restitution;
	bool bullet;
	bool sensor;
	bool fixedRotation;
	bool active;

	unsigned int depth;

	PhysicalBodyType bodyType;
	
	BodyShape shape;

	actor::Identifier friendActorId;
	std::string collisionCategory;
	std::vector<std::string> maskCategories;
};

struct BulletBodyDefinition
{
	BulletBodyDefinition() :
		position(glm::vec3(0.0f)),
		linearVelocity(glm::vec3(0.0f)),
		mass(0.0f),
		bodyType(PhysicalBodyType::DYNAMIC),
		angularDamping(0.0f),
		linearDamping(0.0f),
		angularVelocity(glm::vec3(0.0f)),
		restitution(0.0f),
		friction(0.0f),
		collisionFlag(0),
		name(""),
		meshDatas(nullptr),
		quick_cd(false),
		scale(glm::vec3(1)),
		shapes()
	{}

	std::string name;
	std::vector<BulletBodyDefinition> shapes;
	PhysicalBodyType bodyType;
	BodyShape3d shape;

	float angularDamping;
	float angularFactor;
	float linearDamping;
	float restitution;
	float friction;
	float mass;

	glm::vec2 sleepingThresholds;

	int collisionFlag;

	glm::vec3 position;
	glm::vec3 linearVelocity;
	glm::vec3 angularVelocity;
	glm::vec3 scale;

	std::shared_ptr<nox::app::graphics::Mesh3d> meshDatas;
	bool quick_cd;
};

//struct HeightMap
//{
//	HeightMap() :
//		heightStickWidth(0),
//		heightStickLength(0),
//		heightfieldData(nullptr),
//		heightScale(0),
//		minHeight(0),
//		maxHeight(0),
//		upAxis(1),
//		heightDataType(PHY_FLOAT),
//		flipQuadEdges(false)
//	{}
//	int heightStickWidth;
//	int heightStickLength;
//	const void *heightfieldData;
//	btScalar heightScale;
//	btScalar minHeight;
//	btScalar maxHeight;
//	int upAxis;
//	PHY_ScalarType heightDataType;
//	bool flipQuadEdges;
//};

util::Mask<PhysicalBodyType> staticAndKinematicBodyTypes();
util::Mask<PhysicalBodyType> staticAndDynamicBodyTypes();
util::Mask<PhysicalBodyType> kinematicAndDynamicBodyTypes();
util::Mask<PhysicalBodyType> allBodyTypes();

util::Mask<BodyState> allBodyStates();

/**
 * Parse body shape data from a JSON value.
 *
 * # JSON Shape Properties
 * - __type__:string - What type of shape is it. Can be "circle", "box", "polygon", and "none". A "none" shape will have no shape.
 *
 * ## Circle Shape Properties
 * - __radius__:real - The radius of the circle. Default 1.
 *
 * ## Box Shape Properties
 * - __lowerBound__:vec2 - The bottom left corner of the box. Default vec2(0, 0).
 * - __upperBound__:vec2 - The top right corner of the box. Default vec2(1, 1).
 *
 * ## Polygon Shape Properties
 * - __vertexList__:array[vec2] - Vertices defining the polygon, in counter-clockwise order. May be concave. Must have at leat three.
 *
 * @param shapeJson JSON value to parse.
 * @param[out] shape Shape to output parsed data to.
 * @return If parsing succeeded or failed.
 */
bool parseBodyShapeJson(const Json::Value& shapeJson, BodyShape& shape);

BodyShape transformShape(const BodyShape& shape, const glm::vec2& position, const glm::vec2& scale, float rotation);

/**
 * Create a physics shape that is formed as a box.
 * The box will be positioned around [0,0].
 * @param size The size of the box.
 */
BodyShape makeBoxShape(const glm::vec2& size);

} } }

#endif
