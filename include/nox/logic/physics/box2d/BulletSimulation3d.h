/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_BULLETSIMULATION3D_H_
#define NOX_LOGIC_PHYSICS_BULLETSIMULATION3D_H_

#include <nox/app/log/Logger.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/Event.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/physics/box2d/BulletDebugDraw3d.h>
#include <nox/app/graphics/GraphicsAssetManager3d.h>
#include <nox/util/Booker.h>
#include <nox/logic/actor/Actor.h>
#include <memory>
#include <unordered_map>
#include <set>
#include <functional>
#include <LinearMath/btMatrix3x3.h>

namespace nox { namespace logic {

namespace world
{

namespace timemanipulation
{
class IWorldLogger3d;
}

}

class IContext;

namespace physics
{
	
/**
 *  Handles physics simulation using the Bullet engine.
 */


class BulletSimulation3d : public Simulation3d, public event::IListener
{
public:
	


	/**
	 *  Initiates the Bullet simulator.
	 */
	BulletSimulation3d(IContext* logicContext);

	virtual ~BulletSimulation3d();
	
	/**
	*  Set the logger for this class
	*  @param the logger
	*/
	void setLogger(app::log::Logger logger);
	
	void onUpdate(const Duration& deltaTime) override;
	
	/**
	*  Not implemented
	*  Recieve the events and deciede what to do, used in combination with listener
	*/
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;
	
	/**
	*  Create the physics shape for an actor
	*  @param Parameters from the json file, decides what shape and its parameters
	*/
	btCollisionShape* createShape(const BulletBodyDefinition& bodyDefinition, actor::Actor* actor );
	
	/**
	*
	* Don't know yet. TODO: Write this after finishing the function.
	*
	*/

	void updateTimeManipulationConflicts(ActorPhysics3d* bodyOne, ActorPhysics3d* bodyTwo);

	/**
	*  The callback function Bullet uses when a collision happens
	*  @param the world
	*  @param amount of time to step the simulation
	*/
	static void myTickCallback(btDynamicsWorld* world, btScalar timeStep);
	
	void enableCollisionEvent(bool enabled) override;
	
	bool createActorBody(actor::Actor* actor, const BulletBodyDefinition& bodyDefinition) override;

	void clearForces(const actor::Identifier& actorId) override;

	void applyCentralForce(const actor::Identifier& actorId, const glm::vec3& force) override;

	void applyForce(const actor::Identifier& actorId, const glm::vec3& force, const glm::vec3& relativePosition) override;
	
	void applyCentralImpulse(const actor::Identifier& actorId, const glm::vec3& impulse) override;
	
	void applyImpulse(const actor::Identifier& actorId, const glm::vec3& impulse, glm::vec3& relativePosition) override;
	
	void applyDamping(const actor::Identifier& actorId, float timeStep) override;

	void applyGravity(const actor::Identifier& actorId) override;

	void applyTorque(const actor::Identifier& actorId, const glm::vec3&  torque) override;

	void applyTorqueImpulse(const actor::Identifier& actorId, const glm::vec3& torque) override;

	actor::Actor *findActorRayIntersectingPoint(glm::vec2 screenPosition, float zDistance, glm::vec2 windoWSize, glm::mat4x4& viewProjectionMatrix) override;

	void setLinearVelocity(const actor::Identifier& actorId, glm::vec3 linearVelocity) override;
	
	void setAngularVelocity(const actor::Identifier& actorId, glm::vec3 angularVelocity) override;
	
	void setAngularFactor(const actor::Identifier& actorId, float factor) override;
	
	void setSleepingThresholds(const actor::Identifier& actorId, glm::vec2 angAndLin) override;
	
	void setTransform(const actor::Identifier& actorId, glm::vec3 position, glm::quat rotation, glm::vec3 scale) override;
	
	void setAnisotropicFriction(const actor::Identifier& actorId, const glm::vec3& anisotropicFriction) override;

	void setCenterOfMassTransform(const actor::Identifier& actorId, const btTransform& xform) override;

	void setCollisionFlags(const actor::Identifier& actorId, int flags) override;

	void setDamping(const actor::Identifier& actorId, float linearDamping, float angularDamping) override;

	void setFriction(const actor::Identifier& actorId, float friction) override;

	void setGravity(const actor::Identifier& actorId, const glm::vec3& acceleration) override;

	void setIgnoreCollisionCheck(const actor::Identifier& actorId, bool ignore) override;

	void setInterpolationAngularVelocity(const actor::Identifier& actorId, glm::vec3& angularVelocity) override;

	void setInterpolationLinearVelocity(const actor::Identifier& actorId, glm::vec3& linearVelocity) override;
	
	void setInterpolationWorldTransform(const actor::Identifier& actorId, btTransform& transform) override;

	void setLinearFactor(const actor::Identifier& actorId, glm::vec3& linearFactor) override;

	void setRollingFriction(const actor::Identifier& actorId, float friction) override;

	void startSimulation(const actor::Identifier& actorId) override;

	void stopSimulation(const actor::Identifier& actorId) override;

	void setRestitution(const actor::Identifier& actorId, float restitution) override;


	glm::vec3 getTotalForces(const actor::Identifier& actorId) override;
	
	glm::vec3 getTotalTorque(const actor::Identifier& actorId) override;
	
	glm::vec3 getLinearVelocity(const actor::Identifier& actorId) override;
	
	glm::vec3 getAngularVelocity(const actor::Identifier& actorId) override;

	float getAngularDamping(const actor::Identifier& actorId) override;

	glm::vec3 getAngularFactor(const actor::Identifier& actorId) override;

	float getAngularSleepingThreshold(const actor::Identifier& actorId) override;

	glm::vec3 getAnisotropicFriction(const actor::Identifier& actorId) override;

	glm::vec3 getCenterOfMassPosition(const actor::Identifier& actorId) override;

	float getFriction(const actor::Identifier& actorId) override;

	btTransform getCenterOfMassTransform(const actor::Identifier& actorId) override;

	int getCollisionFlags(const actor::Identifier& actorId) override;

	glm::vec3 getGravity(const actor::Identifier& actorId) override;

	glm::vec3 getInterpolationAngularVelocity(const actor::Identifier& actorId) override;

	glm::vec3 getInterpolationLinearVelocity(const actor::Identifier& actorId) override;

	btTransform getInterpolationWorldTransform(const actor::Identifier& actorId) override;

	float getLinearDamping(const actor::Identifier& actorId) override;

	glm::vec3 getLinearFactor(const actor::Identifier& actorId) override;

	glm::quat getOrientation(const actor::Identifier& actorId) override;

	float getRestitution(const actor::Identifier& actorId) override;

	float getRollingFriction(const actor::Identifier& actorId) override;

	bool isInWorld(const actor::Identifier& actorId) override;

	bool isKinematicObject(const actor::Identifier& actorId) override;	
	
	bool isStaticObject(const actor::Identifier& actorId) override;

	bool isStaticOrKinematicObject(const actor::Identifier& actorId) override;

	void setGraphicsAssetManager(nox::app::graphics::GraphicsAssetManager3d* assetManager) override;
	
	nox::app::graphics::GraphicsAssetManager3d* getAssetManager() override;
	
	void setPosition(const actor::Identifier& actorId, const glm::vec3 position) override;
	
	float distanceAboveGround(const actor::Identifier& actorId) override;

	bool getDebugRenderingEnabled() override;
	
	/**
	*  Set the debug renderer for rendering shapes
	*/
	void setDebugRenderer(BulletDebugDraw3d* debugRenderer);
	
	void setTimeManager(world::timemanipulation::ITimeManager3d* timeManager) override;
	/**
	*  Convert a bullet vector to glm vector
	*/
	static glm::vec3 btVector3_to_Vec3(btVector3 const & btvec);
	
	/**
	*  Convert a glm vector to bullet vector
	*/
	static btVector3 vec3_to_btVector(glm::vec3 const & glmVec3);
	
	/**
	*  Convert a glm quaternion to bullet quaternion
	*/
	static btQuaternion quat_to_btQuaternion(glm::quat const & glmQuat);

	/**
	*  Convert a bullet quaternion to glm quaternion
	*/
	static glm::quat btQuaternion_to_quat(btQuaternion const & btQuat);

    int setCallbackForActor(const nox::logic::actor::Identifier &id, const std::function<void(const CollisionParameters&)> CollisionCallback, CALLBACKTYPE type) override;
    
	bool removeCallbackForActor(const nox::logic::actor::Identifier id, CALLBACKTYPE type, int callbackNr) override;

	void serializePhysicsBody(const nox::logic::actor::Identifier &actorId, nox::logic::world::timemanipulation::PhysicalProperties* physicsState) override;

protected:

private:
	

	mutable app::log::Logger log;

	IContext* logicContext;
	event::ListenerManager listener;

	typedef std::pair< const btRigidBody * , const btRigidBody  * > CollisionPair;
	typedef std::set< CollisionPair > CollisionPairs;

	CollisionPairs oldTickCollisionPairs;

	std::unordered_map<std::string, btCollisionShape*> reuseShapeList;
	std::unordered_map<nox::logic::actor::Identifier, std::unordered_map<int, std::function<void(const CollisionParameters&)>>> collisionCallbackListStart;
	std::unordered_map<nox::logic::actor::Identifier, std::unordered_map<int, std::function<void(const CollisionParameters&)>>> collisionCallbackListStop;

	util::Booker<int, std::function<void(const CollisionParameters&)>> collisionStartId;
	util::Booker<int, std::function<void(const CollisionParameters&)>> collisionStopId;

	/**
	*  Called when two new actors are coliding
	*  @param contact point cache
	*  @param the rigidBody of actor one
	*  @param the rigidBody of actor two
	*/
	void sendCollisionPairAddEvent(const btPersistentManifold* manifold, const btRigidBody* bodyOne, const btRigidBody* bodyTwo);
	
	/**
	*  Called when two actors are no longer coliding
	*  @param the rigidBody of actor one
	*  @param the rigidBody of actor two
	*/
	void sendCollisionPairRemoveEvent(const btRigidBody* bodyOne, const btRigidBody* bodyTwo);

	void notifyFutureCollisions(ActorPhysics3d* actorPhys);

	// TODO: Create data structure for body
	std::unordered_map<actor::Identifier, TrdActorData> actorMap;
	
	std::shared_ptr<btDbvtBroadphase> broadphase;
	std::shared_ptr<btDefaultCollisionConfiguration> collisionConfiguration;
	std::shared_ptr<btCollisionDispatcher> dispatcher;
	std::shared_ptr<btSequentialImpulseConstraintSolver> solver;
	std::shared_ptr<btDiscreteDynamicsWorld> dynamicsWorld;

	world::timemanipulation::ITimeManager3d* timeManager;
	BulletDebugDraw3d* debugRenderer;

	glm::mat3x3 btMatrix3x3ToGlm(const btMatrix3x3& btmat);

	nox::app::graphics::GraphicsAssetManager3d* assetManager;

	bool isCollisionBroadcastEnabled;

};
} } }

#endif
