/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_PHYSICS_ACTORPHYSICS3D_H_
#define NOX_LOGIC_PHYSICS_ACTORPHYSICS3D_H_


#include <nox/logic/actor/Component.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>

namespace nox { namespace logic { namespace physics {


/**
 * Enables an Actor to be physically simulated.
 *
 * # Dependencies
 * - Transform
 *
 * # JSON Description
 * ## Name
 * %Physics
 *
 * ## Properties
 *
 */
class ActorPhysics3d : public actor::Component, public nox::logic::event::IListener
{
public:
	static const IdType NAME;

	ActorPhysics3d();

	const IdType& getName() const override;
	bool initialize(const Json::Value& componentJsonObject) override;
	void onCreate() override;
	void onDestroy() override;
	void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	void onUpdate(const Duration& deltaTime) override;
	void onActivate() override;
	void onDeactivate() override;

	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void createActorPhysicsShape();
	
	void clearForces();
	
	void applyCentralForce(const glm::vec3& force);
	void applyCentralImpulse(const glm::vec3& impulse);
	void applyImpulse(const glm::vec3& impulse, glm::vec3& relativePosition);
	void applyDamping(float timeStep);
	void applyGravity();
	void applyTorqueImpulse(const glm::vec3& torque);
	void applyForce(glm::vec3& force, glm::vec3& relPos);
	void applyTorque(glm::vec3 torque);
	void setLinearVelocity(glm::vec3 velocity);
	void setAngularVelocity(glm::vec3 velocity);
	void setTransform(glm::vec3 position, glm::quat rotation, glm::vec3 scale);
	void setAngularFactor(float factor);
	void setSleepingThresholds(glm::vec2 angAndLin);
	void setAnisotropicFriction(const glm::vec3& anisotropicFriction);
	void setCenterOfMassTransform(const btTransform& xform);
	void setCollisionFlags(int flags);
	void setDamping(float linearDamping, float angularDamping);
	void setFriction(float friction);
	void setGravity(const glm::vec3& acceleration);
	void setIgnoreCollisionCheck(bool ignore);
	void setInterpolationAngularVelocity(glm::vec3& angularVelocity);
	void setInterpolationLinearVelocity(glm::vec3& linearVelocity);
	void setInterpolationWorldTransform(btTransform& transform);
	void setLinearFactor(glm::vec3& linearFactor);
	void setRollingFriction(float friction);
	void setRestitution(float restitution);
	void setSimulatedFlag(bool simulated);
	void startSimulation();
	void stopSimulation();
	
	glm::vec3 getLinearVelocity();
	glm::vec3 getAngularVelocity();
	glm::vec3 getTotalForces();
	glm::vec3 getTotalTorque();
	glm::vec3 getAngularFactor();
	glm::vec3 getAnisotropicFriction();
	glm::vec3 getCenterOfMassPosition();
	glm::vec3 getGravity();
	glm::vec3 getInterpolationAngularVelocity();
	glm::vec3 getInterpolationLinearVelocity();
	glm::vec3 getLinearFactor();
	btTransform getCenterOfMassTransform();
	btTransform getInterpolationWorldTransform();
	glm::quat getOrientation();
	int getCollisionFlags();
	float getAngularSleepingThreshold();
	float getAngularDamping();
	float getFriction();
	float getLinearDamping();
	float getRestitution();
	float getRollingFriction();
	bool isInWorld();
	bool isKinematicObject();
	bool isStaticObject();
	bool isStaticOrKinematicObject();
	bool isSimulated();

	void setPosition(const glm::vec3 position);

	float getDistanceAboveGround();

	void serializePhysicsBody(nox::logic::world::timemanipulation::ActorPhysicsState* actorState);

	std::vector<nox::logic::world::timemanipulation::SavedCollision> collisions;
	int startSimulatingAt;
	int oldStartSimulatingAt;
	bool collisionUpdated;
	nox::logic::actor::Actor* conflictedActor;
	bool conflictResolved;

private:
	void serialize(Json::Value& componentObject) override;
	ShapeType3d getShapeType(std::string shape);
	BulletBodyDefinition physicalProperties;

	Simulation3d* physics;

	nox::logic::event::ListenerManager listener;

	bool simulationLock;
	
	bool simulated;

	
};

} } }

#endif
