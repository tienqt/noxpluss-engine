/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATION_DEFAULTTIMECONFLICTSOLVER_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATION_DEFAULTTIMECONFLICTSOLVER_H_

#include <nox/logic/actor/event/Event.h>
#include <nox/logic/world/timeManipulation/ITimeConflictSolver3d.h>
#include <unordered_map>

namespace nox { namespace logic { namespace world { namespace timemanipulation {


class DefaultTimeConflictSolver3d : public ITimeConflictSolver3d
{
public:
	DefaultTimeConflictSolver3d();
	~DefaultTimeConflictSolver3d();

	void setOnReplayStarted(std::function<void(Manager*)> onStartFunction) override;
	void setOnReplayStopped(std::function<void(Manager*)> onStopFunction) override;
	void setOnReplayFinished(std::function<void(Manager*)> onFinishedFunction) override;
	std::function<void(Manager*)> getOnReplayStarted() override;
	std::function<void(Manager*)> getOnReplayStopped() override;
	std::function<void(Manager*)> getOnReplayFinished() override;
	void setConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType, std::function<void(const ConflictParameters&)> conflictSolver) override;
	std::function<void(const ConflictParameters&)> getConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType) override;

private:
	std::function<void(Manager*)> onReplayStart;
	std::function<void(Manager*)> onReplayStop;
	std::function<void(Manager*)> onReplayFinished;
	std::unordered_map<nox::logic::actor::Identifier, std::function<void(const ConflictParameters&)>> actorCreatedSolvers;
	std::unordered_map<nox::logic::actor::Identifier, std::function<void(const ConflictParameters&)>> actorDeletedSolvers;
	std::unordered_map<nox::logic::actor::Identifier, std::function<void(const ConflictParameters&)>> actorInterruptedSolvers;
	std::unordered_map<nox::logic::actor::Identifier, std::function<void(const ConflictParameters&)>> positionOverlapSolvers;

};

} } } }

#endif
