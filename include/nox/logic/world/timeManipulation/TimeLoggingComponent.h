/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMELOGGINGCOMPONENT_H_
#define NOX_LOGIC_WORLD_TIMELOGGINGCOMPONENT_H_

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>
//#include <boost/archive/binary_oarchive.hpp>
//#include <boost/archive/binary_oarchive.hpp>

namespace nox { namespace logic { namespace world { namespace timemanipulation
{

typedef std::vector<unsigned char> STORAGE_TYPE;

class TimeLoggingComponent: public actor::Component, public event::IListener
{
public:
	TimeLoggingComponent();
	virtual ~TimeLoggingComponent();

	virtual bool initialize(const Json::Value& componentJson) override;
	virtual void onActivate() override;
	virtual void onDeactivate() override;
	virtual void onCreate() override;
	virtual void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	virtual void onEvent(const std::shared_ptr<event::Event>& event) override;

	virtual void onSave(TimeManipulationData3d* saveData) = 0;
	virtual void onRestore(TimeManipulationData3d* restoreData) = 0;

	void logComponentData();

protected:


private:

	event::ListenerManager listener;
	nox::logic::world::timemanipulation::IWorldLogger3d* worldLogger;

	

};

} } } } 

#endif
