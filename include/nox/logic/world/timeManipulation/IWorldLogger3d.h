/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_IWORLDLOGGER3D_H_
#define NOX_LOGIC_WORLD_IWORLDLOGGER3D_H_

#include <nox/common/types.h>
#include <vector>
#include <glm/mat4x4.hpp>
#include <glm/mat3x3.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>



namespace nox { namespace logic {
	class Logic;
namespace graphics
{
class AnimationChanged3d;
}

namespace physics {
class Simulation3d;
class ActorPhysics3d;
}

namespace actor {
	class Transform3d;
}

namespace world { 

class Manager;

namespace timemanipulation {

	class TimeLoggingComponent;
	class ITimeConflictSolver3d;

enum OptionsTwoTiming {
	LOGG_PHYSICS = 0x01,
	LOGG_ANIMATION = 0x02,
	LOGG_LIGHT = 0x04,
	LOGG_EVERYTHING = 0x08
	/*,
	LOGG_ = 0x10,
	OPT_F_HELLO_WORLD = 0x20,
	OPT_G_AA = 0x40,
	OPT_H = 0x80,*/
};

class IWorldLogger3d
{
public:
	enum PlayMode
	{
		REWIND = -1,
		FORWARD = 1
	};

	~IWorldLogger3d();

	virtual void logActorState(nox::logic::actor::Transform3d* actorTransform, nox::logic::physics::ActorPhysics3d* actorPhysics) = 0;
	virtual void serialize(nox::Duration& time) = 0;
	virtual void deSerialize(nox::Duration& time) = 0;
	virtual void setEndOfCurrentFrame() = 0; 

	/**
	*  IWorldLogger3d::playMode
	*/
	virtual void playFrame(int playMode) = 0;
	virtual nox::logic::world::Manager* getWorldManager() = 0;
	virtual bool endOfSavedFrames() = 0;
	virtual void logAnimationChanged(nox::logic::graphics::AnimationChanged3d* animeChanged) = 0;

	virtual void setRewindEnabled(bool enabled) = 0;
	virtual void setReplayEnabled(bool enabled) = 0;
	virtual void setTimeConflictSolver(nox::logic::world::timemanipulation::ITimeConflictSolver3d* conflictSolver) = 0;

	virtual int getNumberOfFrames() = 0;
	virtual int getCurrentFrame() = 0;
	virtual nox::logic::Logic* getLogic() = 0;

	virtual void logComponent(TimeLoggingComponent* component) = 0;

	virtual WorldState* getCurrentWorldState() = 0;

	
};

} } } }

#endif
