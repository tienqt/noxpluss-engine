/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATION_ITIMECONFLICTSOLVER3D_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATION_ITIMECONFLICTSOLVER3D_H_

#include <functional>

namespace nox { namespace logic 
{

namespace actor
{
class Identifier;
class Actor;
}

namespace world 
{ 

class Manager;

namespace timemanipulation {

enum ConflictType
{
	ACTOR_CREATED,
	ACTOR_DELETED,
	ACTOR_INTERRUPTED,
	POSITION_OVERLAP
};


struct ConflictParameters
{
	ConflictParameters()
	{
	}

	unsigned int conflictType;
	nox::logic::actor::Actor* affectedActor;
	nox::logic::actor::Actor* conflictingActor;
};


class ITimeConflictSolver3d 
{
public:
	~ITimeConflictSolver3d();
	virtual void setOnReplayStarted(std::function<void(Manager*)> onStartFunction) = 0;
	virtual void setOnReplayStopped(std::function<void(Manager*)> onStopFunction) = 0;
	virtual void setOnReplayFinished(std::function<void(Manager*)> onFinishedFunction) = 0;
	virtual std::function<void(Manager*)> getOnReplayStarted() = 0;
	virtual std::function<void(Manager*)> getOnReplayStopped() = 0;
	virtual std::function<void(Manager*)> getOnReplayFinished() = 0;
	virtual void setConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType, std::function<void(const ConflictParameters&)> conflictSolver) = 0;
	virtual std::function<void(const ConflictParameters&)> getConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType) = 0;
};

} } } }

#endif
