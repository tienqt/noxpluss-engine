/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATIONCOMPONENT_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATIONCOMPONENT_H_

#include <nox/logic/actor/Component.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>

namespace nox { namespace logic { namespace world { namespace timemanipulation
{

class TimeManipulationComponent: public actor::Component
{
	

public:
	static const IdType NAME;


	TimeManipulationComponent();
	virtual ~TimeManipulationComponent();

	virtual bool initialize(const Json::Value& componentJson) override;
	virtual void onActivate() override;
	virtual void onDeactivate() override;
	virtual void onCreate() override;
	virtual void onComponentEvent(const std::shared_ptr<event::Event>& event) override;
	virtual void serialize(Json::Value& componentObject) override;
	virtual const IdType& getName() const override;


	bool getLoggingEnabled();
	bool getIgnoreOthers();
	bool getAffectOthers();

	void setLoggingEnabled(bool flag);
	void setIgnoreOthers(bool flag);
	void setAffectOthers(bool flag);

protected:


private:
	bool enableLogging;
	bool ignoreOthers;
	bool affectOthers;
	

};

} } } } 

#endif
