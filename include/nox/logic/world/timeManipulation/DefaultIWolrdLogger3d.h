/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_DEFAULTWORLDLOGGER3D_H_
#define NOX_LOGIC_WORLD_DEFAULTWORLDLOGGER3D_H_

#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/timeManipulation/TimeManipulationComponent.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>
#include <nox/app/storage/IDataStorage.h>
#include <ostream>
#include <vector>
#include <list>


namespace nox {

namespace logic {
	
class Logic;

namespace world { namespace timemanipulation {

	

class DefaultWorldLogger3d : public IWorldLogger3d, public nox::logic::event::IListener
{
public:

	DefaultWorldLogger3d(nox::logic::Logic* logic, ITimeConflictSolver3d* conflictSolver);
	~DefaultWorldLogger3d();

	void logActorState(nox::logic::actor::Transform3d* actorTransform, nox::logic::physics::ActorPhysics3d * actorPhysics) override;
	void serialize(nox::Duration& time) override;
	void deSerialize(nox::Duration& time) override;
	void setEndOfCurrentFrame() override;
	void playFrame(int playMode) override;
	bool endOfSavedFrames() override;
	nox::logic::world::Manager* getWorldManager() override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	void setRewindEnabled(bool enabled) override;
	void setReplayEnabled(bool enabled) override;
	void logComponent(TimeLoggingComponent* component) override;

	nox::logic::world::timemanipulation::ActorAnimationState* getAnimationState(/*int frameOffset*/);

	void logAnimationChanged(nox::logic::graphics::AnimationChanged3d* animeChanged) override;
	void setTimeConflictSolver(nox::logic::world::timemanipulation::ITimeConflictSolver3d* conflictSolver) override;

	int getNumberOfFrames() override;
	int getCurrentFrame() override;
	nox::logic::Logic* getLogic() override;
	WorldState* getCurrentWorldState() override;

private:

	void applyPhysicsChanges(int playMode);
	void applyAnimationChanges(int playMode);
	void applyActorAddedOrRemovedChanges(int playMode);

	nox::logic::world::Manager* world;
	nox::logic::event::ListenerManager listener;

	bool rewindEnabled;
	bool replayEnabled;

	std::list<WorldState*> savedFrames;
	WorldState * bufferWorldState;

	std::list<WorldState*>::iterator frameIterator;

	nox::logic::Logic* logic;

	unsigned int frameCounter;

	nox::logic::world::timemanipulation::ITimeConflictSolver3d* conflictSolver;
};

} } } }

#endif
