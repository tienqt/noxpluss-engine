/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATION_TIMEMANIPULATIONMANAGER3D_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATION_TIMEMANIPULATIONMANAGER3D_H_

#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
//#include <nox/logic/Logic.h>
#include <memory>

namespace nox 
{ 
	
namespace app
{
class IContext;
} 

namespace logic { 

class Logic;

namespace world { namespace timemanipulation {

/**
 * An actor has been created and added to the game logic.
 */
class TimeManipulationManager3d : public  ITimeManager3d
{
public:
	TimeManipulationManager3d(std::unique_ptr<IWorldLogger3d> worldLogger, std::unique_ptr<ITimeConflictSolver3d> conflictSolver);
	~TimeManipulationManager3d();

	void onUpdate() override;
	void rewind() override;
	void setPause(bool pause) override;
	void play() override;

	bool isGamePaused() override;

	IWorldLogger3d* getWorldLogger() override;
	ITimeConflictSolver3d * getConflictSolver() override;

	void setWorldLogger(std::unique_ptr<IWorldLogger3d> worldLogger) override;
	void setConflictSolver(std::unique_ptr<ITimeConflictSolver3d> conflictSolver) override;

	void enable(Options optns) override;
	void disable(Options optns) override;

	bool isRewindEnabled() override;
	bool isReplayEnabled() override;




private:
	void setRewindEnabled(bool enabled);
	void setReplayEnabled(bool enabled);

	std::shared_ptr<nox::app::IContext> applicationContext;
	std::unique_ptr<IWorldLogger3d> worldLogger;
	std::unique_ptr<ITimeConflictSolver3d> conflictSolver;
	
	bool paused;
	bool rewindEnabled;
	bool replayEnabled;
	bool timeManipulationLock;

	bool replayStartedLock;
	bool replayStoppedLock;
	bool replayFinishedLock;

	nox::logic::Logic* logic;

	Options flags;
};

inline Options operator|(Options a, Options b)
{
	return static_cast<Options>(static_cast<int>(a) | static_cast<int>(b));
}

} } } }

#endif
