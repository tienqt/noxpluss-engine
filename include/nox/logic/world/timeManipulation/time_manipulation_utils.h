/*
* NOX Engine
*
* Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATION_TIMEMANIPULATIONUTILS_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATION_TIMEMANIPULATIONUTILS_H_

#include <glm/glm.hpp>
#include <nox/logic/actor/Identifier.h>
#include <glm/gtx/quaternion.hpp>
//#include <nox/logic/actor/component/Transform3d.h>
//#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <sstream>
#include <cstring>
#include <vector>
#include <nox/common/types.h>
#include <memory>
#include <unordered_map>
#include <BulletDynamics/Dynamics/btRigidBody.h>
#include <nox/logic/event/Event.h>

namespace nox 
{ 

namespace app
{
namespace graphics
{
class SceneGraphNode;
} 
}

namespace logic 
{ 

namespace actor
{
class Actor;
class Identifier;
class Transform3d;
}

namespace graphics
{
class ActorGraphics3d;
}

namespace physics
{
class ActorPhysics3d;
}

namespace world { namespace timemanipulation {

	class TimeLoggingComponent;

struct ActorAnimationState
{
	ActorAnimationState() : 
		actorGraphics(nullptr),
		currentAnimationId(0),
		currentAnimationSpeed(0),
		currentAnimationTime(0),
		previousAnimationId(0),
		previousAnimationSpeed(0),
		previousAnimationTime(0)

	{}

	nox::logic::graphics::ActorGraphics3d* actorGraphics;
	int currentAnimationId;
	float currentAnimationSpeed;
	float currentAnimationTime;

	int previousAnimationId;
	float previousAnimationSpeed;
	float previousAnimationTime;
};

struct PhysicalProperties
{
	PhysicalProperties() :
		invInertiaTensorWorld(glm::mat3(1)),
		linearVelocity(glm::vec3()),
		angularVelocity(glm::vec3())
	{
	}

	glm::mat3x3 invInertiaTensorWorld;
	glm::vec3 linearVelocity;
	glm::vec3 angularVelocity;
};

struct ActorPhysicsState
{
	ActorPhysicsState() :
		actorTransform(nullptr),
		actorPhysics(nullptr),
		endOfCurrentFrame(false)
	{
	}

	nox::logic::physics::ActorPhysics3d* actorPhysics;
	nox::logic::actor::Transform3d* actorTransform;

	PhysicalProperties physicalProperties;
	glm::vec3 position;
	glm::quat rotation;
	glm::vec3 scale;
	int animationIndex;
	

	bool endOfCurrentFrame;
	
};

struct ActorAddedOrRemoved		// todo: rename
{

	const nox::logic::actor::Identifier getActorId() const
	{
		return this->actorId;
	}

	bool added;
	nox::logic::physics::ActorPhysics3d* actorPhysics;
	std::shared_ptr<nox::app::graphics::SceneGraphNode> sceneNode;
	nox::logic::actor::Identifier actorId;
};

class TimeManipulationData3d
{
public:
	TimeManipulationData3d(TimeLoggingComponent* component) :
		component(component),
		dataIndex(0)
	{}
	~TimeManipulationData3d(){}

	template<class T>
	void save(T* savedObj);

	template<class T>
	void load(T* loadObj);

	void restore();

private:
	int dataIndex;
	std::vector<char> buffer;
	TimeLoggingComponent* component;
};


template<class T>
inline void TimeManipulationData3d::load(T* loadObj)
{
	int size = sizeof(T);
	memcpy(loadObj, &buffer[dataIndex], size);
	this->dataIndex += size;
}

template<class T>
inline void TimeManipulationData3d::save(T* savedObj)
{
	buffer.insert(buffer.end(), (char*)savedObj, (char*)savedObj + sizeof(T));
}

struct WorldState
{
	std::vector<ActorPhysicsState> actorPhysicsChanges;
	std::vector<ActorAnimationState> actorAnimationChanges;
	std::vector<ActorAddedOrRemoved> actorsAddedOrRemoved;
	std::vector<TimeManipulationData3d> componentChanged;

	std::unordered_map<nox::logic::actor::Identifier, std::vector<nox::logic::physics::ActorPhysics3d*>> actorCollisons;

	int frameNumber;
};

struct SavedCollision
{
	nox::logic::physics::ActorPhysics3d* affected;
	int frameNumber;

};

class PlayModeChange : public nox::logic::event::Event
{
public:
	static const nox::logic::event::Event::IdType ID;

	PlayModeChange(bool isPlayEnabled);
	~PlayModeChange();

	bool isPlay();

private:
	bool playEnabled;
};

}}}}

#endif