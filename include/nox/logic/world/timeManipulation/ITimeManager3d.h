/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_LOGIC_WORLD_TIMEMANIPULATION_ITIMEMANAGER_H_
#define NOX_LOGIC_WORLD_TIMEMANIPULATION_ITIMEMANAGER_H_

#include <memory>

namespace nox { namespace logic { namespace world { namespace timemanipulation {


enum Options {
	REWIND = 0x01,
	FAST_FORWARD = 0x02,
	AFFECT_PAST = 0x04,
	AFFECT_FUTURE = 0x08,
	EVERYTHING = 0x10,
	/*,
	OPT_F_HELLO_WORLD = 0x20,
	OPT_G_AA = 0x40,
	OPT_H = 0x80,*/
};

class IWorldLogger3d;
class ITimeConflictSolver3d;

class ITimeManager3d 
{
public:
	~ITimeManager3d();

	void setSpeed();

	virtual void onUpdate() = 0;
	virtual void rewind() = 0;
	virtual void setPause(bool pause) = 0;
	virtual void play() = 0;

	virtual bool isGamePaused() = 0;

	virtual void setConflictSolver(std::unique_ptr<ITimeConflictSolver3d> conflictSolver) = 0;
	virtual void setWorldLogger(std::unique_ptr<IWorldLogger3d> worldLogger) = 0;
	virtual IWorldLogger3d* getWorldLogger() = 0;
	virtual ITimeConflictSolver3d * getConflictSolver() = 0;

	virtual void enable(Options optns) = 0;
	virtual void disable(Options optns) = 0;
	
	virtual bool isRewindEnabled() = 0;
	virtual bool isReplayEnabled() = 0;

};

} } } }

#endif
