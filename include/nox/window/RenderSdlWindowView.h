/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_WINDOW_RENDERSDLWINDOWVIEW_H_
#define NOX_WINDOW_RENDERSDLWINDOWVIEW_H_

#include "SdlWindowView.h"
#include <nox/app/log/Logger.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/app/graphics/IRenderer3d.h>
#include <thread>

namespace nox
{

namespace app { namespace graphics
{

class IRenderer;

} }

namespace window
{

class RenderSdlWindowView: public SdlWindowView, public logic::event::IListener
{
public:
	RenderSdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, bool create2dRenderer, bool create3dRenderer);
	virtual ~RenderSdlWindowView();

	void render();

	virtual void onSdlEvent(const SDL_Event& event) override;

protected:
	logic::IContext* getLogicContext();
	app::IContext* getApplicationContext();

	virtual bool initialize(logic::IContext* context) override;
	virtual void onWindowSizeChanged(const glm::uvec2& size) override;
	virtual void onEvent(const std::shared_ptr<logic::event::Event>& event) override;
	virtual void onDestroy() override;

private:
	virtual void onRendererCreated(app::graphics::IRenderer* renderer) = 0;
	virtual void onRendererCreated3d(app::graphics::IRenderer3d* renderer3d) = 0;

	bool onWindowCreated(SDL_Window* window) override final;

	app::IContext* applicationContext;
	logic::IContext* logicContext;
	app::log::Logger log;
	SDL_Window* window;

	std::unique_ptr<app::graphics::IRenderer> renderer2d;
	std::unique_ptr<app::graphics::IRenderer3d> renderer3d;

	logic::event::ListenerManager listener;

	bool use2dRendering;
	bool use3dRendering;
	
	bool renderingInProgress;
	bool started;

	void startRendering();

	std::thread renderThread;
};

}
}

#endif
