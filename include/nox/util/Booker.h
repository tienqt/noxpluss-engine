/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */ 

#ifndef NOX_UTIL_BOOKER_H_
#define NOX_UTIL_BOOKER_H_

#include <unordered_map>
#include <queue>

namespace nox { namespace util
{

/**
 * Book unique keys for values.
 * @tparam KeyType The type used for the keys. Usually integral.
 * @tparam ValueType The type used for the values stored.
 */
template<class KeyType, class ValueType>
class Booker
{
public:
	Booker();

	/**
	 * Book a key for a value.
	 * @param value Value to book key for.
	 * @return Unique key for value.
	 */
	KeyType book(ValueType value);

	/**
	 * Book a key for a list of values.
	 * @param value Values to book key for.
	 * @return Unique key for value.
	 */
	KeyType book(std::vector<ValueType> valueList);

	/**
	 * Find the values for a key.
	 * @param key Key to find values for.
	 * @return The values for the key booked.
	 */
	std::vector<ValueType> find(KeyType key) const;

	/**
	 * Unbook a key.
	 * The key will no longer be valid, and can be used by new values.
	 * @param key Key to unbook.
	 */
	void unbook(KeyType key);

	/**
	 * Clear all bookings.
	 * None of the keys booked will be valid and unique.
	 */
	void clear();

private:
	KeyType findFreeKey();

	std::unordered_multimap<KeyType, ValueType> booked;
	std::queue<KeyType> freeKeys;
	KeyType numKeys;
};

template<class KeyType, class ValueType>
Booker<KeyType, ValueType>::Booker():
	numKeys()
{
}

template<class KeyType, class ValueType>
KeyType Booker<KeyType, ValueType>::book(ValueType value)
{
	const KeyType key = this->findFreeKey();

	this->booked.insert({key, value});

	return key;
}

template<class KeyType, class ValueType>
KeyType Booker<KeyType, ValueType>::book(std::vector<ValueType> valueList)
{
	const KeyType key = this->findFreeKey();

	for (const ValueType& value : valueList)
	{
		this->booked.insert({key, value});
	}

	return key;
}

template<class KeyType, class ValueType>
void Booker<KeyType, ValueType>::unbook(KeyType key)
{
	this->booked.erase(key);
}

template<class KeyType, class ValueType>
std::vector<ValueType> Booker<KeyType, ValueType>::find(KeyType key) const
{
	std::vector<ValueType> values;

	auto valueRange = this->booked.equal_range(key);

	for (auto valueIt = valueRange.first; valueIt != valueRange.second; ++valueIt)
	{
		values.push_back(valueIt->second);
	}

	return values;
}

template<class KeyType, class ValueType>
inline void Booker<KeyType, ValueType>::clear()
{
	this->booked.clear();
	this->numKeys = KeyType();

	decltype(this->freeKeys) emtyQueue;
	this->freeKeys.swap(emtyQueue);
}

template<class KeyType, class ValueType>
KeyType Booker<KeyType, ValueType>::findFreeKey()
{
	KeyType key;

	if (this->freeKeys.empty() == false)
	{
		key = this->freeKeys.front();
		this->freeKeys.pop();
	}
	else
	{
		key = this->numKeys;
		this->numKeys++;
	}

	return key;
}

} }

#endif
