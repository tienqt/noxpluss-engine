/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOX_MATH_VECTORMATH_H_
#define NOX_MATH_VECTORMATH_H_

#include <glm/vec2.hpp>
#include <glm/geometric.hpp>
#include <glm/trigonometric.hpp>
#include "equality.h"

namespace nox { namespace math
{

/**
 * Rotate a vector around a point with an angle.
 * @param vector Vector to rotate.
 * @param point Point to rotate around.
 * @param angle How much to rotate the vector.
 * @return Rotated vector.
 */
glm::vec2 rotateVectorAroundPoint(glm::vec2 vector, glm::vec2 point, float angle);

/**
 * Check if two vectors are approximately equal.
 */
bool approximatelyEqual(glm::vec2 a, glm::vec2 b, float epsilon);

/**
 * Convert a vec2 type to another vec2 type.
 * @tparam InputVec2 Type of input vector. Must have public InputVec2::x and InputVec2::y.
 * @tparam OutputVec2 Type of returned vector. Must have constructor taking InputVec2::x and InputVec2::y.
 * @param inputVector Vector to convert.
 * @return Converted vector.
 */
template<class OutputVec2, class InputVec2>
OutputVec2 convertVec2(const InputVec2& inputVector);

/**
 * Get the angle of a vector.
 * @param vector Vector to calculate angle from.
 * @return The angle of the vector in range (-pi, pi]
 */
template<class Vec2Type>
float vectorAngle(const Vec2Type& vector);

/**
 * Get the vector representing an angle.
 * @param angle Angle to calculate vector from.
 * @return A vector of length 1 in the direction of the angle.
 */
template<class Vec2Type, class Type>
Vec2Type angleVector(const Type angle);

/**
 * Normalize a vector safely so that if the length of the vector is 0, the resulting vector will be {0,0} rather than NaN.
 * @param vec2 Vector to normalize.
 * @return Vector of length 1, or 0 if vec2 has a length of 0.
 */
template<class T, glm::precision P>
glm::tvec2<T, P> normalizeSafe(const glm::tvec2<T, P>& vec2);

/**
 * Find the cross product of two 2D vectors.
 */
float cross(const glm::vec2& vectorA, const glm::vec2& vectorB);



template<class OutputVec2, class InputVec2>
inline OutputVec2 convertVec2(const InputVec2& inputVector)
{
	return OutputVec2(inputVector.x, inputVector.y);
}

template<typename Type>
inline Type gravitationalConstant()
{
	return Type(6.67e-11);
}

template<class Vec2Type>
inline float vectorAngle(const Vec2Type& vector)
{
	return glm::atan(vector.y, vector.x);
}

template<class Vec2Type, class Type>
inline Vec2Type angleVector(const Type angle)
{
	return Vec2Type(glm::cos(angle), glm::sin(angle));
}

template<class T, glm::precision P>
inline glm::tvec2<T, P> normalizeSafe(const glm::tvec2<T, P>& vec2)
{
	if (vec2.x != T(0) || vec2.y != T(0))
	{
		return glm::normalize(vec2);
	}
	else
	{
		return glm::tvec2<T, P>();
	}
}

inline bool approximatelyEqual(glm::vec2 a, glm::vec2 b, float epsilon)
{
	return (approximatelyEqual(a.x, b.x, epsilon) && approximatelyEqual(a.y, b.y, epsilon));
}

} }

#endif
