/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/window/RenderSdlWindowView.h>

#include <nox/app/IContext.h>
#include <nox/app/graphics/opengl/OpenGlRenderer3d.h>
#include <nox/app/graphics/2d/OpenGlRenderer.h>
#include <nox/logic/IContext.h>
#include <nox/logic/graphics/event/DebugGeometryChange.h>

#include <assert.h>

namespace nox
{
namespace window
{

	RenderSdlWindowView::RenderSdlWindowView(app::IContext* applicationContext, const std::string& windowTitle, bool create2dRenderer, bool create3dRenderer) :
		SdlWindowView(applicationContext, windowTitle, true),
	applicationContext(applicationContext),
	logicContext(nullptr),
	window(nullptr),
	listener("RenderSdlWindowView"),
	use2dRendering(create2dRenderer),
	use3dRendering(create3dRenderer)
{
	assert(applicationContext != nullptr);
	assert(!(use2dRendering == false && use3dRendering == false));	// minimum one rendering type

	this->log = applicationContext->createLogger();
	this->log.setName("SdlRendererWindowView");

	this->listener.addEventTypeToListenFor(logic::graphics::DebugGeometryChange::ID);

	renderingInProgress = false;
	started = false;
}

RenderSdlWindowView::~RenderSdlWindowView() = default;

bool RenderSdlWindowView::initialize(logic::IContext* context)
{
	this->logicContext = context;

	if (this->SdlWindowView::initialize(context) == false)
	{
		return false;
	}

	this->listener.setup(this, context->getEventBroadcaster(), logic::event::ListenerManager::StartListening_t());

	return true;
}

void RenderSdlWindowView::startRendering()
{

	while (true)
	{
		renderingInProgress = true;
		this->renderer3d->onRender();
		renderingInProgress = false;
		std::printf("Renders 1111: %i\n", renderingInProgress);
	}

}

void RenderSdlWindowView::render()
{
	if (this->use2dRendering)
	{
		assert(this->renderer2d != nullptr);
		this->renderer2d->onRender();
	}
	if (this->use3dRendering)
	{
		assert(this->renderer3d != nullptr);
		this->renderer3d->onRender();
	}
	
	SDL_GL_SwapWindow(this->window);
}

bool RenderSdlWindowView::onWindowCreated(SDL_Window* window)
{
	this->window = window;

	if (this->use2dRendering)
	{
		this->renderer2d = std::unique_ptr<app::graphics::OpenGlRenderer>(new app::graphics::OpenGlRenderer());
		std::string shaderDirectory = "nox/shader/";

		if (this->renderer2d->init(this->applicationContext, shaderDirectory, this->getWindowSize()) == false)
		{
			this->log.fatal().raw("Failed to create OpenGL 2D renderer.");

			return false;
		}
		else
		{
			this->log.verbose().raw("Created OpenGL 2D renderer.");

			this->onRendererCreated(this->renderer2d.get());			
		}
	}

	if (this->use3dRendering)
	{
		this->renderer3d = std::unique_ptr<app::graphics::OpenGlRenderer3d>(new app::graphics::OpenGlRenderer3d());
		std::string shaderDirectory = "nox/shader/";

		if (this->renderer3d->init(this->applicationContext, shaderDirectory, this->getWindowSize()) == false)
		{
			this->log.fatal().raw("Failed to create OpenGL 3D renderer.");

			return false;
		}
		else
		{
			this->log.verbose().raw("Created OpenGL 3D renderer.");

			this->onRendererCreated3d(this->renderer3d.get());
		}
	}

	return true;

}

void RenderSdlWindowView::onSdlEvent(const SDL_Event& event)
{
	this->SdlWindowView::onSdlEvent(event);
}

void RenderSdlWindowView::onDestroy()
{
	//this->started = false;

	//this->renderThread.join();
	
	this->listener.stopListening();
}

void RenderSdlWindowView::onWindowSizeChanged(const glm::uvec2& size)
{
	if (this->use2dRendering)
	{
		assert(this->renderer2d != nullptr);

		this->renderer2d->resizeWindow(size);
	}

	if (this->use3dRendering)
	{
		assert(this->renderer3d != nullptr);

		this->renderer3d->resizeWindow(size);
	}
}

void RenderSdlWindowView::onEvent(const std::shared_ptr<logic::event::Event>& event)
{
	if (this->use2dRendering)
	{
		if (event->isType(logic::graphics::DebugGeometryChange::ID))
		{
			auto geometryEvent = static_cast<logic::graphics::DebugGeometryChange*>(event.get());

			if (geometryEvent->wasAdded())
			{
				this->renderer2d->addDebugGeometrySet(geometryEvent->getGeometry());
			}
			else if (geometryEvent->wasRemoved())
			{
				this->renderer2d->removeDebugGeometrySet(geometryEvent->getGeometry());
			}
		}
	}
	if (this->use2dRendering)
	{
		// So far nothing
	}
}

logic::IContext* RenderSdlWindowView::getLogicContext()
{
	return this->logicContext;
}

app::IContext* RenderSdlWindowView::getApplicationContext()
{
	return this->applicationContext;
}

}
}
