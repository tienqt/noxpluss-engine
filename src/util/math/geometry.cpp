/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/util/math/geometry.h>

namespace nox { namespace math
{

// Thank you: http://devmag.org.za/2009/04/17/basic-collision-detection-in-2d-part-2/
CircleLineIntersection findCircleLineIntersections(const Circle& circle, const Line& line)
{
	const glm::vec2 lineDelta = line.end - line.start;

	Line localLine;
	localLine.start = line.start - circle.position;
	localLine.end = line.end - circle.position;

	const float a = lineDelta.x * lineDelta.x + lineDelta.y * lineDelta.y;
	const float b = 2.0f * (localLine.start.x * lineDelta.x + localLine.start.y * lineDelta.y);
	const float c = localLine.start.x * localLine.start.x + localLine.start.y * localLine.start.y - circle.radius * circle.radius;

	const float discriminant = b * b - 4.0f * a * c;

	if (discriminant > 0)
	{
		const float discriminantSquareRoot = glm::sqrt(discriminant);
		const float doubleA = 2.0f * a;

		const float positiveCalculation = (-b + discriminantSquareRoot) / doubleA;
		const float negativeCalculation = (-b - discriminantSquareRoot) / doubleA;

		CircleLineIntersection intersection;
		intersection.intersected = true;
		intersection.firstIntersection = line.start + positiveCalculation * lineDelta;
		intersection.secondIntersection = line.start + negativeCalculation * lineDelta;

		return intersection;
	}
	else
	{
		return CircleLineIntersection();
	}
}

float findCircleCircumference(const float radius)
{
	return 2.0f * glm::pi<float>() * radius;
}

bool simpleCircleAabbRangeCheck(const Circle& circle, const Box<glm::vec2>& aabb)
{
	const glm::vec2 aabbCenter = aabb.getCenter();
	const float aabbPerimiter = aabb.getPerimeter();
	const float circleAabbDistance = glm::distance(circle.position, aabbCenter);

	return (circleAabbDistance < (aabbPerimiter + circle.radius));
}

} }
