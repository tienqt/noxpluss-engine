/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/PointLight3d.h>
#include <nox/app/graphics/opengl/GBuffer.h>

#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>


namespace nox { namespace app { namespace graphics
{

float ffmax(float a, float b)
{
	if (a > b)
		return a;
	else
		return b;
}

PointLight3d::PointLight3d() : 
	BaseLight3d()
{
}

PointLight3d::PointLight3d(std::shared_ptr<Mesh3d> sphereLightVolume, glm::vec3 color, float ambientIntensity, float diffuseIntensity,
	glm::vec3 position, float constantAttenuation, float linearAttenuation, float exponentialAttenuation) :
		BaseLight3d(sphereLightVolume.get(), color, ambientIntensity, diffuseIntensity),
		position(position),
		constantAttenuation(constantAttenuation),
		linearAttenuation(linearAttenuation),
		exponentialAttenuation(exponentialAttenuation)
{
	this->sphereRadiuse = calculateLightVolumeRadiuse();
}

PointLight3d::~PointLight3d()
{

}

float PointLight3d::calculateLightVolumeRadiuse()
{
	float MaxChannel = ffmax(ffmax(getColor().x, getColor().y), getColor().z);

	float ret = (this->linearAttenuation + sqrtf(this->linearAttenuation * this->linearAttenuation -
		4 * this->exponentialAttenuation* (this->exponentialAttenuation - 255 * MaxChannel * getDiffuseIntensity())))
		/
		2 * this->exponentialAttenuation;
	return ret;
}

void PointLight3d::init(GLuint shaderProgram, GLuint stencilProgram)
{
	this->pointLight_base_color = glGetUniformLocation(shaderProgram, "gPointLight.Base.Color");
	this->pointLight_base_ambientIntensity = glGetUniformLocation(shaderProgram, "gPointLight.Base.AmbientIntensity");
	this->pointLight_base_diffuseIntensity = glGetUniformLocation(shaderProgram, "gPointLight.Base.DiffuseIntensity");
	this->pointLight_position = glGetUniformLocation(shaderProgram, "gPointLight.Position");
	this->pointLight_base_atten_constant = glGetUniformLocation(shaderProgram, "gPointLight.Atten.Constant");
	this->pointLight_base_atten_linear = glGetUniformLocation(shaderProgram, "gPointLight.Atten.Linear");
	this->pointLight_base_atten_exp = glGetUniformLocation(shaderProgram, "gPointLight.Atten.Exp");
	
	this->eyeWorldPos = glGetUniformLocation(shaderProgram, "gEyeWorldPos");
	this->specularIntensity = glGetUniformLocation(shaderProgram, "gMatSpecularIntensity");
	this->specularPower = glGetUniformLocation(shaderProgram, "gSpecularPower");
	this->lightType = glGetUniformLocation(shaderProgram, "gLightType");
	this->screenSize = glGetUniformLocation(shaderProgram, "gScreenSize");

	this->posSampler = glGetUniformLocation(shaderProgram, "gPositionMap");
	this->colorSampler = glGetUniformLocation(shaderProgram, "gColorMap");
	this->normalSampler = glGetUniformLocation(shaderProgram, "gNormalMap");
	
	this->mvpHandle = glGetUniformLocation(shaderProgram, "mvpMatrix");

	this->stencilMvpHandle = glGetUniformLocation(stencilProgram, "mvpMatrix");
}

void PointLight3d::render(RenderData * renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection)
{
	glUniform1i(this->posSampler, GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
	glUniform1i(this->colorSampler, GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
	glUniform1i(this->normalSampler, GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);

	glUniform3fv(this->pointLight_base_color, 1, glm::value_ptr(getColor()));
	glUniform1f(this->pointLight_base_ambientIntensity, getAmbientIntensity());
	glUniform1f(this->pointLight_base_diffuseIntensity, getDiffuseIntensity());
	glUniform3fv(this->pointLight_position, 1, glm::value_ptr(this->position));
	glUniform1f(this->pointLight_base_atten_constant, this->constantAttenuation);
	glUniform1f(this->pointLight_base_atten_linear, this->linearAttenuation);
	glUniform1f(this->pointLight_base_atten_exp, this->exponentialAttenuation);

	glUniform3fv(this->eyeWorldPos, 1, glm::value_ptr(eyePosition));
	glUniform1f(this->specularIntensity, 1.0f);
	glUniform1f(this->specularPower, 1.0f);
	glUniform1i(this->lightType, 1);
	glUniform2fv(this->screenSize, 1, glm::value_ptr(glm::vec2(windowSize.x, windowSize.y)));

	glm::mat4 model(1);
	model = glm::translate(model, this->position);

	this->sphereRadiuse = calculateLightVolumeRadiuse();
	model = glm::scale(model, glm::vec3(this->sphereRadiuse, this->sphereRadiuse, this->sphereRadiuse));
	glUniformMatrix4fv(mvpHandle, 1, false, glm::value_ptr(viewProjection * model));

	this->getLightVolume()->render(*renderData);

}

void PointLight3d::render(RenderData * renderData, const glm::mat4x4& mvp)
{
	//TODO:fix mvp to vp

	glm::mat4 model(1);
	model = glm::translate(model, this->position);
	this->sphereRadiuse = calculateLightVolumeRadiuse();
	model = glm::scale(model, glm::vec3(this->sphereRadiuse, this->sphereRadiuse, this->sphereRadiuse));
	
	glUniformMatrix4fv(stencilMvpHandle, 1, false, glm::value_ptr(mvp * model));
	glUniform1i(this->lightType, 0);
	this->getLightVolume()->render(*renderData);
}

glm::vec3 PointLight3d::getPosition()
{
	return this->position;
}

float PointLight3d::getConstantAttention()
{
	return this->constantAttenuation;
}

float PointLight3d::getLinearAttention()
{
	return linearAttenuation;
}

float PointLight3d::getExponentialAttention()
{
	return this->exponentialAttenuation;
}

void PointLight3d::setColor(glm::vec3 color)
{
	BaseLight3d::setColor(color);
	calculateLightVolumeRadiuse();
}

void PointLight3d::setPosition(glm::vec3 position)
{
	this->position = position;
}

void PointLight3d::setConstantAttention(float constantAttenuation)
{
	this->constantAttenuation = constantAttenuation;
	calculateLightVolumeRadiuse();
}

void PointLight3d::setLinearAttention(float linearAttenuation)
{
	this->linearAttenuation = linearAttenuation;
	calculateLightVolumeRadiuse();
}

void PointLight3d::setExponentialAttention(float exponentialAttenuation)
{
	this->exponentialAttenuation = exponentialAttenuation;
	calculateLightVolumeRadiuse();
}




} } }
