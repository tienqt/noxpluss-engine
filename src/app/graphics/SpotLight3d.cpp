/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <nox/app/graphics/SpotLight3d.h>
#include <nox/app/graphics/opengl/GBuffer.h>
#include <nox/app/graphics/Mesh3d.h>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/transform.hpp>

namespace nox { namespace app
{
namespace graphics
{

SpotLight3d::SpotLight3d()
{
}

	SpotLight3d::SpotLight3d(std::shared_ptr<Mesh3d> sphereLightVolume, glm::vec3 color, float ambientIntensity, float diffuseIntensity,
		glm::vec3 position, float constantAttention, float linearAttention, float exponentialAttenuation, float cutoffAngle, glm::vec3 direction)
		: PointLight3d(sphereLightVolume, color, ambientIntensity, diffuseIntensity, position, constantAttention, linearAttention, exponentialAttenuation),
		coneAngle(cutoffAngle),
		coneDirection(direction)
	{

	}

	void SpotLight3d::init(GLuint shaderProgram, GLuint stenchilProgram)
	{
		PointLight3d::init(shaderProgram, stenchilProgram);

		this->spotLight_coneAngle = glGetUniformLocation(shaderProgram, "gConeAngle");
		this->spotLight_coneDirection = glGetUniformLocation(shaderProgram, "gConeDirection");

	}

	void SpotLight3d::render(RenderData * renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection)
	{
		glUniform1i(this->posSampler, GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
		glUniform1i(this->colorSampler, GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
		glUniform1i(this->normalSampler, GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);

		glUniform3fv(this->pointLight_base_color, 1, glm::value_ptr(getColor()));
		glUniform1f(this->pointLight_base_ambientIntensity, getAmbientIntensity());
		glUniform1f(this->pointLight_base_diffuseIntensity, getDiffuseIntensity());
		glUniform3fv(this->pointLight_position, 1, glm::value_ptr(this->position));
		glUniform1f(this->pointLight_base_atten_constant, this->constantAttenuation);
		glUniform1f(this->pointLight_base_atten_linear, this->linearAttenuation);
		glUniform1f(this->pointLight_base_atten_exp, this->exponentialAttenuation);

		glUniform3fv(this->eyeWorldPos, 1, glm::value_ptr(eyePosition));
		glUniform1f(this->specularIntensity, 1.0f);
		glUniform1f(this->specularPower, 1.0f);
		glUniform1i(this->lightType, this->getType());
		glUniform2fv(this->screenSize, 1, glm::value_ptr(glm::vec2(windowSize.x, windowSize.y)));

		glUniform1f(this->spotLight_coneAngle, coneAngle);		//THIS WAS CHANGED TO FLOAT!!! CHECK SHADER!!!
		glUniform3fv(this->spotLight_coneDirection, 1, glm::value_ptr(coneDirection));

		glm::mat4 model(1);
		model = glm::translate(model, this->position);
		model = glm::scale(model, glm::vec3(this->sphereRadiuse, this->sphereRadiuse, this->sphereRadiuse));
		glUniformMatrix4fv(mvpHandle, 1, false, glm::value_ptr(viewProjection * model));

		this->getLightVolume()->render(*renderData);

	}

	void SpotLight3d::render(RenderData * renderData, const glm::mat4x4& mvp)
	{
		PointLight3d::render(renderData, mvp);
	}

	void SpotLight3d::setDirection(glm::vec3 direction)
	{
		this->coneDirection = direction;
	}


	void SpotLight3d::setConeAngle(float angle)
	{
		this->coneAngle = angle;
	}

	glm::vec3 SpotLight3d::getDirection()
	{
		return this->coneDirection;
	}

	float SpotLight3d::getConeAngle()
	{
		return this->coneAngle;
	}

	SpotLight3d::~SpotLight3d()
	{

	}

}
} }
