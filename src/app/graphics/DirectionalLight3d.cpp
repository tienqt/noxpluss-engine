/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/DirectionalLight3d.h>
#include <glm/vec3.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <nox/app/graphics/opengl/GBuffer.h>

namespace nox { namespace app { namespace graphics {

DirectionalLight3d::DirectionalLight3d()
{
}

DirectionalLight3d::DirectionalLight3d(std::shared_ptr<Mesh3d> quadLightVolume, glm::vec3 color, float ambientIntensity, float diffuseIntensity,
									   glm::vec3 direction) :
		BaseLight3d(quadLightVolume.get(), color, ambientIntensity, diffuseIntensity),
		direction(direction),
		quadLightVolume(quadLightVolume)
{
}

DirectionalLight3d::~DirectionalLight3d()
{

}

void DirectionalLight3d::init(GLuint shaderProgram, GLuint stenchilProgram)
{
	this->directionalLight_base_color = glGetUniformLocation(shaderProgram, "gDirectionalLight.Base.Color");
	this->directionalLight_base_ambientIntensity = glGetUniformLocation(shaderProgram, "gDirectionalLight.Base.AmbientIntensity");
	this->directionalLight_base_diffuseIntensity = glGetUniformLocation(shaderProgram, "gDirectionalLight.Base.DiffuseIntensity");
	this->directionalLight_direction = glGetUniformLocation(shaderProgram, "gDirectionalLight.Direction");
	
	this->eyeWorldPos = glGetUniformLocation(shaderProgram, "gEyeWorldPos");
	this->specularIntensity = glGetUniformLocation(shaderProgram, "gMatSpecularIntensity");
	this->specularPower = glGetUniformLocation(shaderProgram, "gSpecularPower");
	this->lightType = glGetUniformLocation(shaderProgram, "gLightType");
	this->screenSize = glGetUniformLocation(shaderProgram, "gScreenSize");

	this->posSampler = glGetUniformLocation(shaderProgram, "gPositionMap");
	this->colorSampler = glGetUniformLocation(shaderProgram, "gColorMap");
	this->normalSampler = glGetUniformLocation(shaderProgram, "gNormalMap");

	this->mvpHandle = glGetUniformLocation(shaderProgram, "mvpMatrix");

	this->identityMatrix = glm::mat4(1);

}

void DirectionalLight3d::render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection)
{
	glUniform1i(this->posSampler, GBuffer::GBUFFER_TEXTURE_TYPE_POSITION);
	glUniform1i(this->colorSampler, GBuffer::GBUFFER_TEXTURE_TYPE_DIFFUSE);
	glUniform1i(this->normalSampler, GBuffer::GBUFFER_TEXTURE_TYPE_NORMAL);

	glUniform3fv(this->directionalLight_base_color, 1, glm::value_ptr(this->getColor()));
	glUniform1f(this->directionalLight_base_ambientIntensity, 0.9f);
	glUniform1f(this->directionalLight_base_diffuseIntensity, 0.9f);
	glUniform3fv(this->directionalLight_direction, 1, glm::value_ptr(this->direction));

	//std::printf("x: %f, y: %f, z: %f\n", direction.x, direction.y, direction.z);

	glUniform3fv(this->eyeWorldPos, 1, glm::value_ptr(eyePosition));
	glUniform1f(this->specularIntensity, 1.0f);
	glUniform1f(this->specularPower, 1.0f);
	glUniform1i(this->lightType, 1);
	glUniform2fv(this->screenSize, 1, glm::value_ptr(glm::vec2(windowSize.x, windowSize.y)));

	glUniformMatrix4fv(mvpHandle, 1, false, glm::value_ptr(this->identityMatrix));

	this->getLightVolume()->render(*renderData);
}

void DirectionalLight3d::setDirection(glm::vec3 direction)
{
	this->direction = direction;
}

}
} }
