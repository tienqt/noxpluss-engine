/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include <nox/app/graphics/opengl/OpenGlRenderer3d.h>
#include <nox/app/graphics/Camera3d.h>
#include <nox/app/IContext.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/IResourceAccess.h>
#include <nox/logic/physics/box2d/BulletDebugDraw3d.h>

#include <glm/gtc/type_ptr.hpp>
#include <sstream>
#include <cstring>
#include <cassert>


#define COLOR_TEXTURE_UNIT_INDEX        0


namespace nox { namespace app { namespace graphics
{

namespace
{
	//std::vector<TextureQuad::VertexAttribute> makeTriangleStripBox(const math::Box<glm::vec2>& box);
}

OpenGlRenderer3d::OpenGlRenderer3d() :
	renderCamera(std::make_shared<Camera3d>(glm::uvec2(1, 1))),
	windowSizeChanged(false)
{
	debugRenderer = std::make_unique<nox::logic::physics::BulletDebugDraw3d>();
}

OpenGlRenderer3d::~OpenGlRenderer3d()
{
	delete d1;
}

bool OpenGlRenderer3d::init(IContext* context, const std::string& shaderDirectory, const glm::uvec2& windowSize)
{
	assert(context != nullptr);

	// Set up logger
	this->log = context->createLogger();
	this->log.setName("OpenGlRenderer3d");

	// Get access to resource cache (for loading the shader files)
	resource::IResourceAccess* resourceCache = context->getResourceAccess();
	assert(resourceCache != nullptr);

	// Initialize OpenGL
	if (this->initOpenGL(windowSize) == false)
	{
		return false;
	}

	std::string shaderNames[ShaderType::NUM_SHADERS];
	shaderNames[ShaderType::GEOMETRY_PASS]		= "geometryPass";
	shaderNames[ShaderType::DIRLIGHT_PASS]		= "directionalLight3d";
	shaderNames[ShaderType::POINTLIGHT_PASS]	= "pointLight3d";
	shaderNames[ShaderType::STENCIL_PASS]		= "stencilPass";
	shaderNames[ShaderType::SPOTLIGHT_PASS]		= "spotLight3d";
	shaderNames[ShaderType::DEBUG_PASS]			= "debug3d";

	VertexAndFragmentShader tmpShaders[NUM_SHADERS];

	for (int i = 0; i < NUM_SHADERS; i++)
	{
		this->shaderPrograms[i] = glCreateProgram();
		tmpShaders[i] = this->createVertexAndFragmentShader(resourceCache, shaderDirectory + shaderNames[i]);
		assert(tmpShaders[i].isValid());

		if (!linkShaderProgram(this->shaderPrograms[i], tmpShaders[i].vertex, tmpShaders[i].fragment))
		{
			this->log.error().format("Could not link %s shader.", shaderNames[i].c_str());
			return false;
		}
	}

	uniformHandels[UniformType::MVP] = glGetUniformLocation(this->shaderPrograms[ShaderType::GEOMETRY_PASS], "mvpMatrix");
	uniformHandels[UniformType::MODEL] = glGetUniformLocation(this->shaderPrograms[ShaderType::GEOMETRY_PASS], "modelMatrix");
	
	sphereMvpHandle = glGetUniformLocation(this->shaderPrograms[ShaderType::POINTLIGHT_PASS], "mvpMatrix");

	glBindAttribLocation(this->shaderPrograms[ShaderType::DEBUG_PASS], 0, "vertex");

	for (unsigned int i = 0; i < spotLights.size(); i++)
	{
		spotLights[i]->init(this->shaderPrograms[ShaderType::SPOTLIGHT_PASS], this->shaderPrograms[ShaderType::STENCIL_PASS]);
	}

	for (unsigned int i = 0; i < pointLights.size(); i++)
	{
		pointLights[i]->init(this->shaderPrograms[ShaderType::POINTLIGHT_PASS], this->shaderPrograms[ShaderType::STENCIL_PASS]);
	}


	d1->init(this->shaderPrograms[ShaderType::DIRLIGHT_PASS], 0);


	srand(9);	// TODO: noooo magic number

	return true;
}

void OpenGlRenderer3d::setGraphicsAssetManager(const std::shared_ptr<GraphicsAssetManager3d>& assetManager)
{
	this->assetManager = assetManager;
}

const std::shared_ptr<GraphicsAssetManager3d> OpenGlRenderer3d::getGraphicsAssetManager()
{
	return this->assetManager;
}

void OpenGlRenderer3d::addLight(std::shared_ptr<BaseLight3d> light, int type)
{
	switch (type)
	{
	case BaseLight3d::LightType::DIRECTIONAL:
		light->setLightVolume(this->quad.get());
		light->init(this->shaderPrograms[ShaderType::DIRLIGHT_PASS], this->shaderPrograms[ShaderType::STENCIL_PASS]);
		directionalLights.push_back(light);
		return;

	case BaseLight3d::LightType::POINT:
		light->setLightVolume(this->sphere.get());
		light->init(this->shaderPrograms[ShaderType::POINTLIGHT_PASS], this->shaderPrograms[ShaderType::STENCIL_PASS]);
		break;
	case BaseLight3d::LightType::SPOT:
		light->setLightVolume(this->sphere.get());
		light->init(this->shaderPrograms[ShaderType::SPOTLIGHT_PASS], this->shaderPrograms[ShaderType::STENCIL_PASS]);
		break;
	default:
		//logg error
		break;
	}
	light->setId(lightBooker.book(light));
	this->dynamicLights.push_back(light);
}

void OpenGlRenderer3d::removeLight(std::shared_ptr<BaseLight3d> light, int type)
{
	if (type == BaseLight3d::LightType::DIRECTIONAL)
	{
		for (int i = 0; i < this->directionalLights.size(); i++)
		{
			if (directionalLights[i]->getId() == light->getId())
			{
				this->lightBooker.unbook(light->getId());
				this->directionalLights.erase(directionalLights.begin() + i);
			}
		}
	}
	else
	{
		for (int i = 0; i < this->dynamicLights.size(); i++)
		{
			if (this->dynamicLights[i]->getId() == light->getId())
			{
				this->lightBooker.unbook(light->getId());
				this->dynamicLights.erase(dynamicLights.begin() + i);
			}
		}
	}
}



void OpenGlRenderer3d::renderLightVolume(BaseLight3d* p)
{
	renderData.bindShaderProgram(this->shaderPrograms[ShaderType::STENCIL_PASS]);
	gBuffer->BindForStencilPass();
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	glClear(GL_STENCIL_BUFFER_BIT);

	// We need the stencil test to be enabled but we want it
	// to succeed always. Only the depth test matters.
	glStencilFunc(GL_ALWAYS, 0, 0);

	glStencilOpSeparate(GL_BACK, GL_KEEP, GL_INCR_WRAP, GL_KEEP);
	glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_DECR_WRAP, GL_KEEP);

	p->render(&renderData, renderCamera->getViewProjectionMatrix());

}

void OpenGlRenderer3d::renderSpotLight(BaseLight3d* p)
{
	gBuffer->BindForLightPass();
	renderData.bindShaderProgram(this->shaderPrograms[ShaderType::SPOTLIGHT_PASS]);

	glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glm::mat4x4 viewProjection = renderCamera->getViewProjectionMatrix();
	p->render(&renderData, this->windowSize, renderCamera->getPosition(), viewProjection);

	glCullFace(GL_BACK);
	glDisable(GL_BLEND);
}

void OpenGlRenderer3d::renderPointLight(BaseLight3d* p)
{
	gBuffer->BindForLightPass();
	renderData.bindShaderProgram(this->shaderPrograms[ShaderType::POINTLIGHT_PASS]);

	glStencilFunc(GL_NOTEQUAL, 0, 0xFF);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glm::mat4x4 viewProjection = renderCamera->getViewProjectionMatrix();

	((PointLight3d*)p)->render(&renderData, this->windowSize, renderCamera->getPosition(), viewProjection);

	glCullFace(GL_BACK);
	glDisable(GL_BLEND);
}


void OpenGlRenderer3d::directionalLightPass()
{
	gBuffer->BindForLightPass();
	renderData.bindShaderProgram(this->shaderPrograms[ShaderType::DIRLIGHT_PASS]);

	glDisable(GL_CULL_FACE);
	//glDisable(GL_STENCIL);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	for (unsigned int i = 0; i < directionalLights.size(); i++)
	{
		directionalLights[i]->render(&renderData, windowSize, renderCamera->getPosition());
	}

	glDisable(GL_BLEND);
}

void OpenGlRenderer3d::finalPass()
{
	unsigned int WINDOW_WIDTH = windowSize.x;
	unsigned int WINDOW_HEIGHT = windowSize.y;

	gBuffer->BindForFinalPass();
	glBlitFramebuffer(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT,
		0, 0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_COLOR_BUFFER_BIT, GL_LINEAR);
}


void OpenGlRenderer3d::renderWorld()
{
	gBuffer->StartFrame();
	glViewport(0, 0, windowSize.x, windowSize.y);

	glm::mat4x4 viewProjection = renderCamera->getViewProjectionMatrix();
	renderData.bindShaderProgram(this->shaderPrograms[ShaderType::GEOMETRY_PASS]);
	gBuffer->BindForGeomPass();

	glDepthMask(GL_TRUE);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);

	GLuint colorSamplerr = glGetUniformLocation(renderData.getBoundShaderProgram(), "gColorMap");
	glUniform1i(colorSamplerr, COLOR_TEXTURE_UNIT_INDEX);

	// Render scene
	rootSceneNode->onTraverse(viewProjection, uniformHandels[UniformType::MVP], uniformHandels[UniformType::MODEL], renderData, rootSceneNode->getTransform(), alphaNodes);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	for (const auto aNode : this->alphaNodes)
	{
		glUniformMatrix4fv(uniformHandels[UniformType::MVP], 1, GL_FALSE, glm::value_ptr(viewProjection * aNode->concatModelMatrix));
		glUniformMatrix4fv(uniformHandels[UniformType::MODEL], 1, GL_FALSE, glm::value_ptr(aNode->concatModelMatrix));
		aNode->mesh->render(this->renderData);
	}
	glDisable(GL_BLEND);


	alphaNodes.clear();

	if (debugRenderer->getIsDebugModeEnabled())
	{
		renderData.bindShaderProgram(this->shaderPrograms[ShaderType::DEBUG_PASS]);
		debugRenderer->onDebugRender(&renderData, viewProjection);
	}

	glDepthMask(GL_FALSE);
}

void OpenGlRenderer3d::renderLitWorld()
{
	glEnable(GL_STENCIL_TEST);
	for (unsigned int i = 0; i < dynamicLights.size(); i++)
	{
		renderLightVolume(dynamicLights[i].get());
		switch (dynamicLights[i]->getType())
		{
		case BaseLight3d::LightType::POINT:
			renderPointLight(dynamicLights[i].get());
			break;
		case BaseLight3d::LightType::SPOT:
			renderSpotLight(dynamicLights[i].get());
			break;
		default:
			break;
		};
	}

	glDisable(GL_STENCIL_TEST);
	directionalLightPass();
	finalPass();
}

void OpenGlRenderer3d::onRender()
{
	assert(rootSceneNode);
	
	renderWorld();
	renderLitWorld();

	GLenum error = glGetError();
	while (error != GL_NO_ERROR)
	{
		this->log.error().format("OpenGL: %i", error);
		error = glGetError();
	}
}

bool OpenGlRenderer3d::compareZvalues(alphaNode * a, alphaNode * b)
{
	return a->zOffset > b->zOffset;
}

void OpenGlRenderer3d::setCamera(const std::shared_ptr<Camera3d>& camera)
{
    this->renderCamera = camera;
}


void OpenGlRenderer3d::setRootSceneNode(const std::shared_ptr<TransformationNode3d>& rootSceneNode)
{
	this->rootSceneNode = rootSceneNode;
	this->rootSceneNode->setCurrentRenderer(this);
}

bool OpenGlRenderer3d::toggleDebugRendering()
{
	//this->debugRenderer->toggleDebugDraw();
	//return this->debugRenderer->getIsDebugModeEnabled();
	//this->debugRenderer->enableDebugRenderer(true);
	return false;
}

bool OpenGlRenderer3d::initOpenGL(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();

	glGetError();
	if (glewError != GLEW_OK)
	{
		glewGetErrorString(glewError);
		this->log.error().format("Error initializing GLEW: %s", (const char *) glewGetErrorString(glewError));
		return false;
	}

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Error initializing OpenGL: %d", error);
		return false;
	}

	const unsigned char* glVersionString = static_cast<const unsigned char *>(glGetString(GL_VERSION));
	const unsigned char* glVendorString = static_cast<const unsigned char *>(glGetString(GL_VENDOR));
	const unsigned char* glslVersionString = static_cast<const unsigned char *>(glGetString(GL_SHADING_LANGUAGE_VERSION));

	GLint glMajor = 0;
	GLint glMinor = 0;
	glGetIntegerv(GL_MAJOR_VERSION, &glMajor);
	glGetIntegerv(GL_MINOR_VERSION, &glMinor);

	this->renderData = RenderData(GlVersion(glMajor, glMinor), nullptr);

	if (this->renderData.getGlVersion().getMajor() < 3)
	{
		this->log.error().format("Unsupported hardware detected: OpenGL %i.%i, with GLSL %s, using \"%s\" by \"%s\". OpenGL 3.0 or better is required.",
					this->renderData.getGlVersion().getMajor(),
					this->renderData.getGlVersion().getMinor(),
					 glslVersionString,
					 glVersionString,
					 glVendorString);

		return false;
	}

	this->log.verbose().format(
				   "Initialized OpenGL %i.%i, with GLSL %s, using \"%s\" by \"%s\".",
				   this->renderData.getGlVersion().getMajor(),
				   this->renderData.getGlVersion().getMinor(),
				   glslVersionString,
				   glVersionString,
				   glVendorString);


	// Draw only vertecies that are visible to the camera:
//	renderData->enable(GL_DEPTH_TEST);
//	renderData->enable(GL_BLEND);
//	renderData->depthFunc(GL_LESS);

//	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// TODO: make function. Parameters also not work

	// Draw wireframe only:
//	glPolygonMode(GL_FRONT_AND_BACK, GL_LINES);

	// Set background color
	glClearColor(0.0, 0.0, 0.0, 1.0);

	gBuffer = std::make_unique<GBuffer>();
	gBuffer->Init(windowSize.x, windowSize.y);

	sphere = std::make_shared<Mesh3d>();
	sphere->loadMesh("assets/models/lightmodels/sphere.obj");

	quad = std::make_shared<Mesh3d>();
	quad->loadMesh("assets/models/lightmodels/quad.obj");

	this->spotLights.push_back(new SpotLight3d(sphere, glm::vec3(1, 0,0), 1.0f, 1.0f, glm::vec3(0, 0, 1), 0.1f, 0.1f, 1.0f));

	for (int i = 0; i < 20; i++)
	{
		int z = rand() % (0 +20 + 1) -20;
		int y = rand() % (10 + 10 + 1) - 10;
		int x = rand() % (20 + 20 + 1) - 20;

		int r = rand() % 2; //((double)rand() / (RAND_MAX));
		int g = rand() % 2;//((double)rand() / (RAND_MAX));
		int b = rand() % 2;  //((double)rand() / (RAND_MAX));

		

		glm::vec3 position = glm::vec3(x, 0, z);
		glm::vec3 color = glm::vec3(r, g, b);

		pointLights.push_back(new PointLight3d(sphere, color, 1.0f, 1.0f, position, 1.0f, 1.0f, 1.0f));
	}

	d1 = new DirectionalLight3d(quad);
	
	return true;
}

void OpenGlRenderer3d::resizeWindow(const glm::uvec2& windowSize)
{
	this->windowSize = windowSize;

	glViewport(0, 0, (GLsizei)this->windowSize.x, (GLsizei)this->windowSize.y);
	
	this->windowSizeChanged = true;
	gBuffer->Init(windowSize.x, windowSize.y);

	const glm::vec2 windowSizeFloat(this->windowSize.x, this->windowSize.y);

	GLenum error = glGetError();
	if (error != GL_NO_ERROR)
	{
		this->log.error().format("Got error %d while resizing.", error);
	}
}

bool OpenGlRenderer3d::isDebugRenderingEnabled()
{
	return this->debugRenderer->getIsDebugModeEnabled();
}

GLuint OpenGlRenderer3d::createShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath, const GLenum shaderType) const
{
	// Use resourcecache to get handle to the file
	const auto shaderHandle = resourceCache->getHandle(resource::Descriptor(shaderPath));

	if (!shaderHandle)
	{
		this->log.error().format("Could not open shader resource \"%s\"", shaderPath.c_str());
		return 0;
	}

	// Get the shader file content
	std::istringstream shaderStream(std::string(shaderHandle->getResourceBuffer(), shaderHandle->getResourceBufferSize()));

	// Create the shader, using the file content
	auto shader = createShaderAutoVersion(shaderStream, shaderType, renderData.getGlVersion());

	if (shader.isCreated() == false)
	{
		this->log.error().format("Could not create shader \"%s\"", shaderPath.c_str());
		return 0;
	}
	else if (shader.isCompiled() == false)
	{
		const std::string errorString = shader.getCompileLog();

		if (errorString.empty() == false)
		{
			this->log.error().format("Could not compile shader \"%s\": %s", shaderPath.c_str(), errorString.c_str());
		}
		else
		{
			this->log.error().format("Could not compile shader \"%s\": Unknown reason.", shaderPath.c_str());
		}

		shader.destroy();
	}

	return shader.getId();
}

OpenGlRenderer3d::VertexAndFragmentShader OpenGlRenderer3d::createVertexAndFragmentShader(resource::IResourceAccess* resourceCache, const std::string& shaderPath) const
{
	VertexAndFragmentShader shader;
	shader.vertex = this->createShader(resourceCache, shaderPath + ".vert", GL_VERTEX_SHADER);
	shader.fragment = this->createShader(resourceCache, shaderPath + ".frag", GL_FRAGMENT_SHADER);

	return shader;
}

bool OpenGlRenderer3d::VertexAndFragmentShader::isValid() const
{
	return (this->vertex > 0 && this->fragment > 0);
}

nox::logic::physics::BulletDebugDraw3d* OpenGlRenderer3d::getDebugRenderer()
{
	return debugRenderer.get();
}

} } }
