#include <iostream>
#include "nox/app/graphics/SdlTexture3d.h"

namespace nox { namespace app { namespace graphics {

	SdlTexture3d::SdlTexture3d() : textureTarget(0),
		fileName(""),
		alpha(false)
{
}

void SdlTexture3d::init(unsigned int textureTarget, const std::string& fileName)
{
	this->textureTarget = textureTarget;
	this->fileName = fileName;
}

bool SdlTexture3d::load()
{
	glGenTextures(1, &this->textureObj);

	this->image = IMG_Load(this->fileName.c_str());

	if (this->image == nullptr)
	{
		std::cout << "img not loaded: " << this->fileName << std::endl;
		return false;
	}

	GLenum textureFormat = GL_RGB;
	GLenum texelFormat = GL_RGB;

	if (this->image->format->BytesPerPixel == 4)
	{
		textureFormat = GL_RGBA;
		texelFormat = GL_RGBA;
		this->alpha = true;
	}

	/*
	*  Asbj�rn Sporaland:
	*  SDL_image will load images in the BGR(A) format on some systems.
	*  Here we check if the blue color is the first color and sets the BGR(A) format if it is.
	*/
	if (this->image->format->Bmask == 0xFF)
	{
		texelFormat = GL_BGR;
		if (this->image->format->BytesPerPixel == 4)
		{
			texelFormat = GL_BGRA;
			this->alpha = true;
		}
	}

	glBindTexture(this->textureTarget, this->textureObj);
	glTexParameteri(this->textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(this->textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(this->textureTarget, 0, textureFormat, this->image->w, this->image->h, 0, texelFormat, GL_UNSIGNED_BYTE, this->image->pixels);
	glBindTexture(this->textureTarget, 0);
	SDL_FreeSurface(this->image);

	return true;
}

void SdlTexture3d::bind(GLenum TextureUnit)
{
	glActiveTexture(TextureUnit);
	glBindTexture(this->textureTarget, this->textureObj);
}

bool SdlTexture3d::hasAlpha()
{
	return this->alpha;
}

SdlTexture3d::~SdlTexture3d()
{
	glDeleteTextures(1, &this->textureObj);
}

} } }