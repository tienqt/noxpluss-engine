/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TexturePackerTextureAtlasReader.h>

namespace nox { namespace app
{
namespace graphics
{

bool TexturePackerTextureAtlasReader::readAtlasJson(const Json::Value& atlasObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	Json::Value framesArray = atlasObject.get("frames", Json::nullValue);
	if (framesArray.isNull() || framesArray.isArray() == false)
	{
		this->setErrorString("json atlas with no frames array.");
		return false;
	}

	Json::Value atlasMeta = atlasObject.get("meta", Json::nullValue);
	if (atlasMeta.isNull())
	{
		this->setErrorString("json atlas with no meta.");
		return false;
	}

	Json::Value atlasSize = atlasMeta.get("size", Json::nullValue);
	if (atlasSize.isNull())
	{
		this->setErrorString("json atlas with no meta size.");
		return false;
	}

	Json::Value atlasWidth = atlasSize.get("w", Json::nullValue);
	if (atlasWidth.isNull())
	{
		this->setErrorString("json atlas with no width specified.");
		return false;
	}

	Json::Value atlasHeight = atlasSize.get("h", Json::nullValue);
	if (atlasHeight.isNull())
	{
		this->setErrorString("json atlas with no height specified.");
		return false;
	}

	this->atlasWidth = atlasWidth.asUInt();
	this->atlasHeight = atlasHeight.asUInt();

	for (const Json::Value& frameObject : framesArray)
	{
		this->readAtlasFrame(frameObject, atlasId, texelsPerMeter);
	}

	return true;
}

bool TexturePackerTextureAtlasReader::readAtlasFrame(const Json::Value& frameObject, unsigned int atlasId, unsigned int texelsPerMeter)
{
	Json::Value frameName = frameObject.get("filename", Json::nullValue);
	if (frameName.isNull())
	{
		this->setErrorString("json atlas frame object without name.");
		return false;
	}

	Json::Value frame = frameObject.get("frame", Json::nullValue);
	if (frame.isNull())
	{
		this->setErrorString("json atlas frame object without frame.");
		return false;
	}

	Json::Value framePosX = frame.get("x", Json::nullValue);
	if (framePosX.isNull())
	{
		this->setErrorString("json atlas frame without x position.");
		return false;
	}

	Json::Value framePosY = frame.get("y", Json::nullValue);
	if (framePosY.isNull())
	{
		this->setErrorString("json atlas frame without y position.");
		return false;
	}

	Json::Value frameWidth = frame.get("w", Json::nullValue);
	if (frameWidth.isNull())
	{
		this->setErrorString("json atlas frame without width.");
		return false;
	}

	Json::Value frameHeight = frame.get("h", Json::nullValue);
	if (frameHeight.isNull())
	{
		this->setErrorString("json atlas frame without height.");
		return false;
	}

	TextureQuad sprite(atlasId);
	TextureQuad::RenderQuad spriteQuad;

	spriteQuad.bottomLeft.setPosition({0.0f, 0.0f});
	spriteQuad.bottomRight.setPosition({frameWidth.asFloat() / (float)texelsPerMeter, 0.0f});
	spriteQuad.topRight.setPosition({frameWidth.asFloat() / (float)texelsPerMeter, frameHeight.asFloat() / (float)texelsPerMeter});
	spriteQuad.topLeft.setPosition({0.0f, frameHeight.asFloat() / (float)texelsPerMeter});

	/*
	 * PNG is flipped upside down, so we flip the top and bottom y texture values.
	 */
	float textureCoordLeft = framePosX.asFloat() / (float)this->atlasWidth;
	float textureCoordRight = (framePosX.asFloat() + frameWidth.asFloat()) / (float)this->atlasWidth;
	float textureCoordTop = framePosY.asFloat() / (float)this->atlasHeight;
	float textureCoordBottom = (framePosY.asFloat() + frameHeight.asFloat()) / (float)this->atlasHeight;

	spriteQuad.bottomLeft.setTextureCoordinate({textureCoordLeft, textureCoordBottom});
	spriteQuad.bottomRight.setTextureCoordinate({textureCoordRight, textureCoordBottom});
	spriteQuad.topRight.setTextureCoordinate({textureCoordRight, textureCoordTop});
	spriteQuad.topLeft.setTextureCoordinate({textureCoordLeft, textureCoordTop});

	sprite.setRenderQuad(spriteQuad);
	this->spriteMap[frameName.asString()] = sprite;

	return true;
}

}
} }
