/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/RenderNode3d.h>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/graphics/actor/Animation3d.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/app/graphics/IRenderer3d.h>
#include <nox/util/chrono_utils.h>


namespace nox { namespace app
{
namespace graphics
{
	RendererNode3d::RendererNode3d(nox::logic::actor::Actor * owner)
{
	this->actorGraphics = owner->findComponent<nox::logic::graphics::ActorGraphics3d>();
	this->actorPhysics = owner->findComponent<nox::logic::physics::ActorPhysics3d>();
	this->name = this->actorGraphics->getModelName();
}

void RendererNode3d::onNodeEnter3d(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes)
{
	glUniformMatrix4fv(mvpHandle, 1, GL_FALSE, glm::value_ptr(viewProjection * modelMatrix));
	glUniformMatrix4fv(modelHandle, 1, GL_FALSE, glm::value_ptr(modelMatrix));

	std::vector<glm::mat4> boneTransforms;
	int hasAnimHandle = glGetUniformLocation(renderData.getBoundShaderProgram(), "hasAnimation");
	int activeAnimationI = this->actorGraphics->getAnimationIndex();

	GLuint colorOverlayHandle = glGetUniformLocation(renderData.getBoundShaderProgram(), "overlayColor");
	
	glUniform3fv(colorOverlayHandle, 1, glm::value_ptr(actorGraphics->overLayColor));

	if (activeAnimationI >= 0 && this->actorGraphics->getMeshdatas()->numAnimations() > 0)
	{
		glUniform1i(hasAnimHandle, 1);
		this->actorGraphics->getMeshdatas()->boneTransform(activeAnimationI, this->actorGraphics->getAnimationTime(), boneTransforms);	 // Step animation forward
	}
	else
	{
		glUniform1i(hasAnimHandle, 0);
	}

	for (unsigned int i = 0; i < boneTransforms.size(); i++) 
	{
		std::string name = "bones[" + std::to_string(i) + "]";
		GLuint boneLocation = glGetUniformLocation(renderData.getBoundShaderProgram(), name.c_str());
		glUniformMatrix4fv(boneLocation, 1, GL_TRUE, glm::value_ptr(boneTransforms[i]));
	}

	this->actorGraphics->getMeshdatas()->render(renderData); // ->getMeshesWithoutAlpha();

	this->previousMatrix = modelMatrix;

}


void RendererNode3d::onAttachToRenderer(IRenderer3d* renderer)
{
	this->previousMatrix = glm::mat4(1.0f);

} 

void RendererNode3d::updateAnimation(const nox::Duration& deltaTime)
{
	actorGraphics->stepAnimation(deltaTime);
}


void RendererNode3d::onDetachedFromRenderer()
{
	//this->getCurrentRenderer()->getTextureRenderer().deleteData(this->spriteDataHandle);
}


}
} }
