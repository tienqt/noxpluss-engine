/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/2d/SpriteRenderNode.h>

#include <nox/app/graphics/2d/OpenGlRenderer.h>

#include <cassert>
#include <glm/gtx/transform.hpp>

namespace nox { namespace app
{
namespace graphics
{

SpriteRenderNode::SpriteRenderNode():
	levelID(0),
	color(1.0f, 1.0f, 1.0f, 1.0f),
	lightMultiplier(1.0f),
	emissiveLight(0.0f),
	needUpdate(false)
{
}

void SpriteRenderNode::onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
	if (modelMatrix != this->previousMatrix || this->needUpdate == true)
	{
		const TextureQuad& activeSprite = this->spriteMap[this->activeSpriteName];

		glm::vec3 centerTranslation;
		centerTranslation.x = -(this->center.x * activeSprite.getWidth());
		centerTranslation.y = -(this->center.y * activeSprite.getHeight());

		glm::mat4 centeredModelMatrix = glm::translate(modelMatrix, centerTranslation);

		TextureQuad::RenderQuad transformedQuad = transform(activeSprite.getRenderQuad(), centeredModelMatrix);

		transformedQuad.setColor(this->color);
		transformedQuad.setLightMultiplier(this->lightMultiplier);
		transformedQuad.setEmissiveLight(this->emissiveLight);

		renderData.setData(this->spriteDataHandle, std::vector<TextureQuad::RenderQuad>(1, transformedQuad));

		this->previousMatrix = modelMatrix;
		this->needUpdate = false;
	}
}

void SpriteRenderNode::onAttachToRenderer(IRenderer* renderer)
{
	this->previousMatrix = glm::mat4(1.0f);

	const TextureManager& spriteManager = renderer->getTextureManager();

	for (std::string spriteName : this->spriteNames)
	{
		this->spriteMap[spriteName] = spriteManager.getTexture(spriteName);
	}

	this->spriteDataHandle = renderer->getTextureRenderer().requestDataSpace(1, this->levelID);
}

void SpriteRenderNode::setRenderLevel(const unsigned int level)
{
	this->levelID = level;
	
	if (this->spriteDataHandle.isValid())
	{
		this->getCurrentRenderer()->getTextureRenderer().deleteData(this->spriteDataHandle);
		this->spriteDataHandle = this->getCurrentRenderer()->getTextureRenderer().requestDataSpace(1, this->levelID);
	}
}

void SpriteRenderNode::addSprite(const std::string& spriteName)
{
	this->spriteNames.push_back(spriteName);
}

void SpriteRenderNode::setActiveSprite(const std::string& spriteName)
{
	this->activeSpriteName = spriteName;
	this->needUpdate = true;
}

void SpriteRenderNode::setCenter(const glm::vec2& center)
{
	this->center = center;
	this->needUpdate = true;
}

void SpriteRenderNode::setColor(const glm::vec4& color)
{
	this->color = color;
	this->needUpdate = true;
}

void SpriteRenderNode::setEmissiveLight(float emissive)
{
	this->emissiveLight = emissive;
	this->needUpdate = true;
}

void SpriteRenderNode::setLightMultiplier(float multiplier)
{
	this->lightMultiplier = multiplier;
	this->needUpdate = true;
}

void SpriteRenderNode::onDetachedFromRenderer()
{
	this->getCurrentRenderer()->getTextureRenderer().deleteData(this->spriteDataHandle);
}

}
} }
