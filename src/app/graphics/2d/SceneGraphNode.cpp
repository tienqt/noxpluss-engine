/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <nox/app/graphics/2d/SceneGraphNode.h>
#include <nox/app/graphics/RenderData.h>
#include <nox/util/algorithm.h>
#include <functional>
#include <thread>

namespace nox { namespace app
{
namespace graphics
{

SceneGraphNode::SceneGraphNode():
	parent(nullptr),
	currentRenderer2d(nullptr),
	currentRenderer3d(nullptr)
{
}

SceneGraphNode::~SceneGraphNode()
{
    this->children.clear();
}

void SceneGraphNode::addChild(const std::shared_ptr<SceneGraphNode>& child)
{
	if (this->currentRenderer2d != nullptr)
	{
		child->setCurrentRenderer(this->currentRenderer2d);
	}

	if (this->currentRenderer3d != nullptr)
	{
		child->setCurrentRenderer(this->currentRenderer3d);
	}

	child->parent = this;

	this->children.push_back(child);
}

void SceneGraphNode::removeChild(const std::shared_ptr<SceneGraphNode>& childToBeRemoved)
{
	auto childIt = std::find(this->children.begin(), this->children.end(), childToBeRemoved);

	if (childIt != this->children.end())
	{
		(*childIt)->removeCurrentRenderer();
		(*childIt)->onDetachedFromParent();

		util::eraseFast(this->children, childIt);
	}
}

void SceneGraphNode::removeChildren()
{
	for (const auto& child : this->children)
	{
		child->removeCurrentRenderer();
		child->onDetachedFromParent();
	}

	this->children.clear();
}

void SceneGraphNode::onTraverse(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{
    this->onNodeEnter(renderData, modelMatrix);
    
    for (auto& child : this->children)
    {
        child->onTraverse(renderData, modelMatrix);
    }

    this->onNodeLeave(renderData, modelMatrix);
}


void SceneGraphNode::onTraverse(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes)
{

	// start tr�d ------------

	//std::thread renderThread(&SceneGraphNode::onNodeEnter3d, std::ref(assManager), std::ref(viewProjection), mvpHandle, modelHandle, renderData, std::ref(modelMatrix), std::ref(alphaNodes), std::ref(*this));
	this->onNodeEnter3d(viewProjection, mvpHandle, modelHandle, renderData, modelMatrix, alphaNodes);
	
	glm::vec3 p1 = glm::vec3(modelMatrix[3]);

	for (auto& child : this->children)
	{
		child->onTraverse(viewProjection, mvpHandle, modelHandle, renderData, modelMatrix, alphaNodes);
	}

	// vent ------------------
	//renderThread.join();
	this->onNodeLeave3d(renderData, modelMatrix);
}

void SceneGraphNode::onAttachToRenderer(IRenderer* /*renderer*/)
{
}

void SceneGraphNode::onAttachToRenderer(IRenderer3d* /*renderer*/)
{

}

void SceneGraphNode::onNodeLeave(TextureRenderer& /*renderData*/, glm::mat4x4& /*modelMatrix*/)
{
}

void SceneGraphNode::onNodeLeave3d(RenderData& /*renderData*/, glm::mat4x4& /*modelMatrix*/)
{

}

void SceneGraphNode::onNodeEnter(TextureRenderer& renderData, glm::mat4x4& modelMatrix)
{

}


void SceneGraphNode::onNodeEnter3d(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes)
{

}

void SceneGraphNode::onDetachedFromRenderer()
{
}

void SceneGraphNode::setCurrentRenderer(IRenderer* renderer)
{
	if (this->currentRenderer2d != renderer)
	{
		this->currentRenderer2d = renderer;
		this->onAttachToRenderer(renderer);

		for (auto child : this->children)
		{
			child->setCurrentRenderer(renderer);
		}
	}
}

void SceneGraphNode::setCurrentRenderer(IRenderer3d* renderer)
{
	if (this->currentRenderer3d != renderer)
	{
		this->currentRenderer3d = renderer;
		this->onAttachToRenderer(renderer);

		for (auto child : this->children)
		{
			child->setCurrentRenderer(renderer);
		}
	}
}

void SceneGraphNode::removeCurrentRenderer()
{
	if (this->currentRenderer2d != nullptr)
	{
		this->onDetachedFromRenderer();

		this->currentRenderer2d = nullptr;
		for (auto& child : this->children)
		{
			child->removeCurrentRenderer();
		}
	}

	if (this->currentRenderer3d != nullptr)
	{
		this->onDetachedFromRenderer();

		this->currentRenderer3d = nullptr;
		for (auto& child : this->children)
		{
			child->removeCurrentRenderer();
		}
	}
}


SceneGraphNode* SceneGraphNode::getParent()
{
	return this->parent;
}


void SceneGraphNode::onDetachedFromParent()
{
	this->parent = nullptr;
}

IRenderer* SceneGraphNode::getCurrentRenderer()
{
	return this->currentRenderer2d;
}

IRenderer3d* SceneGraphNode::getCurrentRenderer3d()
{
	return this->currentRenderer3d;
}

bool SceneGraphNode::hasNodeInTree(const std::shared_ptr<SceneGraphNode>& node) const
{
	bool nodeFound = false;
	auto childIt = this->children.begin();

	while (nodeFound == false && childIt != this->children.end())
	{
		if (*childIt == node)
		{
			nodeFound = true;
		}
		else
		{
			nodeFound = (*childIt)->hasNodeInTree(node);
		}

		childIt++;
	}

	return nodeFound;
}

}
} }
