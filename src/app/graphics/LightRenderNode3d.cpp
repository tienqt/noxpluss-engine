/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/IRenderer3d.h>
#include <nox/app/graphics/LightRenderNode3d.h>
#include <nox/app/graphics/PointLight3d.h>
#include <nox/app/graphics/Mesh3d.h>

#include <nox/logic/actor/component/Transform3d.h>

#include <cassert>

namespace nox { namespace app { namespace graphics
{

LightRenderNode3d::LightRenderNode3d()
{
	this->attachedToRender = false;
}

void LightRenderNode3d::setActorTransform(nox::logic::actor::Transform3d* actorTransform)
{
	this->actorTransform = actorTransform;
	updatePosition();
}

void LightRenderNode3d::onNodeEnter(TextureRenderer& /*renderData*/, glm::mat4x4& /*modelMatrix*/)
{
	//if (this->light->needsRenderUpdate())
	//{
	//	this->getCurrentRenderer()->lightUpdate(this->light);
	//	this->light->setRenderingUpdated();
	//}
}

void LightRenderNode3d::onAttachToRenderer(IRenderer3d* renderer)
{
		std::string name = this->light->getName();
		renderer->addLight(this->light, this->type);
		this->attachedToRender = true;
}

void LightRenderNode3d::onDetachedFromRenderer()
{
	this->getCurrentRenderer3d()->removeLight(this->light, this->type); 
}

//void LightRenderNode3d::setLight(const std::shared_ptr<Light>& light)
//{
//	this->light = light;
//}
//
//const std::shared_ptr<Light>& LightRenderNode3d::getLight() const
//{
//	return this->light;
//}


void LightRenderNode3d::updatePosition()
{
	if (this->type == nox::app::graphics::BaseLight3d::LightType::DIRECTIONAL)
	{
		return;
	}
	((PointLight3d*)(this->light.get()))->setPosition(actorTransform->getPosition());
}


void LightRenderNode3d::setLight(std::shared_ptr<BaseLight3d>& light, int type)
{
	this->light = light;
	this->type = type;
}

std::shared_ptr<BaseLight3d>& LightRenderNode3d::getLight()
{
	return this->light;
}

int LightRenderNode3d::getType()
{
	return this->type;
}

} } } 
