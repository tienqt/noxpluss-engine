/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/BaseLight3d.h>
#include <nox/app/graphics/Mesh3d.h>

namespace nox { namespace app { namespace graphics
{

BaseLight3d::BaseLight3d()
{
}

BaseLight3d::BaseLight3d(Mesh3d* lightVolume, glm::vec3 color, float ambientIntensity, float diffuseIntensity) : 
	lightVolume(lightVolume),
	color(color),
	ambientIntensity(ambientIntensity),
	diffusseIntensity(diffuseIntensity)
{
}

BaseLight3d::~BaseLight3d()
{

}

void BaseLight3d::setType(int type)
{
	this->type = type;
}

int BaseLight3d::getType()
{
	return this->type;
}

void BaseLight3d::setName(std::string name)
{
	this->name = name;
}

void BaseLight3d::setId(int id)
{
	this->lightId = id;
}

std::string BaseLight3d::getName()
{
	return this->name;
}

int BaseLight3d::getId()
{
	return this->lightId;
}

void BaseLight3d::init(GLuint shaderProgram, GLuint stenchilProgram)
{

}

void BaseLight3d::render(RenderData* renderData, const glm::mat4x4& mvp)
{

}

void BaseLight3d::render(RenderData* renderData, glm::vec2 windowSize, glm::vec3 eyePosition, const glm::mat4& viewProjection)
{

}


void BaseLight3d::setLightVolume(Mesh3d* lightVolume)
{
	this->lightVolume = lightVolume;
}

void BaseLight3d::setColor(glm::vec3 color)
{
	this->color = color;
}

void BaseLight3d::setAmbientIntensity(float ambientIntensity)
{
	this->ambientIntensity = ambientIntensity;
}
void BaseLight3d::setDiffuseIntensity(float diffuseIntensity)
{
	this->diffusseIntensity = diffuseIntensity;
}

Mesh3d* BaseLight3d::getLightVolume()
{
	return this->lightVolume;
}

glm::vec3 BaseLight3d::getColor()
{
	return this->color;
}
float BaseLight3d::getAmbientIntensity()
{
	return this->ambientIntensity;
}
float BaseLight3d::getDiffuseIntensity()
{
	return this->diffusseIntensity;
}


} } }
