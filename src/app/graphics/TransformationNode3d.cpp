/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/TransformationNode3d.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <glm/gtc/type_ptr.hpp>

namespace nox { namespace app
{
namespace graphics
{

TransformationNode3d::TransformationNode3d() :
	transformationComponent(nullptr)
{
}

TransformationNode3d::TransformationNode3d(const nox::logic::actor::Transform3d* transformation, std::string name) :
	name(name)
{
	this->transformationComponent = transformation;
	this->transformationMatrix = this->transformationComponent->getTransformMatrix();
}

TransformationNode3d::~TransformationNode3d()
{
    
}


void TransformationNode3d::onNodeEnter3d(glm::mat4x4& viewProjection, unsigned int mvpHandle, unsigned int modelHandle, RenderData& renderData, glm::mat4x4& modelMatrix, std::vector<alphaNode*> & alphaNodes)
{
	this->savedMatrix = modelMatrix;
	modelMatrix = modelMatrix * transformationMatrix;

	glm::vec3 p1 = glm::vec3(savedMatrix[3]);
	glm::vec3 p2 = glm::vec3(transformationMatrix[3]);

}

void TransformationNode3d::onNodeLeave3d(RenderData& renderData, glm::mat4x4& modelMatrix)
{
	modelMatrix = this->savedMatrix;
}

void TransformationNode3d::setTransform(const glm::mat4& matrix)
{
	this->transformationMatrix = matrix;
}

glm::mat4& TransformationNode3d::getTransform()
{
	return this->transformationMatrix;
}


}
} }
