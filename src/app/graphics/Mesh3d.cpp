/*

Copyright 2011 Etay Meiri

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.


This code is modified by the NOX-pluss-group.

*/

#include <glm/gtx/quaternion.hpp>

#include <nox/app/graphics/Mesh3d.h>
#include <nox/app/graphics/SdlTexture3d.h>

namespace nox { namespace app { namespace graphics 
{

#define POSITION_LOCATION    0
#define NORMAL_LOCATION      1
#define COLOR_LOCATION       2
#define TEX_COORD_LOCATION   3
#define BONE_ID_LOCATION     4
#define BONE_WEIGHT_LOCATION 5

void Mesh3d::VertexBoneData::addBoneData(unsigned int BoneID, float Weight)
{
	for (int i = 0; i < sizeof(ids) / sizeof(ids[0]); i++) {
		if (weights[i] == 0.0) {
			ids[i] = BoneID;
			weights[i] = Weight;
			return;
		}
	}

	// should never get here - more bones than we have space for
	assert(0);
}

Mesh3d::Mesh3d() :
VAO(0),
numBones(0),
meshAiScene(nullptr),
numIndx(0),
animationFramesPerSecond(0.0f)
//,
//currentAnimation(nullptr)
{
	for (int i = 0; i < sizeof(buffers) / sizeof(buffers[0]); i++)
		buffers[0] = 0;
}


Mesh3d::~Mesh3d()
{
	clear();
}


void Mesh3d::clear()
{
//	for (unsigned int i = 0; i < textures.size(); i++) {
//	//	if (textures[i] != nullptr)
////			delete(textures[i]);
//	}

	if (buffers[0] != 0) {
		glDeleteBuffers(sizeof(buffers) / sizeof(buffers[0]), buffers);
	}

	if (VAO != 0) {
		glDeleteVertexArrays(1, &VAO);
		VAO = 0;
	}
}


bool Mesh3d::loadMesh(const std::string& Filename)
{
	// Release the previously loaded mesh (if it exists)
	clear();

	// Create the VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// Create the buffers for the vertices attributes
	glGenBuffers(sizeof(buffers) / sizeof(buffers[0]), buffers);

	bool Ret = false;

	this->meshAiScene = importer.ReadFile(Filename.c_str(), aiProcess_LimitBoneWeights | aiProcess_Triangulate | aiProcess_FlipUVs | aiProcess_GenNormals);	// aiProcess_GenSmoothNormals

	if (meshAiScene)
	{
		globalInverseTransform = convertToGlmMatrix(meshAiScene->mRootNode->mTransformation);
		globalInverseTransform = glm::inverse(globalInverseTransform);
		Ret = initFromScene(meshAiScene, Filename);
	}
	else {
		printf("Error parsing '%s': '%s'\n", Filename.c_str(), importer.GetErrorString());
	}

	// Make sure the VAO is not changed from the outside
	glBindVertexArray(0);

	float siz = sizeof(glm::mat4);

	return Ret;
}



bool Mesh3d::initFromScene(const aiScene* scene, const std::string& filename)
{
	this->fileName = filename;
	entries.resize(scene->mNumMeshes);
	textures.resize(scene->mNumMaterials);

	int NumVertices = 0;
	int Numindices = 0;

	// Count the number of vertices and indices
	for (unsigned int i = 0; i < entries.size(); i++) {
		entries[i].materialIndex = scene->mMeshes[i]->mMaterialIndex;
		entries[i].numIndices = scene->mMeshes[i]->mNumFaces * 3;
		entries[i].baseVertex = NumVertices;
		entries[i].baseIndex = Numindices;

		NumVertices += scene->mMeshes[i]->mNumVertices;
		Numindices += entries[i].numIndices;
	}

	// Reserve space in the vectors for the vertex attributes and indices
	positions.reserve(NumVertices);
	normals.reserve(NumVertices);
	colors.reserve(NumVertices);
	texCoords.reserve(NumVertices);
	bones.resize(NumVertices);
	indices.reserve(Numindices);


	// Initialize the meshes in the scene one by one
	for (unsigned int i = 0; i < entries.size(); i++) {
		const aiMesh* paiMesh = scene->mMeshes[i];
		initMesh(i, paiMesh, positions, normals, colors, texCoords, bones, indices);
	}

	///////////////////////////////////////////////////////////////////////////////

	if (scene->HasAnimations())
	{
		this->generateAnimationFrames(60.0f);		// TODO: move this another place/make it more customizable??
	}

	if (!initMaterials(scene, filename)) {
		return false;
	}

	// buffer vertex positions
	this->updateVertices();

	// buffer texture coordinates
	glBindBuffer(GL_ARRAY_BUFFER, buffers[TEXCOORD_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORD_LOCATION);
	glVertexAttribPointer(TEX_COORD_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	// buffer colors
	glBindBuffer(GL_ARRAY_BUFFER, buffers[COLOR_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors[0]) * colors.size(), &colors[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(COLOR_LOCATION);
	glVertexAttribPointer(COLOR_LOCATION, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// buffer normals
	glBindBuffer(GL_ARRAY_BUFFER, buffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// buffer bones
	glBindBuffer(GL_ARRAY_BUFFER, buffers[BONE_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bones[0]) * bones.size(), &bones[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(BONE_ID_LOCATION);
	glVertexAttribIPointer(BONE_ID_LOCATION, 4, GL_INT, sizeof(VertexBoneData), (const GLvoid*)0);
	glEnableVertexAttribArray(BONE_WEIGHT_LOCATION);
	glVertexAttribPointer(BONE_WEIGHT_LOCATION, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBoneData), (const GLvoid*)16);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	return (glGetError() == GL_NO_ERROR);
}

bool Mesh3d::updateVertices()
{
	// Generate and populate the buffers with vertex attributes and the indices
	glBindBuffer(GL_ARRAY_BUFFER, buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	return (glGetError() == GL_NO_ERROR);
}


void Mesh3d::initMesh(int MeshIndex,
	const aiMesh* paiMesh,
	std::vector<glm::vec3>& positions,
	std::vector<glm::vec3>& normals,
	std::vector<glm::vec4>& colors,
	std::vector<glm::vec2>& texCoords,
	std::vector<VertexBoneData>& bones,
	std::vector<unsigned int>& indices)
{
	const aiVector3D Zero3D(0.0f, 0.0f, 0.0f);
	const aiColor4D defaultColor4D(1, 1, 1, 1);

	// Populate the vertex attribute vectors
	for (unsigned int i = 0; i < paiMesh->mNumVertices; i++) {
		const aiVector3D* pPos = &(paiMesh->mVertices[i]);
		const aiVector3D* pNormal = &(paiMesh->mNormals[i]);
		const aiColor4D* pColor = paiMesh->HasVertexColors(0) ? paiMesh->mColors[0] : &defaultColor4D;			// TODO: handle more than one color per vertex
		const aiVector3D* pTexCoord = paiMesh->HasTextureCoords(0) ? &(paiMesh->mTextureCoords[0][i]) : &Zero3D;

		positions.push_back(glm::vec3(pPos->x, pPos->y, pPos->z));
		normals.push_back(glm::vec3(pNormal->x, pNormal->y, pNormal->z));
		colors.push_back(glm::vec4(1, 1, 1, 1));
		texCoords.push_back(glm::vec2(pTexCoord->x, pTexCoord->y));

		// TODO: optimize this:
		//allVertices.push_back(glm::vec3(pPos->x, pPos->y, pPos->z));
	}

	std::vector<glm::mat4> mats;
	loadBones(MeshIndex, paiMesh, bones);

	// Populate the index buffer
	for (unsigned int i = 0; i < paiMesh->mNumFaces; i++) {
		const aiFace& face = paiMesh->mFaces[i];

		assert(face.mNumIndices == 3);

		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);

		//// TODO: optimize this:
		if (numIndx < positions.size())		 // TODO: don't use this if()
		{
			allIndices.push_back(numIndx + face.mIndices[0]);
			allIndices.push_back(numIndx + face.mIndices[1]);
			allIndices.push_back(numIndx + face.mIndices[2]);
		}
	}
	numIndx += paiMesh->mNumFaces * 3;


}


void Mesh3d::loadBones(int meshIndex, const aiMesh* mesh, std::vector<VertexBoneData>& bones)
{
	float size = 0;

	for (unsigned int i = 0; i < mesh->mNumBones; i++) {
		int boneIndex = 0;
		std::string boneName(mesh->mBones[i]->mName.data);

		if (boneMapping.find(boneName) == boneMapping.end()) {
			// Allocate an index for a new bone
			boneIndex = numBones;
			numBones++;
			BoneInfo bi;
			this->boneInfo.push_back(bi);
			this->boneInfo[boneIndex].boneOffset = convertToGlmMatrix(mesh->mBones[i]->mOffsetMatrix);
			boneMapping[boneName] = boneIndex;
		}
		else {
			boneIndex = boneMapping[boneName];
		}

		
		for (unsigned int j = 0; j < mesh->mBones[i]->mNumWeights; j++) {
			int vertexID = entries[meshIndex].baseVertex + mesh->mBones[i]->mWeights[j].mVertexId;
			float weight = mesh->mBones[i]->mWeights[j].mWeight;
			bones[vertexID].addBoneData(boneIndex, weight);
			size += sizeof(bones[vertexID]);
		}
	}


}


bool Mesh3d::initMaterials(const aiScene* scene, const std::string& filename)
{
	// Extract the directory part from the file name
	std::string::size_type SlashIndex = filename.find_last_of("/");
	std::string Dir;

	if (SlashIndex == std::string::npos) {
		Dir = ".";
	}
	else if (SlashIndex == 0)
	{
		Dir = "/";
	}
	else
	{
		Dir = filename.substr(0, SlashIndex);
	}

	bool Ret = true;

	// Initialize the materials
	for (unsigned int i = 0; i < scene->mNumMaterials; i++)
	{
		const aiMaterial* pMaterial = scene->mMaterials[i];

		textures[i] = NULL;

		if (pMaterial->GetTextureCount(aiTextureType_DIFFUSE) > 0)
		{
			aiString Path;

			if (pMaterial->GetTexture(aiTextureType_DIFFUSE, 0, &Path, NULL, NULL, NULL, NULL, NULL) == AI_SUCCESS)
			{
				std::string p(Path.data);

				if (p.substr(0, 2) == ".\\") {
					p = p.substr(2, p.size() - 2);
				}

				std::string FullPath = Dir + "/" + p;

				textures[i] = std::make_shared<SdlTexture3d>();
				textures[i]->init(GL_TEXTURE_2D, FullPath.c_str());

				if (!textures[i]->load())
				{
					printf("Error loading texture '%s'\n", FullPath.c_str());
					//delete textures[i];
					textures[i] = NULL;
					Ret = false;
				}
				else
				{
					printf("%d - loaded texture '%s'\n", i, FullPath.c_str());
				}
			}
		}
	}

	return Ret;
}


void Mesh3d::render(RenderData& renderData)
{
	renderData.bindVertexArray(VAO);

	for (unsigned int i = 0; i < entries.size(); i++)
	{
		const unsigned int materialIndex = entries[i].materialIndex;
		assert(materialIndex < textures.size());
		GLuint numTexHandle = glGetUniformLocation(renderData.getBoundShaderProgram(), "numTextures");

		if (textures[materialIndex] != 0)
		{
			textures[materialIndex]->bind(GL_TEXTURE0);
			glUniform1i(numTexHandle, 1);		// Has texture
		}
		else
		{
			glUniform1i(numTexHandle, 0);		// No texture
		}

		glDrawElementsBaseVertex(GL_TRIANGLES,
			entries[i].numIndices,
			GL_UNSIGNED_INT,
			(void*)(sizeof(int) * entries[i].baseIndex),
			entries[i].baseVertex);

	}
}


int Mesh3d::findPosition(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	for (unsigned int i = 0; i < pNodeAnim->mNumPositionKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mPositionKeys[i + 1].mTime)
		{
			return i;
		}
	}

	//assert(0);

	return 0;
}


int Mesh3d::findRotation(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumRotationKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumRotationKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mRotationKeys[i + 1].mTime)
		{
			return i;
		}
	}

	//assert(0);

	return 0;
}


int Mesh3d::findScaling(float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	assert(pNodeAnim->mNumScalingKeys > 0);

	for (unsigned int i = 0; i < pNodeAnim->mNumScalingKeys - 1; i++)
	{
		if (AnimationTime < (float)pNodeAnim->mScalingKeys[i + 1].mTime)
		{
			return i;
		}
	}

	//assert(0);

	return 0;
}


void Mesh3d::calcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumPositionKeys == 1)
	{
		Out = pNodeAnim->mPositionKeys[0].mValue;
		return;
	}

	unsigned int PositionIndex = findPosition(AnimationTime, pNodeAnim);
	unsigned int NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->mNumPositionKeys);
	float DeltaTime = (float)(pNodeAnim->mPositionKeys[NextPositionIndex].mTime - pNodeAnim->mPositionKeys[PositionIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mPositionKeys[PositionIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mPositionKeys[PositionIndex].mValue;
	const aiVector3D& End = pNodeAnim->mPositionKeys[NextPositionIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}


void Mesh3d::calcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	// we need at least two values to interpolate...
	if (pNodeAnim->mNumRotationKeys == 1) {
		Out = pNodeAnim->mRotationKeys[0].mValue;
		return;
	}

	unsigned int RotationIndex = findRotation(AnimationTime, pNodeAnim);
	unsigned int NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->mNumRotationKeys);
	float DeltaTime = (float)(pNodeAnim->mRotationKeys[NextRotationIndex].mTime - pNodeAnim->mRotationKeys[RotationIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mRotationKeys[RotationIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiQuaternion& StartRotationQ = pNodeAnim->mRotationKeys[RotationIndex].mValue;
	const aiQuaternion& EndRotationQ = pNodeAnim->mRotationKeys[NextRotationIndex].mValue;
	aiQuaternion::Interpolate(Out, StartRotationQ, EndRotationQ, Factor);
	Out = Out.Normalize();
}



void Mesh3d::calcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim)
{
	if (pNodeAnim->mNumScalingKeys == 1) {
		Out = pNodeAnim->mScalingKeys[0].mValue;
		return;
	}

	unsigned int ScalingIndex = findScaling(AnimationTime, pNodeAnim);
	unsigned int NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->mNumScalingKeys);
	float DeltaTime = (float)(pNodeAnim->mScalingKeys[NextScalingIndex].mTime - pNodeAnim->mScalingKeys[ScalingIndex].mTime);
	float Factor = (AnimationTime - (float)pNodeAnim->mScalingKeys[ScalingIndex].mTime) / DeltaTime;
	//assert(Factor >= 0.0f && Factor <= 1.0f);
	const aiVector3D& Start = pNodeAnim->mScalingKeys[ScalingIndex].mValue;
	const aiVector3D& End = pNodeAnim->mScalingKeys[NextScalingIndex].mValue;
	aiVector3D Delta = End - Start;
	Out = Start + Factor * Delta;
}

glm::mat4 Mesh3d::mulitplyMatrix(const glm::mat4& m, glm::mat4& Right)
{
	glm::mat4 Ret;

	for (unsigned int i = 0; i < 4; i++)
	{
		for (unsigned int j = 0; j < 4; j++)
		{
			Ret[i][j] = m[i][0] * Right[0][j] +
				m[i][1] * Right[1][j] +
				m[i][2] * Right[2][j] +
				m[i][3] * Right[3][j];
		}
	}

	return Ret;
}



void Mesh3d::readNodeHeirarchy(float AnimationTime, aiAnimation * animation, const aiNode* pNode, const glm::mat4& ParentTransform)
{
	std::string NodeName(pNode->mName.data);

	glm::mat4 NodeTransformation = convertToGlmMatrix(pNode->mTransformation);

	if (animation != nullptr)
	{
		const aiNodeAnim* pNodeAnim = findNodeAnim(animation, NodeName);

		if (pNodeAnim)
		{
			// Interpolate scaling and generate scaling transformation matrix
			aiVector3D Scaling;
			calcInterpolatedScaling(Scaling, AnimationTime, pNodeAnim);
			glm::mat4 ScalingM = (glm::scale(glm::mat4(1), glm::vec3(Scaling.x, Scaling.y, Scaling.z)));

			// Interpolate rotation and generate rotation transformation matrix
			aiQuaternion RotationQ;
			calcInterpolatedRotation(RotationQ, AnimationTime, pNodeAnim);
			glm::quat glmRotQ = glm::quat(RotationQ.x, RotationQ.y, RotationQ.z, RotationQ.w);
			glm::mat4 RotationM = convertToGlmMatrix(RotationQ.GetMatrix());

			// Interpolate translation and generate translation transformation matrix
			aiVector3D Translation;
			calcInterpolatedPosition(Translation, AnimationTime, pNodeAnim);
			glm::mat4 TranslationM = glm::transpose(glm::translate(glm::mat4(1), glm::vec3(Translation.x, Translation.y, Translation.z)));

			// Combine the above transformations

			NodeTransformation = mulitplyMatrix(mulitplyMatrix(TranslationM, RotationM), ScalingM);
			//	NodeTransformation = TranslationM * RotationM *ScalingM;

		}
	}

	glm::mat4 GlobalTransformation = mulitplyMatrix(ParentTransform, NodeTransformation);

	if (boneMapping.find(NodeName) != boneMapping.end())
	{
		int BoneIndex = boneMapping[NodeName];
		boneInfo[BoneIndex].finalTransformation = mulitplyMatrix(mulitplyMatrix(globalInverseTransform, GlobalTransformation), boneInfo[BoneIndex].boneOffset);
	}

	for (unsigned int i = 0; i < pNode->mNumChildren; i++)
	{
		readNodeHeirarchy(AnimationTime, animation, pNode->mChildren[i], GlobalTransformation);
	}
}

//float Mesh3d::getAnimatonDuration(unsigned int animationIndex)
//{
//	if (animationIndex < this->meshAiScene->mNumAnimations)
//	{
//		float tps = this->meshAiScene->mAnimations[animationIndex]->mTicksPerSecond;
//		float numTicks = this->meshAiScene->mAnimations[animationIndex]->mDuration;
//
//		float seconds = tps / numTicks;
//		return seconds;
//	}
//	return 0.0;
//}

int Mesh3d::numAnimations()
{
	return this->meshAiScene->mNumAnimations;
}

void Mesh3d::boneTransform(unsigned int animationIndex, float time, std::vector<glm::mat4>& Transforms)
{
	aiAnimation * anim = this->meshAiScene->mAnimations[animationIndex];

	//int currentFrame = floor((time / (1.0f / animationFramesPerSecond)) + 0.5f);	// TODO: use this if rounding is needed
	int currentFrame = (int)(time / (1.0f / animationFramesPerSecond));
	int frameNumber = currentFrame % animationBoneFrames[anim].size();
	Transforms = animationBoneFrames[anim][frameNumber];
}


const aiNodeAnim* Mesh3d::findNodeAnim(const aiAnimation* pAnimation, const std::string NodeName)
{
	for (unsigned int i = 0; i < pAnimation->mNumChannels; i++) {
		const aiNodeAnim* pNodeAnim = pAnimation->mChannels[i];

		if (std::string(pNodeAnim->mNodeName.data) == NodeName) {
			return pNodeAnim;
		}
	}

	return NULL;
}

std::vector<glm::vec3> & Mesh3d::getVertices()
{
	return positions;
}

std::vector<unsigned int> Mesh3d::getIndices()
{
	return allIndices;
}



// TODO: take reference matrix instead of return
glm::mat4& Mesh3d::convertToGlmMatrix(const aiMatrix4x4 AssimpMatrix)
{
	glm::mat4 m;

	m[0][0] = AssimpMatrix.a1; m[0][1] = AssimpMatrix.a2; m[0][2] = AssimpMatrix.a3; m[0][3] = AssimpMatrix.a4;
	m[1][0] = AssimpMatrix.b1; m[1][1] = AssimpMatrix.b2; m[1][2] = AssimpMatrix.b3; m[1][3] = AssimpMatrix.b4;
	m[2][0] = AssimpMatrix.c1; m[2][1] = AssimpMatrix.c2; m[2][2] = AssimpMatrix.c3; m[2][3] = AssimpMatrix.c4;
	m[3][0] = AssimpMatrix.d1; m[3][1] = AssimpMatrix.d2; m[3][2] = AssimpMatrix.d3; m[3][3] = AssimpMatrix.d4;

	return m;
}

// TODO: take reference matrix instead of return
glm::mat4& Mesh3d::convertToGlmMatrix(const aiMatrix3x3 AssimpMatrix)
{
	glm::mat4 m;

	m[0][0] = AssimpMatrix.a1; m[0][1] = AssimpMatrix.a2; m[0][2] = AssimpMatrix.a3; m[0][3] = 0.0f;
	m[1][0] = AssimpMatrix.b1; m[1][1] = AssimpMatrix.b2; m[1][2] = AssimpMatrix.b3; m[1][3] = 0.0f;
	m[2][0] = AssimpMatrix.c1; m[2][1] = AssimpMatrix.c2; m[2][2] = AssimpMatrix.c3; m[2][3] = 0.0f;
	m[3][0] = 0.0f; m[3][1] = 0.0f; m[3][2] = 0.0f; m[3][3] = 1.0f;

	return m;
} 

void Mesh3d::generateAnimationFrames(float framesPerSecond)
{
	this->animationFramesPerSecond = framesPerSecond;
	for (int i = 0; i < this->meshAiScene->mNumAnimations; i++)
	{
		aiAnimation * animation = this->meshAiScene->mAnimations[i];

		std::vector<std::vector<glm::mat4>> generatedFrames;

		float TimeInSeconds = 0;
		float secsPerFrame = 1.0f / framesPerSecond;	// 1 sec per X frames
		int numFrames = (int)(animation->mDuration / secsPerFrame);

		generatedFrames.resize(numFrames);

		float size = 0;

		for (int f = 0; f < numFrames; f++)
		{
			glm::mat4 Identity = glm::mat4(1);

			float TicksPerSecond = (float)(animation->mTicksPerSecond != 0 ? animation->mTicksPerSecond : 25.0f);
			float TimeInTicks = TimeInSeconds * TicksPerSecond;

			float AnimationTime = fmod(TimeInTicks, (float)animation->mDuration);
			readNodeHeirarchy(AnimationTime, animation, this->meshAiScene->mRootNode, Identity);


			generatedFrames[f].resize(numBones);

			for (int b = 0; b < numBones; b++)
			{
				generatedFrames[f].resize(numBones);

				generatedFrames[f][b] = this->boneInfo[b].finalTransformation;

				size += sizeof(generatedFrames[f][b]);
			}

			TimeInSeconds += secsPerFrame;
		}

		float animSize = sizeof(generatedFrames[0][0]);
		this->animationBoneFrames[animation] = generatedFrames;
	}
}

int Mesh3d::getAnimationIndexByName(std::string name)
{
	for (unsigned int i = 0; i < this->meshAiScene->mNumAnimations; i++)
	{
		if (!strcmp(this->meshAiScene->mAnimations[i]->mName.C_Str(), name.c_str()))
		{
			return i;
		}
	}
	return -1;
}

std::string Mesh3d::getAnimationNameByIndex(unsigned int i)
{
	if (i < this->meshAiScene->mNumAnimations)
	{
		return this->meshAiScene->mAnimations[i]->mName.C_Str();
	}

	return (i + " not found!");
}

std::string Mesh3d::getMeshName()
{
	return this->fileName;
}


} } }