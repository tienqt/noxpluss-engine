/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/app/graphics/Mesh3d.h>
#include <nox/app/graphics/PointLight3d.h>

#include <nox/logic/graphics/actor/ActorLight3d.h>

#include <nox/app/graphics/PointLight3d.h>
#include <nox/app/graphics/SpotLight3d.h>
#include <nox/app/graphics/DirectionalLight3d.h>
#include <nox/app/graphics/LightRenderNode3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>

#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/IContext.h>
#include <nox/app/graphics/TransformationNode3d.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>

#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/constants.hpp>

namespace nox { namespace logic { namespace graphics
{

const ActorLight3d::IdType ActorLight3d::NAME = "Light3d";

ActorLight3d::~ActorLight3d() = default;

bool ActorLight3d::initialize(const Json::Value& componentJsonObject)
{
	const auto& lightListJson = componentJsonObject["lights"];
	const auto lightNameList = lightListJson.getMemberNames();

	for (const auto& lightName : lightNameList)
	{
		const auto& lightJson = lightListJson[lightName];

		std::shared_ptr<nox::app::graphics::BaseLight3d> light = nullptr;
		std::shared_ptr<nox::app::graphics::LightRenderNode3d> lightNode = nullptr;

		std::string lightTypeString = lightJson.get("type", "").asString();

		Json::Value colorJson = lightJson.get("color", Json::nullValue);
		glm::vec3 color = glm::vec3();
		color.x = colorJson.get("r", 1.0f).asFloat();
		color.y = colorJson.get("g", 1.0f).asFloat();
		color.z = colorJson.get("b", 1.0f).asFloat();

		float ambientIntensity = lightJson.get("ambientIntensity", 1).asFloat();
		float diffuseIntensity = lightJson.get("diffuseIntensity", 1).asFloat();

	
		Json::Value directionJson = lightJson.get("direction", Json::nullValue);
		glm::vec3 direction = glm::vec3();
		direction.x = directionJson.get("x", 1.0f).asFloat();
		direction.y = directionJson.get("y", 1.0f).asFloat();
		direction.z = directionJson.get("z", 1.0f).asFloat();

		bool castShadows = lightJson.get("castShadows", false).asBool();
		int type = 0;
		
		if (std::strcmp(lightTypeString.c_str(), "directional") == false)
		{
			light = std::make_shared<nox::app::graphics::DirectionalLight3d>(nullptr, color, ambientIntensity, diffuseIntensity, direction);
			type = nox::app::graphics::BaseLight3d::LightType::DIRECTIONAL;
		}
		else
		{
			float constantAttenuation = lightJson.get("constantAttenuation", 1.0f).asFloat();
			float linearAttenuation = lightJson.get("linearAttenuation", 1.0f).asFloat();
			float exponentialAttenuation = lightJson.get("exponentialAttenuation", 1.0f).asFloat();
			float range = lightJson.get("range", 1).asFloat();

			if (std::strcmp(lightTypeString.c_str(), "point") == false)
			{
				light = std::make_shared<nox::app::graphics::PointLight3d>(nullptr, color, ambientIntensity, diffuseIntensity, glm::vec3(0, 0, 0), constantAttenuation, linearAttenuation, exponentialAttenuation);
				type = nox::app::graphics::BaseLight3d::LightType::POINT;

			}
			else if (std::strcmp(lightTypeString.c_str(), "spot") == false)
			{
				int cutOfAngle = lightListJson.get("cutOfAngle", 1).asFloat();
				light = std::make_shared<nox::app::graphics::SpotLight3d>(nullptr, color, ambientIntensity, diffuseIntensity, glm::vec3(0, 0, 0), constantAttenuation, linearAttenuation, exponentialAttenuation, cutOfAngle, direction);
				type = nox::app::graphics::BaseLight3d::LightType::SPOT;
			}
		}
		
		if (light != nullptr)
		{
			lightNode = std::make_shared<nox::app::graphics::LightRenderNode3d>();
			light->setType(type);
			light->setName(lightName);
			lightNode->setLight(light, type);
			std::pair<std::string, std::shared_ptr<nox::app::graphics::LightRenderNode3d>> lightNodePair(lightName, lightNode);
			lightMap.insert(lightNodePair);
		}
	}

	return true;
}

void ActorLight3d::onCreate()
{	
	const auto actorTransform = this->getOwner()->findComponent<nox::logic::actor::Transform3d>();
	const auto actorGraphics = this->getOwner()->findComponent<nox::logic::graphics::ActorGraphics3d>();

	for (const auto lightNode : lightMap)
	{
		lightNode.second->setActorTransform(actorTransform);
	}

}

const ActorLight3d::IdType& ActorLight3d::getName() const
{
	return NAME;
}

void ActorLight3d::serialize(Json::Value& componentObject)
{
}

void ActorLight3d::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(nox::logic::actor::TransformChange3d::ID))
	{
		for (auto actorLightNode : lightMap)
		{
			actorLightNode.second->updatePosition();
		}
	}
}

void ActorLight3d::disableLights()
{
	for (const auto lightNode : lightMap)
	{
		broadcastLightRemoval(lightNode.second);
	}
}

void ActorLight3d::disableLight(const std::string& lightName)
{
	broadcastLightRemoval(this->lightMap[lightName]);
}

void ActorLight3d::enableLights()
{
	for (const auto lightNode : lightMap)
	{
		broadcastLightCreation(lightNode.second);
	}
}

void ActorLight3d::enableLight(const std::string &lightName)
{
	broadcastLightCreation(this->lightMap[lightName]);
}

void ActorLight3d::onActivate()
{
	this->enableLights();
}

std::shared_ptr<nox::app::graphics::BaseLight3d> ActorLight3d::getLight(std::string lightName)
{
	return this->lightMap[lightName]->getLight();
}

void ActorLight3d::onDeactivate()
{
	this->disableLights();
}

void ActorLight3d::broadcastLightCreation(const std::shared_ptr<app::graphics::LightRenderNode3d>& lightNode)
{
	auto sceneNodeAddEvent = std::make_shared<SceneNodeEdited>(lightNode, SceneNodeEdited::Action::CREATE, this->getOwner()->getId());
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeAddEvent);
}

void ActorLight3d::broadcastLightRemoval(const std::shared_ptr<app::graphics::LightRenderNode3d>& lightNode)
{
	auto sceneNodeRemoveEvent = std::make_shared<SceneNodeEdited>( lightNode, SceneNodeEdited::Action::REMOVE, this->getOwner()->getId());
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeRemoveEvent);
}



} } }
