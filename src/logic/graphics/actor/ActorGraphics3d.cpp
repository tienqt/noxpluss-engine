
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/graphics/event/ActorGraphicsCreated3d.h>
#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/graphics/actor/Animation3d.h>
#include <nox/logic/graphics/event/AnimationChanged3d.h>
#include <nox/app/graphics/TransformationNode3d.h>
#include <nox/logic/graphics/actor/ActorLight3d.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/app/graphics/LightRenderNode3d.h>
#include <nox/util/chrono_utils.h>
#include <nox/app/graphics/RenderNode3d.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>

namespace nox { namespace logic { namespace graphics
{

	const ActorGraphics3d::IdType ActorGraphics3d::NAME = "Graphics3d";


ActorGraphics3d::ActorGraphics3d() : 
listener(this->getName()),
previousAnimation(0),
previousAnimationSpeed(0),
previousAnimationTime(0),
currentAnimationIndex(0),
currentAnimationSpeed(0),
currentAnimationTime(0),
overLayColor(glm::vec3(0.0, 0.0, 0.0))
{
}

ActorGraphics3d::~ActorGraphics3d() = default;

const ActorGraphics3d::IdType& ActorGraphics3d::getName() const
{
	return NAME;
}

bool ActorGraphics3d::initialize(const Json::Value& componentJsonObject)
{
	listener.setup(this, this->getLogicContext()->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());
	listener.startListening();
	listener.addEventTypeToListenFor(nox::logic::world::timemanipulation::PlayModeChange::ID);



	//this->animation = new nox::logic::graphics::Animation3d(this);

	Json::Value dataPathValue = componentJsonObject.get("dataPath", Json::nullValue);
	Json::Value nameValue = componentJsonObject.get("name", Json::nullValue);
	Json::Value animationValue = componentJsonObject.get("startAnimation", Json::nullValue);
	
	this->currentAnimationSpeed = componentJsonObject.get("animationSpeed", 1.0f).asFloat();	// negative to play backwards
	this->currentAnimationTime = componentJsonObject.get("animationStartTime", 0.0f).asFloat();
	
	if (dataPathValue == Json::nullValue || nameValue == Json::nullValue)
	{
		return false;
	}	

	this->dataPath = dataPathValue.asString();
	this->name = nameValue.asString();

	if (animationValue.isString())
	{
		this->animationName = animationValue.asString().c_str();
		printf("%s: animation \"%s\" set. Speed: %fx. Start time: %f sec\n", this->name.c_str(), animationValue.asString().c_str(), currentAnimationSpeed);
		broadcastAnimationChanged();
	}
	else if (animationValue.isInt())
	{
		this->currentAnimationIndex = animationValue.asInt();
		printf("%s: animation number %i set. Speed: %fx. Start time: %f sec\n", this->name.c_str(), animationValue.asInt(), currentAnimationSpeed);
		broadcastAnimationChanged();
	}
	else
	{
		this->currentAnimationIndex = -1;
	}
	position = glm::vec3(0, 0, 0);
	rotation = glm::vec3(0);
	scale = glm::vec3(1.0);

	this->actorTransformNode = std::make_shared<app::graphics::TransformationNode3d>();
	this->renderTransformNode = std::make_shared<app::graphics::TransformationNode3d>();
	this->actorTransformNode->addChild(this->renderTransformNode);

	this->transformComponent = nullptr;
//	this->broadcastAiSceneCreation();
	this->getLogicContext()->getPhysics3d()->getAssetManager()->loadAssetFromFile(this->dataPath, this->name);
	setMeshData(this->getLogicContext()->getPhysics3d()->getAssetManager()->getObjectMesh(this->name));


	return true;
}

void ActorGraphics3d::onCreate()
{
	//auto actorPhysics = this->getOwner()->findComponent<nox::logic::physics::ActorPhysics3d>();
	//if (actorPhysics != nullptr)
	//{
	//	//setMeshData(this->getLogicContext()->getPhysics3d()->getAssetManager()->getObjectMesh(this->name));
	//	//actorPhysics->createActorPhysicsShape();
	//}

	if (this->getOwner()->getId().getValue() > 3)
		overLayColor = glm::vec3(0.0f, 0.5f, 0.0f);

	this->transformComponent = this->getOwner()->findComponent<nox::logic::actor::Transform3d>();
	assert(this->transformComponent);

	this->actorTransformNode->setTransform(this->transformComponent->getTransformMatrix());
	this->renderTransformNode->setTransform(glm::mat4(1));

	this->rendererNode = std::make_shared<app::graphics::RendererNode3d>(this->getOwner());
	assert(rendererNode);

	this->renderTransformNode->addChild(rendererNode);
	this->updateRenderTransform();

	this->broadcastSceneNodeCreation();
}

void ActorGraphics3d::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{

	if (event->isType(nox::logic::world::timemanipulation::PlayModeChange::ID))
	{
		auto playMode = static_cast<nox::logic::world::timemanipulation::PlayModeChange*>(event.get());
		this->setAnimationSpeed(this->getAnimationSpeed() * -1);

		if (playMode->isPlay() == false)
		{
			if (this->getOwner()->getId().getValue() > 3)
				overLayColor = glm::vec3(0.5f, 0.0f, 0.0f);
		}
	}
}

void ActorGraphics3d::onDestroy()
{
}

void ActorGraphics3d::serialize(Json::Value &componentObject)
{
}

void ActorGraphics3d::onDeactivate()
{

}

void ActorGraphics3d::onActivate()
{

}

void ActorGraphics3d::onUpdate(const Duration& deltaTime)
{
	if (this->rendererNode != nullptr)
	{
		this->rendererNode->updateAnimation(deltaTime);
	}
}

void ActorGraphics3d::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (event->isType(actor::TransformChange3d::ID))
	{
		this->updateActorTransform();
	}
}

void ActorGraphics3d::updateRenderTransform()
{
	glm::mat4 positionMatrix = glm::translate(this->position);
	glm::mat4 scaleMatrix = glm::scale(this->scale);

	//glm::mat4 rotationMatrix = glm::mat4_cast(qRotation);

		glm::mat4 rotationMatrix = glm::rotate(this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::rotate(this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
			glm::rotate(this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

	this->renderTransformNode->setTransform( positionMatrix * rotationMatrix * scaleMatrix);
}

void ActorGraphics3d::updateActorTransform()
{
	this->actorTransformNode->setTransform(this->transformComponent->getTransformMatrix());
}

void ActorGraphics3d::broadcastSceneNodeCreation()
{
	auto sceneNodeCreated = std::make_shared<SceneNodeEdited>(actorTransformNode, SceneNodeEdited::Action::CREATE, this->getOwner()->getId());
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeCreated);
}

void ActorGraphics3d::broadcastSceneNodeRemoval()
{
	auto sceneNodeCreated = std::make_shared<SceneNodeEdited>(actorTransformNode, SceneNodeEdited::Action::REMOVE, this->getOwner()->getId());
	this->getLogicContext()->getEventBroadcaster()->queueEvent(sceneNodeCreated);
}

std::shared_ptr<app::graphics::TransformationNode3d> ActorGraphics3d::getActorTransformNode()
{
	return this->actorTransformNode;
}

void ActorGraphics3d::setMeshData(std::shared_ptr<nox::app::graphics::Mesh3d> meshData)
{
	this->meshdatas = meshData;
}

std::string ActorGraphics3d::getModelName()
{
	return this->name;
}

std::shared_ptr<nox::app::graphics::Mesh3d> ActorGraphics3d::getMeshdatas()
{
	return this->meshdatas;
}


void ActorGraphics3d::broadcastAiSceneCreation()
{
	const auto actorGraphicsCreatedEvent = std::make_shared<nox::logic::actor::ActorGraphicsCreated3d>(this->getOwner(), this, this->dataPath, this->name);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(actorGraphicsCreatedEvent);
}

std::shared_ptr<app::graphics::RendererNode3d> ActorGraphics3d::getRendererNode()
{
	return rendererNode;
}

int ActorGraphics3d::getAnimationIndex()
{
	return this->currentAnimationIndex;
}

int ActorGraphics3d::getPreviousAnimationIndex()
{
	return this->previousAnimation;
}

float ActorGraphics3d::getPreviousAnimationSpeed()
{
	return this->previousAnimationSpeed;
}

float ActorGraphics3d::getPreviousAnimationTime()
{
	return this->previousAnimationTime;
}

std::string ActorGraphics3d::getAnimationName()
{
	return this->animationName;
}

bool ActorGraphics3d::setAnimation(std::string name, bool logIt, float speed, float startTime)
{
	
	int ai = this->meshdatas->getAnimationIndexByName(name);
	if (logIt)
	{
		broadcastAnimationChanged();
	}

	this->previousAnimation = this->currentAnimationIndex;
	this->previousAnimationTime = this->currentAnimationTime;
	this->previousAnimationSpeed = this->currentAnimationSpeed;

	this->currentAnimationIndex = ai;
	this->currentAnimationTime = startTime;
	this->currentAnimationSpeed = speed;

	if (ai >= 0)
	{

		printf("%s: Animation \"%s\" started\n", this->name.c_str(), name.c_str());
		return true;
	}
	printf("%s: Animation with name \"%s\" not found\n", this->name.c_str(), name.c_str());
	return false;
	
}

bool ActorGraphics3d::setAnimation(int index, bool logIt, float speed, float startTime)
{
	std::string an = this->meshdatas->getAnimationNameByIndex(index);

	if (logIt)
	{
		broadcastAnimationChanged();
	}

	this->previousAnimation = this->currentAnimationIndex;
	this->previousAnimationTime = this->currentAnimationTime;
	this->previousAnimationSpeed = this->currentAnimationSpeed;

	this->currentAnimationIndex = index;
	this->currentAnimationTime = startTime;
	this->currentAnimationSpeed = speed;

	if (index < this->meshdatas->numAnimations())
	{
		return true;
	}
	return false;
}

void ActorGraphics3d::broadcastAnimationChanged()
{
	const auto animChange = std::make_shared<nox::logic::graphics::AnimationChanged3d>(this);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(animChange);
}

float ActorGraphics3d::getAnimationSpeed()
{
	return this->currentAnimationSpeed;
}

void ActorGraphics3d::setAnimationSpeed(float speed)
{
	this->currentAnimationSpeed = speed;
}

float ActorGraphics3d::getAnimationTime()
{
	return this->currentAnimationTime;
}

void ActorGraphics3d::stepAnimation(const nox::Duration& deltaTime)
{
	this->currentAnimationTime += (util::durationToSeconds<float>(deltaTime) * this->currentAnimationSpeed);
}

} } }
