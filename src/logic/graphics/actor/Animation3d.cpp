

#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/graphics/actor/Animation3d.h>

namespace nox { namespace logic { namespace graphics 
{

Animation3d::Animation3d(nox::logic::graphics::ActorGraphics3d * owner) : 
animationIndex(-1),
animationName(""),
animationSpeed(0),
animationStartTime(0)
{
	this->owner = owner;
}

int Animation3d::getAnimationIndex()
{
	return this->animationIndex;
}
		
std::string Animation3d::getAnimationName()
{
	return this->animationName;
}
		
//bool Animation3d::setAnimation(std::string name, float speed, float startTime)
//{
//	int ai = this->owner->getMeshdatas()->getAnimationIndexByName(name);
//		
//	if (ai >= 0)
//	{
//		this->animationIndex = ai;
//		this->animationStartTime = startTime;
//		this->animationSpeed = speed;
//				
//		printf("%s: Animation \"%s\" started\n",  this->owner->getName().c_str(), name.c_str());
//		return true;
//	}
//	else
//	{
//		printf("%s: Animation with name \"%s\" not found\n", this->owner->getName().c_str(), name.c_str());
//		return false;
//	}
//}

//void Animation3d::init(int index, float speed, float startTime)
//{
//	this->animationIndex = index;
//	this->animationSpeed = speed;
//	this->animationStartTime = startTime;
//}
		
bool Animation3d::setAnimation(int index, float speed, float startTime)
{		
	if (index < this->owner->getMeshdatas()->numAnimations())
	{
		std::string an = this->owner->getMeshdatas()->getAnimationNameByIndex(index);
		this->animationIndex = index;
		this->animationStartTime = startTime;
		this->animationSpeed = speed;
		
		printf("%s: Animation %i (\"%s\") started\n", this->owner->getName().c_str(), index, an.c_str());
		return true;
	}
	else
	{
		printf("%s: Animation with index %i not found\n", this->owner->getName().c_str(), index);
		return false;
	}
}
		
float Animation3d::getAnimationStartTime()
{
	return this->animationStartTime;
}
		
float Animation3d::getAnimationSpeed()
{
	return this->animationSpeed;
}
		
void Animation3d::setAnimationSpeed(float speed)
{
	this->animationSpeed = speed;
}


} } }