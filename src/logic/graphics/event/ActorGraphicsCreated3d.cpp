/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <nox/logic/graphics/event/ActorGraphicsCreated3d.h>

namespace nox { namespace logic { namespace actor
{

	const event::Event::IdType ActorGraphicsCreated3d::ID = "nox.logic.actor.trd_actor_graphics_created";

	ActorGraphicsCreated3d::ActorGraphicsCreated3d(Actor* actorgraphicsCreated, nox::logic::graphics::ActorGraphics3d* ag, std::string path, std::string name) :
	Event(ID, actorgraphicsCreated),
	path(path),
	name(name),
	actorGraphics(ag)
{
}

ActorGraphicsCreated3d::~ActorGraphicsCreated3d() = default;

const std::string ActorGraphicsCreated3d::getPath() const
{
	return this->path;
}

const std::string ActorGraphicsCreated3d::getName() const
{
	return this->name;
}

const nox::logic::graphics::ActorGraphics3d* ActorGraphicsCreated3d::getActorGraphics()
{
	return this->actorGraphics;
}

}
}
}
