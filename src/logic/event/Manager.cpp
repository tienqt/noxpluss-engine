/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/event/IListener.h>
#include <nox/logic/event/Manager.h>
#include <nox/util/algorithm.h>
#include <algorithm>
#include <cassert>

namespace nox { namespace logic { namespace event
{

const Event::IdType Manager::BROADCAST_COMPLETE_EVENT = "nox.event.broadcast_complete";

Manager::Manager():
	activeQueueIndex(0)
{
}

void Manager::queueEvent(std::shared_ptr<Event> event)
{
	std::lock_guard<std::mutex> lock(this->eventQueueMutex);
	this->eventQueues[this->activeQueueIndex].push(std::move(event));
}

void Manager::queueEvent(Event::IdType eventId)
{
	this->queueEvent(std::make_shared<Event>(std::move(eventId)));
}

void Manager::triggerEvent(const std::shared_ptr<Event>& event)
{
	this->broadcastEvent(event);
}

void Manager::triggerEvent(Event::IdType eventId)
{
	this->triggerEvent(std::make_shared<Event>(std::move(eventId)));
}

void Manager::broadcastEvent(const std::shared_ptr<Event>& event)
{
	const Event::IdType eventType = event->getType();

	ListenerList& listenerList = this->eventListeners[eventType];

	std::lock_guard<std::recursive_mutex> listLock(listenerList.listMutex);

	listenerList.occupied = true;

	for (ListenerHandle& listenerHandle : listenerList.listeners)
	{
		if (listenerHandle.isValid())
		{
			listenerHandle.broadcastEvent(event);
		}
	}

	listenerList.occupied = false;

	while (listenerList.additionQueue.empty() == false)
	{
		listenerList.listeners.push_back(ListenerHandle(listenerList.additionQueue.front()));
		listenerList.additionQueue.pop();
	}

	if (listenerList.listenerRemoved == true)
	{
		auto removalPredicate =
			[](const ListenerHandle& listenerData)
			{
				return listenerData.isValid() == false;
			};

		util::removeFastIf(listenerList.listeners, removalPredicate);

		listenerList.listenerRemoved = false;
	}
}

void Manager::broadcastEvents()
{
	std::unique_lock<std::mutex> queueLock(this->eventQueueMutex);

	const unsigned int broadcastQueueIndex = this->activeQueueIndex;
	this->activeQueueIndex = (this->activeQueueIndex + 1) % NUM_QUEUES;

	EventQueue& broadcastQueue = this->eventQueues[broadcastQueueIndex];

	queueLock.unlock();

	while (broadcastQueue.empty() == false)
	{
		const std::shared_ptr<Event> event = broadcastQueue.front();
		broadcastQueue.pop();
		auto t = event.get();
		this->broadcastEvent(event);
	}
	
	this->triggerEvent(std::make_shared<Event>(BROADCAST_COMPLETE_EVENT));
}

void Manager::addListener(IListener* listener, const Event::IdType& type, const std::string& name)
{
	assert(listener != nullptr);

	ListenerList& listenerList = this->eventListeners[type];

	std::unique_lock<std::recursive_mutex> listLock(listenerList.listMutex);

	auto listenerIt = std::find_if(
		listenerList.listeners.begin(),
		listenerList.listeners.end(),
		[listener](const ListenerHandle& listenerHandle)
		{
			return listenerHandle.handlesListener(listener);
		}
	);

	if (listenerIt == listenerList.listeners.end())
	{
		if (listenerList.occupied == true)
		{
			listenerList.additionQueue.push(ListenerData(listener, name));
		}
		else
		{
			listenerList.listeners.push_back(ListenerHandle(listener, name));
		}
	}
}

void Manager::removeListener(IListener* listener, const Event::IdType& type)
{
	assert(listener != nullptr);

	ListenerList& listenerList = this->eventListeners[type];

	std::unique_lock<std::recursive_mutex> listLock(listenerList.listMutex);

	const auto listenerHandleCompare =
		[listener](const ListenerHandle& listenerHandle)
		{
			return listenerHandle.handlesListener(listener);
		};


	if (listenerList.occupied == true)
	{
		auto removeIt = std::find_if(listenerList.listeners.begin(), listenerList.listeners.end(), listenerHandleCompare);

		if (removeIt != listenerList.listeners.end())
		{
			removeIt->invalidate();
			listenerList.listenerRemoved = true;
		}
	}
	else
	{
		util::removeFastIf(listenerList.listeners, listenerHandleCompare);
	}
}

void Manager::clearEventQueue()
{
	std::lock_guard<std::mutex> queueLock(this->eventQueueMutex);

	for (EventQueue& queue : this->eventQueues)
	{
		while (queue.empty() == false)
		{
			queue.pop();
		}
	}
}

bool Manager::hasQueuedEvents() const
{
	std::lock_guard<std::mutex> queueLock(this->eventQueueMutex);

	const auto queuedEventsPredicate =
		[](const EventQueue& queue)
		{
			return queue.empty() == false;
		};

	return std::any_of(this->eventQueues.begin(), this->eventQueues.end(), queuedEventsPredicate);
}

Manager::ListenerHandle::ListenerHandle(IListener* listener, const std::string& name):
	listener(listener),
	name(name)
{}

Manager::ListenerHandle::ListenerHandle(const ListenerData& data):
	listener(data.listener),
	name(data.name)
{
}

void Manager::ListenerHandle::broadcastEvent(const std::shared_ptr<Event>& event)
{
	this->listener->onEvent(event);
}

void Manager::ListenerHandle::invalidate()
{
	this->listener = nullptr;
}

bool Manager::ListenerHandle::handlesListener(const IListener* listener) const
{
	return this->listener == listener;
}

bool Manager::ListenerHandle::isValid() const
{
	return this->listener != nullptr;
}

}
}
}
