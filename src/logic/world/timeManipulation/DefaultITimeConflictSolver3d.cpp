/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/timeManipulation/DefaultITimeConflictSolver3d.h>

namespace nox { namespace logic { namespace world { namespace timemanipulation {

DefaultTimeConflictSolver3d::DefaultTimeConflictSolver3d()
{
}

DefaultTimeConflictSolver3d::~DefaultTimeConflictSolver3d()
{

}

void DefaultTimeConflictSolver3d::setOnReplayStarted(std::function<void(Manager*)> onStartFunction)
{
	this->onReplayStart = onStartFunction;
}

void DefaultTimeConflictSolver3d::setOnReplayStopped(std::function<void(Manager*)> onStopFunction)
{
	this->onReplayStop = onStopFunction;
}
void DefaultTimeConflictSolver3d::setOnReplayFinished(std::function<void(Manager*)> onFinishedFunction)
{
	this->onReplayFinished = onFinishedFunction;
}

std::function<void(Manager*)> DefaultTimeConflictSolver3d::getOnReplayStarted()
{
	return this->onReplayStart;
}
std::function<void(Manager*)> DefaultTimeConflictSolver3d::getOnReplayStopped()
{
	return this->onReplayStop;
}
std::function<void(Manager*)> DefaultTimeConflictSolver3d::getOnReplayFinished()
{
	return this->onReplayFinished;
}

std::function<void(const ConflictParameters&)> DefaultTimeConflictSolver3d::getConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType)
{
	switch (conflictType)
	{
	case ConflictType::ACTOR_CREATED:
		return actorCreatedSolvers[actorId];
	case ConflictType::ACTOR_DELETED:
		return actorDeletedSolvers[actorId];
	case ConflictType::ACTOR_INTERRUPTED:
		return actorInterruptedSolvers[actorId];
	case ConflictType::POSITION_OVERLAP:
		return positionOverlapSolvers[actorId];
	}
}

void DefaultTimeConflictSolver3d::setConflictSolverFunction(nox::logic::actor::Identifier actorId, ConflictType conflictType, std::function<void(const ConflictParameters&)> conflictSolver)
{
	switch (conflictType)
	{
	case ConflictType::ACTOR_CREATED:
		actorCreatedSolvers[actorId] = conflictSolver;
		break;

	case ConflictType::ACTOR_DELETED:
		actorDeletedSolvers[actorId] = conflictSolver;
		break;

	case ConflictType::ACTOR_INTERRUPTED:
		actorInterruptedSolvers[actorId] = conflictSolver;
		break;

	case ConflictType::POSITION_OVERLAP:
		positionOverlapSolvers[actorId] = conflictSolver;
		break;
	}
}

} } } }
