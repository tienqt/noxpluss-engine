/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/timeManipulation/TimeManipulationComponent.h>

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/control/event/Action.h>

namespace nox { namespace logic { namespace world { namespace timemanipulation
{
	const TimeManipulationComponent::IdType TimeManipulationComponent::NAME = "TimeManipulation";


TimeManipulationComponent::TimeManipulationComponent()
{
}

TimeManipulationComponent::~TimeManipulationComponent()
{

}


bool TimeManipulationComponent::initialize(const Json::Value& componentJsonObject)
{
	Json::Value ignoreOthersValue = componentJsonObject.get("ignoreOthers", false);
	Json::Value affectOthersValue = componentJsonObject.get("affectOthers", true);
	Json::Value enableLoggingValue = componentJsonObject.get("enableLogging", true);

	this->affectOthers = affectOthersValue.asBool();
	this->ignoreOthers = ignoreOthersValue.asBool();
	this->enableLogging = enableLoggingValue.asBool();

	return true;
}

const nox::logic::actor::Component::IdType& TimeManipulationComponent::getName() const
{
	return NAME;
}

void TimeManipulationComponent::onCreate()
{
	
}

void TimeManipulationComponent::onActivate()
{

}

void TimeManipulationComponent::onDeactivate()
{

}

void TimeManipulationComponent::onComponentEvent(const std::shared_ptr<event::Event>& event)
{

}

void TimeManipulationComponent::serialize(Json::Value& componentObject)
{

}

bool TimeManipulationComponent::getLoggingEnabled()
{
	return this->enableLogging;
}

bool TimeManipulationComponent::getIgnoreOthers()
{
	return this->ignoreOthers;
}

bool TimeManipulationComponent::getAffectOthers()
{
	return this->affectOthers;
}

void TimeManipulationComponent::setLoggingEnabled(bool flag)
{
	this->enableLogging = flag;
}

void TimeManipulationComponent::setIgnoreOthers(bool flag)
{
	this->ignoreOthers = flag;
}

void TimeManipulationComponent::setAffectOthers(bool flag)
{
	this->affectOthers = flag;
}


} } } }
