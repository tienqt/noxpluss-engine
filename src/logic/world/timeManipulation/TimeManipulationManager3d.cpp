/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <nox/logic/world/timeManipulation/TimeManipulationManager3d.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/world/timeManipulation/ITimeConflictSolver3d.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/app/IContext.h>
#include <nox/logic/Logic.h>
#include <memory>

namespace nox { namespace logic { namespace world { namespace timemanipulation {

	TimeManipulationManager3d::TimeManipulationManager3d(std::unique_ptr<IWorldLogger3d> worldLogger, std::unique_ptr<ITimeConflictSolver3d> conflictSolver) :
	paused(true),
	rewindEnabled(false),
	replayEnabled(false),
	timeManipulationLock(false),
	replayStartedLock(false),
	replayStoppedLock(false)
{
	this->flags = static_cast<Options>(0x0);
	this->worldLogger = std::move(worldLogger);
	this->conflictSolver = std::move(conflictSolver);
	this->worldLogger->setTimeConflictSolver(this->conflictSolver.get());
	this->logic = this->worldLogger->getLogic();
	//this->enable(Options::AFFECT_FUTURE | Options::AFFECT_PAST);
}

TimeManipulationManager3d::~TimeManipulationManager3d()
{

}

// TODO: replace loop with event broadcast
void TimeManipulationManager3d::rewind()
{
	//if (this->flags & Options::REWIND)
	{
		if (timeManipulationLock == false)
		{
			//auto allActors = this->worldLogger->getWorldManager()->getAllActors();

			//for (int i = 0; i < allActors.size(); i++)
			//{
			//	auto actorGraphics = allActors[i]->findComponent<nox::logic::graphics::ActorGraphics3d>();
			//	if (actorGraphics != nullptr)
			//	{
			//		actorGraphics->setAnimationSpeed(actorGraphics->getAnimationSpeed() * -1);
			//	}
			//}

			auto playModeEvent = std::make_shared<nox::logic::world::timemanipulation::PlayModeChange>(false);
			this->logic->getEventBroadcaster()->queueEvent(playModeEvent);

			this->setRewindEnabled(true);

			timeManipulationLock = true;
		}
	}
}

void TimeManipulationManager3d::setRewindEnabled(bool enabled)
{
	this->rewindEnabled = enabled;
	this->worldLogger->setRewindEnabled(enabled);

	// If just disabled:
	if (enabled == true && this->replayEnabled == true && this->replayStoppedLock == false)
	{
		auto onPbStopped = this->conflictSolver->getOnReplayStopped();

		if (onPbStopped != nullptr)
		{
			onPbStopped(this->worldLogger->getWorldManager());
		}

		replayStoppedLock = true;
		replayStartedLock = false;
	}
}

void TimeManipulationManager3d::setReplayEnabled(bool enabled)
{
	this->replayEnabled = enabled;
	this->worldLogger->setReplayEnabled(enabled);

	// On enabled:
	if (enabled == true && replayStartedLock == false)		
	{
		auto onPbEnabled = this->conflictSolver->getOnReplayStarted();

		if (onPbEnabled != nullptr)
		{
			onPbEnabled(this->worldLogger->getWorldManager());
		}
		
		replayStartedLock = true;
		replayStoppedLock = false;
	}

	// If end of saved frames reached (replay finished):
	else if (enabled == false && replayStartedLock == true && this->getWorldLogger()->endOfSavedFrames())
	{
		auto onPbFinished = this->conflictSolver->getOnReplayFinished();

		if (onPbFinished != nullptr)
		{
			onPbFinished(this->worldLogger->getWorldManager());
		}
		
		replayStartedLock = false;
		replayStoppedLock = false;
	}



}

void TimeManipulationManager3d::onUpdate()
{
	if (this->paused == false)
	{
		if (this->rewindEnabled)
		{
			this->getWorldLogger()->playFrame(IWorldLogger3d::PlayMode::REWIND);
		}
		else /*Forward*/
		{
			if (this->getWorldLogger()->endOfSavedFrames() /* || (this->flags & Options::FAST_FORWARD) == false*/)
			{
				this->setReplayEnabled(false);
			}
			else
			{

				this->setReplayEnabled(true);
				this->getWorldLogger()->playFrame(IWorldLogger3d::PlayMode::FORWARD);

		
			}
		}

	}
}

void TimeManipulationManager3d::play()
{
	if (timeManipulationLock)
	{
		//if (this->rewindEnabled)
		//{
		//	auto allActors = this->worldLogger->getWorldManager()->getAllActors();
		//	for (int i = 0; i < allActors.size(); i++)
		//	{
		//		auto actorGraphics = allActors[i]->findComponent<nox::logic::graphics::ActorGraphics3d>();
		//		if (actorGraphics != nullptr)
		//		{
		//			actorGraphics->setAnimationSpeed(actorGraphics->getAnimationSpeed() * -1);
		//		}
		//	}
		//}

		auto playModeEvent = std::make_shared<nox::logic::world::timemanipulation::PlayModeChange>(true);
		this->logic->getEventBroadcaster()->queueEvent(playModeEvent);

		this->paused = false;
		this->setRewindEnabled(false);
		this->timeManipulationLock = false;
	}
}

void TimeManipulationManager3d::setPause(bool pause)
{
	this->paused = pause;
}

bool TimeManipulationManager3d::isGamePaused()
{
	return this->paused;
}

void TimeManipulationManager3d::setConflictSolver(std::unique_ptr<ITimeConflictSolver3d> conflictSolver)
{
	this->conflictSolver = std::move(conflictSolver);
}

void TimeManipulationManager3d::setWorldLogger(std::unique_ptr<IWorldLogger3d> worldLogger)
{
	this->worldLogger = std::move(worldLogger);
}

IWorldLogger3d * TimeManipulationManager3d::getWorldLogger()
{
	return this->worldLogger.get();
}

ITimeConflictSolver3d * TimeManipulationManager3d::getConflictSolver()
{
	return this->conflictSolver.get();
}

void TimeManipulationManager3d::enable(Options optns)
{
	//unsigned int opt = 0x0;
	this->flags = flags | optns;
	if (optns & Options::REWIND)
	{
		printf("REWIND!\n");
	}

	if (optns & Options::AFFECT_FUTURE)
	{
		printf("AFFECT_FUTURE!\n");
	}

	if (optns & Options::AFFECT_PAST)
	{
		printf("AFFECT_PAST!\n");
	}

	if (optns & Options::FAST_FORWARD)
	{
		printf("FAST_FORWARD!\n");
	}

	if (optns & Options::EVERYTHING)
	{
		printf("EVERYTHING!\n");
	}

	//if ((opt & (OPT_B | OPT_C)) == (OPT_B | OPT_C))
	//{
	//	printf("Flags B and C were set.\n");
	//}

}

void TimeManipulationManager3d::disable(Options optns)
{

}

bool TimeManipulationManager3d::isRewindEnabled()
{
	return rewindEnabled;
}

bool TimeManipulationManager3d::isReplayEnabled()
{
	return replayEnabled;
}


} } } }
