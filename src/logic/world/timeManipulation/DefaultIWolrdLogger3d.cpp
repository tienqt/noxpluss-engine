/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/world/timeManipulation/DefaultIWolrdLogger3d.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/graphics/event/AnimationChanged3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <vector>
#include <nox/logic/Logic.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/world/timeManipulation/TimeLoggingComponent.h>
#include <nox/logic/world/timeManipulation/ITimeConflictSolver3d.h>

namespace nox { namespace logic { namespace world { namespace timemanipulation {


DefaultWorldLogger3d::DefaultWorldLogger3d(nox::logic::Logic* logic, ITimeConflictSolver3d* conflictSolver) :
world(logic->getWorldManager()),
listener("DefaultLogger"),
replayEnabled(false),
rewindEnabled(false),
logic(logic),
frameCounter(0)
{

	this->listener.setup(this, logic->getEventBroadcaster(), nox::logic::event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(nox::logic::graphics::AnimationChanged3d::ID);
	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);
	this->listener.startListening();
	

	this->bufferWorldState = new WorldState();
	this->frameIterator = this->savedFrames.end();

	this->conflictSolver = conflictSolver;

	this->bufferWorldState->frameNumber = this->frameCounter;
}

DefaultWorldLogger3d::~DefaultWorldLogger3d()
{

}

void DefaultWorldLogger3d::logActorState(nox::logic::actor::Transform3d* actorTransform, nox::logic::physics::ActorPhysics3d* actorPhysics)
{
	//// TODO swap to disk 
	if (this->rewindEnabled == false)
	{
		ActorPhysicsState newActorState;
		actorPhysics->serializePhysicsBody(&newActorState);
		newActorState.position = actorTransform->getPosition();
		newActorState.rotation = actorTransform->getQuaternion();
		newActorState.scale = actorTransform->getScale();
		newActorState.actorPhysics = actorPhysics;
		newActorState.actorTransform = actorTransform;		
		
		this->bufferWorldState->actorPhysicsChanges.push_back(newActorState);

	}
}

bool runOnce = true;

void DefaultWorldLogger3d::setEndOfCurrentFrame()
{
	if (runOnce)
	{
		runOnce = false;

		//bufferWorldState->
	}

	if (this->rewindEnabled == false && this->frameIterator == this->savedFrames.end())
	{
		savedFrames.push_back(this->bufferWorldState);

		// Create a new one to be filled:
		this->bufferWorldState = new WorldState();
		this->frameIterator = this->savedFrames.end();

		this->bufferWorldState->frameNumber = this->frameCounter;
		this->frameCounter++;
	}
}
int top = 0;
void DefaultWorldLogger3d::playFrame(int playMode)
{

	(playMode == PlayMode::FORWARD) ? (this->frameIterator++) : (this->frameIterator--);

 	if (this->frameIterator != this->savedFrames.end() && this->frameIterator != this->savedFrames.begin())
	{
		if (playMode == PlayMode::FORWARD)
		{
			this->bufferWorldState = *this->frameIterator;
		}

		//************************** Physics *************************************
		std::vector<ActorPhysicsState>* actorStates = &(*this->frameIterator)->actorPhysicsChanges;
		ActorPhysicsState* tmpActorState;


		if (playMode == PlayMode::FORWARD)
		{
			for (int i = 0; i < actorStates->size(); i++)
			{
				tmpActorState = &actorStates->at(i);
	
				//std::printf("start: %i, frame: %i\n", tmpActorState->actorPhysics->startSimulatingAt, this->bufferWorldState->frameNumber);
				//if (tmpActorState->actorPhysics->startSimulatingAt <= this->bufferWorldState->frameNumber && tmpActorState->actorPhysics->startSimulatingAt != -1)
				//{
				//	//std::printf("INNE: start: %i, frame: %i\n", tmpActorState->actorPhysics->startSimulatingAt, this->bufferWorldState->frameNumber);
				//	tmpActorState->actorPhysics->setSimulatedFlag(true);
				//	tmpActorState->actorPhysics->oldStartSimulatingAt = tmpActorState->actorPhysics->startSimulatingAt;
				//	tmpActorState->actorPhysics->startSimulatingAt = -1;
				//	//tmpActorState->actorPhysics->startSimulatingAt = this->bufferWorldState->frameNumber;
				//}

				if (tmpActorState->actorPhysics->startSimulatingAt <= this->bufferWorldState->frameNumber && tmpActorState->actorPhysics->startSimulatingAt != -1 && tmpActorState->actorPhysics->conflictResolved == false)
				{
					tmpActorState->actorPhysics->setSimulatedFlag(true);
					tmpActorState->actorPhysics->startSimulatingAt = this->bufferWorldState->frameNumber;

					if (tmpActorState->actorPhysics->conflictResolved == false)
					{
						auto function = conflictSolver->getConflictSolverFunction(tmpActorState->actorPhysics->getOwner()->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED);
						if (function != nullptr)
						{
							nox::logic::world::timemanipulation::ConflictParameters cp;
							cp.conflictType = nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED;
							cp.conflictingActor = tmpActorState->actorPhysics->getOwner();
							cp.affectedActor = tmpActorState->actorPhysics->conflictedActor;
							function(cp);
							tmpActorState->actorPhysics->conflictResolved = true;
						}
					}

				}

				if (tmpActorState->actorPhysics->isSimulated() == false)
				{
					//std::printf("INNI %i\n", top++);
					tmpActorState->actorPhysics->setTransform(tmpActorState->position, tmpActorState->rotation, tmpActorState->scale);
					tmpActorState->actorPhysics->setLinearVelocity(tmpActorState->physicalProperties.linearVelocity);
					tmpActorState->actorPhysics->setAngularVelocity(tmpActorState->physicalProperties.angularVelocity);
				}
			}
		}
		else if (playMode == PlayMode::REWIND)
		{
			for (int i = 0; i < actorStates->size(); i++)
			{
				tmpActorState = &actorStates->at(i);

				tmpActorState->actorPhysics->setLinearVelocity(tmpActorState->physicalProperties.linearVelocity * 1.0f);
				tmpActorState->actorPhysics->setAngularVelocity(tmpActorState->physicalProperties.angularVelocity * 1.0f);
				tmpActorState->actorPhysics->setTransform(tmpActorState->position, tmpActorState->rotation, tmpActorState->scale);
				
				tmpActorState->actorPhysics->setSimulatedFlag(false);

				tmpActorState->actorPhysics->collisionUpdated = false;
				
			}

			runOnce = true;
		}

		//********************************* Animation ******************************
		nox::logic::world::timemanipulation::ActorAnimationState* animState = nullptr;
		std::vector<ActorAnimationState>* animationStates = &(*this->frameIterator)->actorAnimationChanges;

		if (playMode == PlayMode::REWIND)
		{
			for (int i = 0; i < animationStates->size(); i++)
			{
				animState = &animationStates->at(i);
				animState->actorGraphics->setAnimation(animState->previousAnimationId, true, animState->previousAnimationSpeed*-1, animState->previousAnimationTime);
			}
		}
		else if (playMode == PlayMode::FORWARD)
		{
			for (int i = 0; i < animationStates->size(); i++)
			{
				animState = &animationStates->at(i);
				animState->actorGraphics->setAnimation(animState->currentAnimationId, true, animState->currentAnimationSpeed, animState->currentAnimationTime);
			}
		}

		//******************************** New actors **********************************
		std::vector<ActorAddedOrRemoved>* addedOrRemovedActors = &(*this->frameIterator)->actorsAddedOrRemoved;
		std::shared_ptr<nox::logic::graphics::SceneNodeEdited> sceneNodeEdited;
		

		if (playMode == PlayMode::FORWARD)
		{
			for (int i = 0; i < addedOrRemovedActors->size(); i++)
			{
				auto editiedActor = addedOrRemovedActors->at(i);

				if (editiedActor.added)
				{
					sceneNodeEdited = std::make_shared<nox::logic::graphics::SceneNodeEdited>(editiedActor.sceneNode, nox::logic::graphics::SceneNodeEdited::Action::TIME_ADD, editiedActor.getActorId());
				}
				else
				{
					sceneNodeEdited = std::make_shared<nox::logic::graphics::SceneNodeEdited>(editiedActor.sceneNode, nox::logic::graphics::SceneNodeEdited::Action::TIME_REMOVE, editiedActor.getActorId());
				}

				this->logic->getEventBroadcaster()->queueEvent(sceneNodeEdited);
			}
		}
		else if (playMode == PlayMode::REWIND)
		{
			for (int i = 0; i < addedOrRemovedActors->size(); i++)
			{
				auto editiedActor = addedOrRemovedActors->at(i);

				if (editiedActor.added)
				{
					sceneNodeEdited = std::make_shared<nox::logic::graphics::SceneNodeEdited>(editiedActor.sceneNode, nox::logic::graphics::SceneNodeEdited::Action::TIME_REMOVE, editiedActor.getActorId());
				}
				else
				{
					sceneNodeEdited = std::make_shared<nox::logic::graphics::SceneNodeEdited>(editiedActor.sceneNode, nox::logic::graphics::SceneNodeEdited::Action::TIME_ADD, editiedActor.getActorId());
				}

				this->logic->getEventBroadcaster()->queueEvent(sceneNodeEdited);
			}
		}

		//******************************** Custom components **********************************
		std::vector<TimeManipulationData3d>* componentStates = &(*this->frameIterator)->componentChanged;
		for (int i = 0; i < componentStates->size(); i++)
		{
			componentStates->at(i).restore();
		}


		if (playMode == FORWARD)
		{
			componentStates->clear();
			actorStates->clear();
			animationStates->clear();

			(*this->frameIterator)->actorCollisons.clear();
		}
	}
	else
	{
		if (playMode == PlayMode::REWIND)
		{
			this->frameIterator++;
			std::vector<ActorPhysicsState>* actorStates = &(*this->frameIterator)->actorPhysicsChanges;

			for (int i = 0; i < actorStates->size(); i++)
			{
				ActorPhysicsState actorChange = actorStates->at(i);
				actorStates->at(i).actorPhysics->setTransform(actorChange.position, actorChange.rotation, actorChange.scale);
			}
		}	
	}
}

void DefaultWorldLogger3d::applyPhysicsChanges(int playMode)
{

}

void DefaultWorldLogger3d::applyAnimationChanges(int playMode)
{

}

void DefaultWorldLogger3d::applyActorAddedOrRemovedChanges(int playMode)
{

}


nox::logic::world::Manager* DefaultWorldLogger3d::getWorldManager()
{
	return this->world;
}



bool DefaultWorldLogger3d::endOfSavedFrames()
{
	return (this->frameIterator == this->savedFrames.end());
}

void DefaultWorldLogger3d::serialize(nox::Duration& time)
{

}

void DefaultWorldLogger3d::deSerialize(nox::Duration& time)
{

}

void DefaultWorldLogger3d::logAnimationChanged(nox::logic::graphics::AnimationChanged3d* animeChanged)
{
	if (this->rewindEnabled == false)
	{
		auto actorGraphics = animeChanged->getActorGraphics();

		ActorAnimationState actorAnimation;
		actorAnimation.actorGraphics = actorGraphics;
		actorAnimation.currentAnimationId = actorGraphics->getAnimationIndex();
		actorAnimation.currentAnimationSpeed = actorGraphics->getAnimationSpeed();
		actorAnimation.currentAnimationTime = actorGraphics->getAnimationTime();

		actorAnimation.previousAnimationId = actorGraphics->getPreviousAnimationIndex();
		actorAnimation.previousAnimationSpeed = actorGraphics->getPreviousAnimationSpeed();
		actorAnimation.previousAnimationTime = actorGraphics->getPreviousAnimationTime();
		bufferWorldState->actorAnimationChanges.push_back(actorAnimation);

		bufferWorldState->frameNumber = this->frameCounter;
	}
}

void DefaultWorldLogger3d::setTimeConflictSolver(nox::logic::world::timemanipulation::ITimeConflictSolver3d* conflictSolver)
{
	this->conflictSolver = conflictSolver;
}

int DefaultWorldLogger3d::getNumberOfFrames()
{
	return 0;
}

int DefaultWorldLogger3d::getCurrentFrame()
{
	return this->bufferWorldState->frameNumber;
}

nox::logic::Logic* DefaultWorldLogger3d::getLogic()
{
	return this->logic;
}

WorldState* DefaultWorldLogger3d::getCurrentWorldState()
{
	return this->bufferWorldState;
}

void DefaultWorldLogger3d::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->getType() == nox::logic::graphics::AnimationChanged3d::ID)
	{
		auto animeChanged = static_cast<nox::logic::graphics::AnimationChanged3d*>(event.get());
		logAnimationChanged(animeChanged);
	}
	else if (event->getType() == nox::logic::graphics::SceneNodeEdited::ID)
	{
		
		auto sceneNodeEdited = static_cast<nox::logic::graphics::SceneNodeEdited*>(event.get());

		if (sceneNodeEdited->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_ADD || sceneNodeEdited->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_REMOVE)
			return;

		ActorAddedOrRemoved aaor;
		aaor.added = (sceneNodeEdited->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::CREATE) ? (true) : (false);
		aaor.sceneNode = sceneNodeEdited->getSceneNode();
		aaor.actorId = sceneNodeEdited->getActorId();
		bufferWorldState->actorsAddedOrRemoved.push_back(aaor);
	}
}

void DefaultWorldLogger3d::setRewindEnabled(bool enabled)
{
	this->rewindEnabled = enabled;
}
void DefaultWorldLogger3d::setReplayEnabled(bool enabled)
{
	this->replayEnabled = enabled;
}

void DefaultWorldLogger3d::logComponent(TimeLoggingComponent* component)
{
	if (this->rewindEnabled == false)
	{
		TimeManipulationData3d savedData(component);
		component->onSave(&savedData);
		bufferWorldState->componentChanged.push_back(std::move(savedData));
	}
}


} } } }
