/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/Logic.h>
#include <nox/app/IContext.h>
#include <nox/logic/View.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/Identifier.h>
#include <nox/logic/event/Manager.h>
#include <nox/logic/physics/Simulation.h>
#include <nox/logic/physics/Simulation3d.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/world/timeManipulation/TimeManipulationManager3d.h>
#include <cassert>

namespace nox { namespace logic
{

Logic::Logic():
	paused(true),
	eventManager(std::make_unique<event::Manager>())
{
}

Logic::~Logic() = default;

app::IContext* Logic::getApplicationContext() const NOX_NOEXCEPT
{
	return this->getContext();
}

void Logic::addView(std::unique_ptr<View> view)
{
	view->initialize(this);
	this->viewContainer.push_back(std::move(view));
}

void Logic::pause(bool pause)
{
	if (this->paused != pause)
	{
		this->paused = pause;

		if (this->paused == true)
		{
			this->log.verbose().raw("Paused.");
		}
		else
		{
			this->log.verbose().raw("Unpaused.");
		}
	}
}

bool Logic::isPaused() const
{
	if (this->timeManager3d != nullptr)
	{
		return this->timeManager3d->isGamePaused();
	}
	return this->paused;
}

event::IBroadcaster* Logic::getEventBroadcaster()
{
	return this->eventManager.get();
}

physics::Simulation* Logic::getPhysics()
{
	return this->physics.get();
}

physics::Simulation3d* Logic::getPhysics3d()
{
	return this->trdPhysics.get();
}

world::Manager* Logic::getWorldManager()
{
	return this->world.get();

}

world::timemanipulation::ITimeManager3d* Logic::getTimeManager()
{
	return this->timeManager3d.get();
}

app::resource::IResourceAccess* Logic::getResourceAccess()
{
	return this->getApplicationContext()->getResourceAccess();
}

app::storage::IDataStorage* Logic::getDataStorage()
{
	return this->getApplicationContext()->getDataStorage();
}

app::log::Logger Logic::createLogger()
{
	auto appContext = this->getApplicationContext();
	if (appContext != nullptr)
	{
		return appContext->createLogger();
	}
	else
	{
		return app::log::Logger();
	}
}

const View* Logic::findControllingView(const actor::Identifier& actorId) const
{
	auto viewIt = this->viewContainer.cbegin();
	const View* view = nullptr;

	while (view == nullptr && viewIt != this->viewContainer.cend())
	{
		const auto controlledActor = (*viewIt)->getControlledActor();

		if (controlledActor && controlledActor->getId() != actorId)
		{
			view = viewIt->get();
		}

		++viewIt;
	}

	return view;
}

void Logic::setPhysics(std::unique_ptr<physics::Simulation> physics)
{
	this->physics = std::move(physics);
}


void Logic::setPhysics(std::unique_ptr<physics::Simulation3d> physics)
{
	this->trdPhysics = std::move(physics);
}

void Logic::setTimeManager(std::unique_ptr<world::timemanipulation::ITimeManager3d> timeManager)
{
	this->timeManager3d = std::move(timeManager);
}


void Logic::setWorldManager(std::unique_ptr<world::Manager> worldHandler)
{
	this->world = std::move(worldHandler);
}

void Logic::destroy()
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->destroy();
	}

	this->viewContainer.clear();

	this->log.verbose().raw("Destroyed.");
}

void Logic::onInit()
{
	this->log = this->getApplicationContext()->createLogger();
	this->log.setName("NoxLogic");

	this->log.verbose().raw("Initialized.");
}

void Logic::onUpdate(const Duration& deltaTime)
{
	for (const std::unique_ptr<View>& view : this->viewContainer)
	{
		view->update(deltaTime);
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}

	if (this->timeManager3d != nullptr)
	{
		this->timeManager3d->onUpdate();
	}

	if (this->isPaused() == false)
	{
		if (this->physics != nullptr)
		{
			this->physics->onUpdate(deltaTime);
			this->physics->updateLighting();
		}
		else if (this->trdPhysics != nullptr)
		{
			if (this->timeManager3d != nullptr)
			{
				this->timeManager3d->getWorldLogger()->setEndOfCurrentFrame();
			}
			this->trdPhysics->onUpdate(deltaTime);
		}
		if (this->world != nullptr)
		{
			this->world->update(deltaTime);
		}
	}

	while (this->eventManager->hasQueuedEvents() == true)
	{
		this->eventManager->broadcastEvents();
	}
}

void Logic::onUpdateFinished(const Duration& alpha)
{
	if (this->physics != nullptr)
	{
		this->physics->onSyncState(alpha);
	}
}

void Logic::onSuccess()
{
	this->destroy();
}

void Logic::onFail()
{
	this->destroy();
}

void Logic::onAbort()
{
	this->destroy();
}

} }
