/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define GLM_FORCE_RADIANS

#include <nox/logic/physics/box2d/ActorMotionState3d.h>
#include <glm/vec3.hpp>
#include <glm/gtc/quaternion.hpp>


namespace nox { namespace logic { namespace physics
{

ActorMotionState3d::ActorMotionState3d(actor::Actor* actor, world::timemanipulation::IWorldLogger3d* worldLogger) :
	worldLogger(worldLogger)
{
	this->actorTransform = actor->findComponent<actor::Transform3d>();
	this->actorPhysics = actor->findComponent<physics::ActorPhysics3d>();

	if (this->actorTransform == nullptr || this->actorPhysics == nullptr)
	{
		this->hasRequiredComponents = false;
	}
	else
	{
		this->hasRequiredComponents = true;
	}

}

ActorMotionState3d::~ActorMotionState3d()
{
}

void ActorMotionState3d::getWorldTransform(btTransform& worldTrans) const
{
	if (this->hasRequiredComponents)
	{
		btVector3 startPosition(this->actorTransform->getPosition().x, this->actorTransform->getPosition().y, this->actorTransform->getPosition().z);
		glm::vec3 rot = this->actorTransform->getRotation();

		glm::quat rotX = glm::angleAxis(rot.x, glm::vec3(1, 0, 0));
		glm::quat rotY = glm::angleAxis(rot.y, glm::vec3(0, 1, 0));
		glm::quat rotZ = glm::angleAxis(rot.z, glm::vec3(0, 0, 1));

		glm::quat rotXYZ = rotX * rotY * rotZ;

		worldTrans.setOrigin(startPosition);
		worldTrans.setRotation(btQuaternion(rotXYZ.x, rotXYZ.y, rotXYZ.z, rotXYZ.w));
	}

}


void ActorMotionState3d::setWorldTransform(const btTransform& worldTrans)
{
	if (this->hasRequiredComponents)
	{
		glm::vec3 position(worldTrans.getOrigin().getX(), worldTrans.getOrigin().getY(), worldTrans.getOrigin().getZ());

		btQuaternion qRotation = worldTrans.getRotation();
		btVector3 btaxis = qRotation.getAxis();
		glm::vec3 axis = glm::vec3(btaxis.getX(), btaxis.getY(), btaxis.getZ());
		float angle = qRotation.getAngle();

		glm::quat rotation(glm::angleAxis(angle, axis));

		glm::normalize(rotation);
		this->actorTransform->setPosition(position);
		this->actorTransform->setRotation(rotation);
		auto test = this->actorTransform->getPosition();

		if (this->worldLogger != nullptr)
		{
			this->worldLogger->logActorState(this->actorTransform, this->actorPhysics);
		}
	}

}

} } }