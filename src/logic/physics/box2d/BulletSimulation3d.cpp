/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/box2d/ActorMotionState3d.h>
#include <nox/logic/physics/box2d/BulletSimulation3d.h>
#include <nox/logic/actor/event/ActorCollision3d.h>
#include <nox/logic/graphics/event/DebugRenderingEnabled.h>
#include <nox/logic/IContext.h>
#include <nox/logic/world/event/ActorCreated.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/world/timeManipulation/IWorldLogger3d.h>
#include <nox/logic/world/timeManipulation/ITimeManager3d.h>
#include <nox/logic/world/timeManipulation/ITimeConflictSolver3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/util/chrono_utils.h>

#include <memory>

#include <Box2D/Common/b2Settings.h>
#include <algorithm>
#include <iostream>
#include <vector>

#include <BulletCollision/CollisionShapes/btHeightfieldTerrainShape.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>
#include <BulletCollision/CollisionDispatch/btCollisionWorldImporter.h>
#include <BulletCollision/NarrowPhaseCollision/btRaycastCallback.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>


namespace nox { namespace logic { namespace physics
{

BulletSimulation3d::BulletSimulation3d(IContext* logicContext) :
	listener("BulletSimulation3d"),
	timeManager(nullptr),
	isCollisionBroadcastEnabled(false)
{
	this->logicContext = logicContext;

	this->listener.setup(this, this->logicContext->getEventBroadcaster(), event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(nox::logic::world::ActorCreated::ID);
	this->listener.addEventTypeToListenFor(nox::logic::graphics::DebugRenderingEnabled::ID);

	this->broadphase = std::make_shared<btDbvtBroadphase>();

	this->collisionConfiguration = std::make_shared<btDefaultCollisionConfiguration>();
	this->dispatcher = std::make_shared<btCollisionDispatcher>(collisionConfiguration.get());
	btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher.get());

	this->solver = std::make_shared<btSequentialImpulseConstraintSolver>();

	this->dynamicsWorld = std::make_shared <btDiscreteDynamicsWorld>(this->dispatcher.get(), this->broadphase.get(), this->solver.get(), this->collisionConfiguration.get());
	this->dynamicsWorld->setGravity(btVector3(0.0f, -9.81f, 0.0f));

	this->dynamicsWorld->setInternalTickCallback(myTickCallback);
	this->dynamicsWorld->setWorldUserInfo(this);
}

BulletSimulation3d::~BulletSimulation3d()
{
	//TODO Fix bleeding, shape in list is not deleted
	for (const auto& trdActorData : this->actorMap)
	{
		btRigidBody* mainBody = trdActorData.second.mainBody;
		
		btCollisionShape* shape = mainBody->getCollisionShape();
		btMotionState* motionState = mainBody->getMotionState();

		delete shape;
		delete motionState;
		this->dynamicsWorld->removeRigidBody(mainBody);
		delete mainBody;
	}
}

void BulletSimulation3d::clearForces(const actor::Identifier& actorId)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->clearForces();
}

void BulletSimulation3d::applyCentralForce(const actor::Identifier& actorId, const glm::vec3& force)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->applyCentralForce(btVector3(force.x, force.y, force.z));
}

void BulletSimulation3d::applyForce(const actor::Identifier& actorId, const glm::vec3& force, const glm::vec3& relativePosition)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->applyForce(btVector3(force.x, force.y, force.z), btVector3(relativePosition.x, relativePosition.y, relativePosition.z));
}

void BulletSimulation3d::applyCentralImpulse(const actor::Identifier& actorId, const glm::vec3& impulse)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->applyCentralImpulse(btVector3(impulse.x, impulse.y, impulse.z));
}


void BulletSimulation3d::applyImpulse(const actor::Identifier& actorId, const glm::vec3& impulse, glm::vec3& relativePosition)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->applyImpulse(btVector3(impulse.x, impulse.y, impulse.z), btVector3(relativePosition.x, relativePosition.y, relativePosition.z));
}

void BulletSimulation3d::applyDamping(const actor::Identifier& actorId, float timeStep)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}

	this->actorMap[actorId].mainBody->applyDamping(timeStep);

}

void BulletSimulation3d::applyGravity(const actor::Identifier& actorId)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}
	this->actorMap[actorId].mainBody->applyGravity();
}

void BulletSimulation3d::applyTorque(const actor::Identifier& actorId, const glm::vec3&  torque)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}
	this->actorMap[actorId].mainBody->applyTorque(btVector3(torque.x, torque.y, torque.z));

}

void BulletSimulation3d::applyTorqueImpulse(const actor::Identifier& actorId, const glm::vec3& torque)
{
	if (this->actorMap[actorId].mainBody->isActive() == false)
	{
		this->actorMap[actorId].mainBody->activate();
	}
	this->actorMap[actorId].mainBody->applyTorqueImpulse(btVector3(torque.x, torque.y, torque.z));

}

glm::vec3 BulletSimulation3d::getLinearVelocity(const actor::Identifier& actorId)
{
	btVector3 vel = this->actorMap[actorId].mainBody->getLinearVelocity();
	
	glm::vec3 ret(vel.getX(), vel.getY(), vel.getZ());

	return ret;
}

void BulletSimulation3d::setGraphicsAssetManager(nox::app::graphics::GraphicsAssetManager3d* assetManager)
{
	this->assetManager = assetManager;
}

void BulletSimulation3d::setPosition(const actor::Identifier& actorId, const glm::vec3 position)
{
	auto actor = this->actorMap[actorId];

	btTransform transform = actor.mainBody->getCenterOfMassTransform();

	transform.setOrigin(btVector3(position.x, position.y, position.z));
	actor.mainBody->setCenterOfMassTransform(transform);
}

nox::app::graphics::GraphicsAssetManager3d* BulletSimulation3d::getAssetManager()
{
	return this->assetManager;
}

glm::vec3 BulletSimulation3d::getAngularVelocity(const actor::Identifier& actorId)
{
	btVector3 vel = this->actorMap[actorId].mainBody->getAngularVelocity();

	return glm::vec3(vel.getX(), vel.getY(), vel.getZ());
}

glm::vec3 BulletSimulation3d::getTotalForces(const actor::Identifier& actorId)
{
	btVector3 force = this->actorMap[actorId].mainBody->getTotalForce();

	return glm::vec3(force.getX(), force.getY(), force.getZ());
}

glm::vec3 BulletSimulation3d::getTotalTorque(const actor::Identifier& actorId)
{
	btVector3 torq = this->actorMap[actorId].mainBody->getTotalTorque();

	return glm::vec3(torq.getX(), torq.getY(), torq.getZ());
}

float BulletSimulation3d::getAngularDamping(const actor::Identifier& actorId)
{
	float angDamp = this->actorMap[actorId].mainBody->getAngularDamping();
	
	return angDamp;
}

glm::vec3 BulletSimulation3d::getAngularFactor(const actor::Identifier& actorId)
{
	btVector3 temp = this->actorMap[actorId].mainBody->getAngularFactor();
	return (btVector3_to_Vec3(temp));
}

float BulletSimulation3d::getAngularSleepingThreshold(const actor::Identifier& actorId)
{
	float temp = this->actorMap[actorId].mainBody->getAngularSleepingThreshold();
	return temp;
}

glm::vec3 BulletSimulation3d::getAnisotropicFriction(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getAnisotropicFriction());
}

glm::vec3 BulletSimulation3d::getCenterOfMassPosition(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getCenterOfMassPosition());
}

float BulletSimulation3d::getFriction(const actor::Identifier& actorId)
{
	return float(this->actorMap[actorId].mainBody->getFriction());
}

btTransform BulletSimulation3d::getCenterOfMassTransform(const actor::Identifier& actorId)
{
	return btTransform(this->actorMap[actorId].mainBody->getCenterOfMassTransform());
}

int BulletSimulation3d::getCollisionFlags(const actor::Identifier& actorId)
{
	return int(this->actorMap[actorId].mainBody->getCollisionFlags());
}

glm::vec3 BulletSimulation3d::getGravity(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getGravity());
}

glm::vec3 BulletSimulation3d::getInterpolationAngularVelocity(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getInterpolationAngularVelocity());
}

glm::vec3 BulletSimulation3d::getInterpolationLinearVelocity(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getInterpolationLinearVelocity());
}

btTransform BulletSimulation3d::getInterpolationWorldTransform(const actor::Identifier& actorId)
{
	return btTransform(this->actorMap[actorId].mainBody->getInterpolationWorldTransform());
}

float BulletSimulation3d::getLinearDamping(const actor::Identifier& actorId)
{
	return float(this->actorMap[actorId].mainBody->getLinearDamping());
}

glm::vec3 BulletSimulation3d::getLinearFactor(const actor::Identifier& actorId)
{
	return btVector3_to_Vec3(this->actorMap[actorId].mainBody->getLinearFactor());
}

glm::quat BulletSimulation3d::getOrientation(const actor::Identifier& actorId)
{ 
	return btQuaternion_to_quat(this->actorMap[actorId].mainBody->getOrientation());
}

float BulletSimulation3d::getRestitution(const actor::Identifier& actorId)
{
	return float(this->actorMap[actorId].mainBody->getRestitution());
}

float BulletSimulation3d::getRollingFriction(const actor::Identifier& actorId)
{
	return float(this->actorMap[actorId].mainBody->getRollingFriction());
}

void BulletSimulation3d::setAngularVelocity(const actor::Identifier& actorId, glm::vec3 angularVelocity)
{
	auto actor = this->actorMap[actorId];

	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	
	actor.mainBody->setAngularVelocity(btVector3(angularVelocity.x, angularVelocity.y, angularVelocity.z));
}

void BulletSimulation3d::setLinearVelocity(const actor::Identifier& actorId, glm::vec3 linearVelocity)
{
	auto actor = this->actorMap[actorId];

	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->clearForces();
	actor.mainBody->setLinearVelocity(btVector3(linearVelocity.x, linearVelocity.y, linearVelocity.z));
}

void BulletSimulation3d::setTransform(const actor::Identifier& actorId, glm::vec3 position, glm::quat rotation, glm::vec3 scale)
{
	auto actor = this->actorMap[actorId];
	
	btTransform transform = actor.mainBody->getCenterOfMassTransform();

	transform.setRotation(btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
	transform.setOrigin(btVector3(position.x, position.y, position.z));
	actor.mainBody->setCenterOfMassTransform(transform);
	actor.mainBody->getCollisionShape()->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
}

void BulletSimulation3d::setAngularFactor(const actor::Identifier& actorId, float factor)
{
	auto actor = this->actorMap[actorId];

	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}

	actor.mainBody->setAngularFactor(factor);
}

void BulletSimulation3d::setSleepingThresholds(const actor::Identifier& actorId, glm::vec2 angAndLin)
{
	auto actor = this->actorMap[actorId];

	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setSleepingThresholds(angAndLin.x, angAndLin.y);
}

void BulletSimulation3d::setAnisotropicFriction(const actor::Identifier& actorId, const glm::vec3& anisotropicFriction)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setAnisotropicFriction(vec3_to_btVector(anisotropicFriction));
}

void BulletSimulation3d::setCenterOfMassTransform(const actor::Identifier& actorId, const btTransform& xform)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setCenterOfMassTransform(xform);
}

void BulletSimulation3d::setCollisionFlags(const actor::Identifier& actorId, int flags)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setCollisionFlags(flags);
}

void BulletSimulation3d::setDamping(const actor::Identifier& actorId, float linearDamping, float angularDamping)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setDamping(linearDamping, angularDamping);
}

void BulletSimulation3d::setFriction(const actor::Identifier& actorId, float friction)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setFriction(friction);
}

void BulletSimulation3d::setGravity(const actor::Identifier& actorId, const glm::vec3& acceleration)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setGravity(vec3_to_btVector(acceleration));
}

void BulletSimulation3d::setIgnoreCollisionCheck(const actor::Identifier& actorId, bool ignore)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setIgnoreCollisionCheck(actor.mainBody, ignore);
}

void BulletSimulation3d::setInterpolationAngularVelocity(const actor::Identifier& actorId, glm::vec3& angularVelocity)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setInterpolationAngularVelocity(vec3_to_btVector(angularVelocity));
}

void BulletSimulation3d::setInterpolationLinearVelocity(const actor::Identifier& actorId, glm::vec3& linearVelocity)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setInterpolationLinearVelocity(vec3_to_btVector(linearVelocity));
}

void BulletSimulation3d::setInterpolationWorldTransform(const actor::Identifier& actorId, btTransform& transform)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setInterpolationWorldTransform(transform);
}

void BulletSimulation3d::setLinearFactor(const actor::Identifier& actorId, glm::vec3& linearFactor)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setLinearFactor(vec3_to_btVector(linearFactor));
}

void BulletSimulation3d::setRollingFriction(const actor::Identifier& actorId, float friction)
{
	auto actor = this->actorMap[actorId];
	actor.mainBody->setFriction(friction);
}

void BulletSimulation3d::startSimulation(const actor::Identifier& actorId)
{
	this->dynamicsWorld->addRigidBody(this->actorMap[actorId].mainBody);
}

void BulletSimulation3d::stopSimulation(const actor::Identifier& actorId)
{
	this->dynamicsWorld->removeRigidBody(this->actorMap[actorId].mainBody);
}

void BulletSimulation3d::setRestitution(const actor::Identifier& actorId, float restitution)
{
	auto actor = this->actorMap[actorId];
	if (!actor.mainBody->isActive())
	{
		actor.mainBody->activate();
	}
	actor.mainBody->setRestitution(restitution);
}

bool BulletSimulation3d::isInWorld(const actor::Identifier& actorId)
{
	return this->actorMap[actorId].mainBody->isInWorld();
}

bool BulletSimulation3d::isKinematicObject(const actor::Identifier& actorId)
{
	return this->actorMap[actorId].mainBody->isKinematicObject();
}

bool BulletSimulation3d::isStaticObject(const actor::Identifier& actorId)
{
	return this->actorMap[actorId].mainBody->isStaticObject();
}
bool BulletSimulation3d::isStaticOrKinematicObject(const actor::Identifier& actorId)
{
	return this->actorMap[actorId].mainBody->isStaticOrKinematicObject();
}
float BulletSimulation3d::distanceAboveGround(const actor::Identifier& actorId)
{
	auto actor = this->actorMap[actorId];
	
	btVector3 max, min, actorPos;
	actor.mainBody->getAabb(min, max);
	actorPos = actor.mainBody->getCenterOfMassPosition();

	btVector3 btFrom(actorPos.getX(), min.getY() + 0.05f, actorPos.getZ());
	btVector3 btTo(actorPos.getX(), -1000, actorPos.getZ());
	btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);

	this->dynamicsWorld->rayTest(btFrom, btTo, res);

	if (res.hasHit())
	{
		btVector3 hitPoint = res.m_hitPointWorld;
		float dist = btFrom.distance(hitPoint);
		return dist;
	}

	return -10000;
}


bool BulletSimulation3d::createActorBody(actor::Actor* actor, const  BulletBodyDefinition& bodyDefinition)
{
	if (this->actorMap.find(actor->getId()) != this->actorMap.end())
	{
		this->log.error().raw("Failed creating actor body. Actor already added to physics.");
		return false;
	}

	//if (this->actorMap[actor->getId()].mainBody != nullptr)
	//{
	//	this->log.error().raw("Failed creating actor body. Actor already added to physics.");
	//	return;
	//}

	//TODO: implement map
	btCollisionShape* shape;
	ActorMotionState3d* motionState;
	btRigidBody* rigidBody;
	btScalar mass = 0;
	btVector3 inertia(0, 0, 0);

	if (timeManager != nullptr)
	{
		motionState = new ActorMotionState3d(actor, this->timeManager->getWorldLogger());
	}
	else
	{
		motionState = new ActorMotionState3d(actor, nullptr);
	}
	
	shape = createShape(bodyDefinition, actor);
	

	if (shape != nullptr)
	{
		shape->setLocalScaling(btVector3(bodyDefinition.scale.x, bodyDefinition.scale.y, bodyDefinition.scale.z));

		switch (bodyDefinition.bodyType)
		{
		case PhysicalBodyType::STATIC:

			break;
		case PhysicalBodyType::DYNAMIC:
			mass = bodyDefinition.mass;
			if (mass > 0)
			{
				shape->calculateLocalInertia(mass, inertia);
				
			}
			
			break;
		case PhysicalBodyType::KINEMATIC:
			

			break;

		default:
			//TODO add logger output here
			break;
		}
	}
	else
	{
		return false;
	}

	btRigidBody::btRigidBodyConstructionInfo fallRigidBodyCI(mass, motionState, shape, inertia);
	rigidBody = new btRigidBody(fallRigidBodyCI);
	rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btCollisionObject::CF_CUSTOM_MATERIAL_CALLBACK);
	rigidBody->setDamping(bodyDefinition.linearDamping, bodyDefinition.angularDamping);
	rigidBody->setAngularFactor(bodyDefinition.angularFactor);
	rigidBody->setSleepingThresholds(bodyDefinition.sleepingThresholds.x, bodyDefinition.sleepingThresholds.y);

	rigidBody->setLinearVelocity(btVector3(bodyDefinition.linearVelocity.x, bodyDefinition.linearVelocity.y, bodyDefinition.linearVelocity.z));
	rigidBody->setAngularVelocity(btVector3(bodyDefinition.angularVelocity.x, bodyDefinition.angularVelocity.y, bodyDefinition.angularVelocity.z));
	rigidBody->setFriction(bodyDefinition.friction);
	rigidBody->setRestitution(bodyDefinition.restitution);

	int size = this->actorMap.size();
	TrdActorData& actorData = this->actorMap[actor->getId()];

	actorData.actor = actor;
	actorData.mainBody = rigidBody;
	this->dynamicsWorld->addRigidBody(rigidBody);

	rigidBody->setUserPointer(&actorData);

	if (bodyDefinition.bodyType == PhysicalBodyType::KINEMATIC)
	{
		rigidBody->setMassProps(btScalar(0.0f), btVector3(0.0f, 0.0f, 0.0f));
		rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);

	}
	return true;
}

/**
*  http://www.cplusplus.com/forum/general/135193/
*  http://antongerdelan.net/opengl/raycasting.html
*/

actor::Actor *BulletSimulation3d::findActorRayIntersectingPoint(glm::vec2 screenPosition, float zDistance, glm::vec2 windoWSize, glm::mat4x4& viewProjectionMatrix)
{
	glm::vec2 mouse = glm::vec2(screenPosition.x, windoWSize.y -screenPosition.y);

	glm::vec4 RayStart = glm::vec4(2 * mouse.x / windoWSize.x - 1, 2 * mouse.y / windoWSize.y - 1, -1, 1);
	glm::vec4 RayEnd = RayStart; RayEnd.z = 0;
	RayStart = inverse(viewProjectionMatrix)*RayStart; RayStart /= RayStart.w;
	RayEnd = inverse(viewProjectionMatrix)*RayEnd; RayEnd /= RayEnd.w;
	glm::vec4 RayDirection = zDistance * normalize(RayEnd - RayStart);

	btVector3 btRayStart = btVector3(RayStart.x, RayStart.y, RayStart.z);
	btVector3 btRayDirection = btVector3(RayDirection.x, RayDirection.y, RayDirection.z);

	btCollisionWorld::ClosestRayResultCallback firstRayHit(btRayStart, btRayDirection);
	firstRayHit.m_flags |= btTriangleRaycastCallback::kF_KeepUnflippedNormal;
	firstRayHit.m_flags |= btTriangleRaycastCallback::kF_UseSubSimplexConvexCastRaytest;

	dynamicsWorld->rayTest(btRayStart, btRayDirection, firstRayHit);

	if (firstRayHit.hasHit())
	{
		return static_cast<TrdActorData*>(firstRayHit.m_collisionObject->getUserPointer())->actor;
	}

	return nullptr;
}



btCollisionShape* BulletSimulation3d::createShape(const BulletBodyDefinition &bodyDefinition, actor::Actor* actor)
{
	btCollisionShape * shape = nullptr;

	switch (bodyDefinition.shape.type)
	{
	case ShapeType3d::NONE:
		shape = new btEmptyShape();
		break;
	case ShapeType3d::SPHERE:
		shape = new btSphereShape(bodyDefinition.shape.circleRadius);
		break;
	case ShapeType3d::BOX:
		shape = new btBoxShape(btVector3(bodyDefinition.shape.size.x / 2, bodyDefinition.shape.size.y / 2, bodyDefinition.shape.size.z / 2));
		break;
	case ShapeType3d::CYLINDER:
		shape = new btCylinderShape(btVector3(bodyDefinition.shape.size.x / 2, bodyDefinition.shape.size.y / 2, bodyDefinition.shape.size.z / 2));
		break;
	case ShapeType3d::CAPSULE:
		shape = new btCapsuleShape(bodyDefinition.shape.circleRadius, bodyDefinition.shape.height);

		break;
	case ShapeType3d::CONE:
		shape = new btConeShape(bodyDefinition.shape.circleRadius, bodyDefinition.shape.height);
		break;
	case ShapeType3d::PLANE:
		//ONLY static
		shape = new btStaticPlaneShape(btVector3(bodyDefinition.shape.planeOffset.x, bodyDefinition.shape.planeOffset.y, bodyDefinition.shape.planeOffset.z), bodyDefinition.shape.planeConstant);
		
		break;
	case ShapeType3d::CONVEXHULLSHAPE: {
		
		auto reuseIt = this->reuseShapeList.find(bodyDefinition.name);
		
		if (reuseIt != this->reuseShapeList.end())
		{
			//TODO the last parameter in btUniformScalingShape is the scaling scalar and need to 
			//be recieved from the bodydefinition, but there allready is a scale variable there vec3.
			btUniformScalingShape* scaledShape = new btUniformScalingShape(static_cast<btConvexShape*>(reuseIt->second), 1);
			shape = scaledShape;
		}
		else
		{
			auto actorGraphics = actor->findComponent<nox::logic::graphics::ActorGraphics3d>();
			auto meshDatas = actorGraphics->getMeshdatas();
			auto vertecies = meshDatas->getVertices();
			auto indecies = meshDatas->getIndices();

			btTriangleMesh * trimesh = new btTriangleMesh();

			for (unsigned int i = 0; i < indecies.size() / 3; i++)
			{
				int index0 = indecies[i * 3];
				int index1 = indecies[i * 3 + 1];
				int index2 = indecies[i * 3 + 2];

				btVector3 vertex0(vertecies[index0].x, vertecies[index0].y, vertecies[index0].z);
				btVector3 vertex1(vertecies[index1].x, vertecies[index1].y, vertecies[index1].z);
				btVector3 vertex2(vertecies[index2].x, vertecies[index2].y, vertecies[index2].z);

				trimesh->addTriangle(vertex0, vertex1, vertex2);
			}

			btConvexShape * convex = new btConvexTriangleMeshShape(trimesh);

			// For optimization:
			if (bodyDefinition.quick_cd)
			{
				btShapeHull* hull = new btShapeHull(convex);
				btScalar margin = convex->getMargin();
				hull->buildHull(margin);
				btConvexHullShape* simplifiedConvexShape = new btConvexHullShape(*hull->getVertexPointer(), hull->numVertices());

				shape = simplifiedConvexShape;
				
			}
			else
			{
				shape = convex;
			}
			if (bodyDefinition.name != "")
			{
				reuseShapeList[bodyDefinition.name] = static_cast<btCollisionShape*>(shape);
			}
		}
	}
	break;

	case ShapeType3d::CONCAVEHULLSHAPE:{
		//ONLY static
		auto actorGraphics = actor->findComponent<nox::logic::graphics::ActorGraphics3d>();
		auto meshDatas = actorGraphics->getMeshdatas();
		auto vertecies = meshDatas->getVertices();
		auto indecies = meshDatas->getIndices();

		btTriangleMesh * trimesh = new btTriangleMesh();

		for (unsigned int i = 0; i < indecies.size() / 3; i++)
		{
			int index0 = indecies[i * 3];
			int index1 = indecies[i * 3 + 1];
			int index2 = indecies[i * 3 + 2];

			btVector3 vertex0(vertecies[index0].x, vertecies[index0].y, vertecies[index0].z);
			btVector3 vertex1(vertecies[index1].x, vertecies[index1].y, vertecies[index1].z);
			btVector3 vertex2(vertecies[index2].x, vertecies[index2].y, vertecies[index2].z);

			trimesh->addTriangle(vertex0, vertex1, vertex2);
		}

		btConcaveShape * concave = new btBvhTriangleMeshShape(trimesh, true);
		shape = concave;

	}
	break;
	
	case ShapeType3d::COMPOUNDSHAPE:
	{
		auto tempShape = new btCompoundShape(bodyDefinition.shape.enableDynamicAabbTree);
		for (unsigned int i = 0; i < bodyDefinition.shapes.size(); i++)
		{
			btTransform loadTrans;
			loadTrans.setIdentity();
			loadTrans.setOrigin(btVector3(bodyDefinition.shapes[i].position.x, bodyDefinition.shapes[i].position.y, bodyDefinition.shapes[i].position.z));
			tempShape->addChildShape(loadTrans, createShape(bodyDefinition.shapes[i], actor));
		}
		shape = tempShape;
	}
		break;
	
	case ShapeType3d::GIMPACTTRIANGLEMESHSHAPE:
	{
		auto vertecies = bodyDefinition.meshDatas->getVertices();
		auto indecies = bodyDefinition.meshDatas->getIndices();
		//btTriangleIndexVertexArray(int numTriangles, int* triangleIndexBase, int triangleIndexStride, int numVertices, btScalar* vertexBase, int vertexStride);

		//btTriangleIndexVertexArray* indexVertexArrays = new btTriangleIndexVertexArray((int)indecies.size() / 3,
		//	(int*)&indecies[0],
		//	(int)3 * sizeof(GLuint),
		//	(int) vertecies.size()*3, (btScalar*)&vertecies[0].x, (int) sizeof(vertecies[0].x)*3);

		btTriangleMesh * trimesh = new btTriangleMesh();

		for (unsigned int i = 0; i < indecies.size() / 3; i++)
		{
			int index0 = indecies[i * 3];
			int index1 = indecies[i * 3 + 1];
			int index2 = indecies[i * 3 + 2];

			btVector3 vertex0(vertecies[index0].x, vertecies[index0].y, vertecies[index0].z);
			btVector3 vertex1(vertecies[index1].x, vertecies[index1].y, vertecies[index1].z);
			btVector3 vertex2(vertecies[index2].x, vertecies[index2].y, vertecies[index2].z);

			trimesh->addTriangle(vertex0, vertex1, vertex2);
		}
		btGImpactMeshShape * mesh = new btGImpactMeshShape(trimesh);

		mesh->setLocalScaling(btVector3(1.0, 1.0, 1.0));
		mesh->setMargin(btScalar(0.1));
		mesh->updateBound();
		shape = mesh;

	}
		break;
	case ShapeType3d::HEIGHTFIELD:
		//@PARAM: int heightStickWidth, int heightStickLength, const void *heightfieldData, btScalar heightScale, btScalar minHeight, btScalar maxHeight, int upAxis, PHY_ScalarType heightDataType, bool flipQuadEdges 
		//shape = new btHeightfieldTerrainShape();
		break;
	case ShapeType3d::SCALEDBVHTRIANGLE:
		//ONLY static
		//The btScaledBvhTriangleMeshShape allows to instance a scaled version of an existing btBvhTriangleMeshShape.
		//Note that each btBvhTriangleMeshShape still can have its own local scaling, independent from this btScaledBvhTriangleMeshShape 'localScaling'

		//shape = new btScaledBvhTriangleMeshShape();

		break;
	default:
		//TODO add logger output here!!
		break;
	}

	return shape;
}

void BulletSimulation3d::setLogger(app::log::Logger logger)
{
	this->log = std::move(logger);
	this->log.setName("BulletSimulation3d");
}



void BulletSimulation3d::onUpdate(const Duration& deltaTime)
{
	if (this->debugRenderer->getIsDebugModeEnabled())
	{
		this->dynamicsWorld->debugDrawWorld();
	}

	this->dynamicsWorld->stepSimulation(util::durationToSeconds<float32>(deltaTime));
}

void BulletSimulation3d::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->getType() == nox::logic::graphics::DebugRenderingEnabled::ID)
	{
		auto debugRenderingEnabled = static_cast<nox::logic::graphics::DebugRenderingEnabled*>(event.get());
		this->debugRenderer->enableDebugRenderer(debugRenderingEnabled->isTrue());
		(debugRenderingEnabled->isTrue()) ? (debugRenderer->setDebugMode(debugRenderer->DBG_DrawWireframe)) : (debugRenderer->setDebugMode(debugRenderer->DBG_NoDebug));
	}
}


void BulletSimulation3d::setDebugRenderer(BulletDebugDraw3d* debugRenderer)
{
	debugRenderer->setDebugMode(debugRenderer->DBG_MAX_DEBUG_DRAW_MODE);
	this->dynamicsWorld->setDebugDrawer(debugRenderer);
	this->debugRenderer = debugRenderer;
}

void BulletSimulation3d::setTimeManager(world::timemanipulation::ITimeManager3d* timeManager)
{
	this->timeManager = timeManager;
}

bool BulletSimulation3d::getDebugRenderingEnabled()
{
	return this->debugRenderer->getIsDebugModeEnabled();
}


void BulletSimulation3d::notifyFutureCollisions(ActorPhysics3d* actorPhys)
{
	if (actorPhys->collisionUpdated == false)
	{
		actorPhys->collisionUpdated = true;

		for (int i = 0; i < actorPhys->collisions.size(); i++)
		{
			if (actorPhys->collisions[i].frameNumber <= actorPhys->collisions[i].affected->startSimulatingAt || actorPhys->collisions[i].affected->startSimulatingAt == -1)
			{
				actorPhys->collisions[i].affected->startSimulatingAt = actorPhys->collisions[i].frameNumber;	
				actorPhys->collisions[i].affected->conflictedActor = actorPhys->getOwner();
			}
			notifyFutureCollisions(actorPhys->collisions[i].affected);
		}
		actorPhys->collisions.clear();
	}
}

void BulletSimulation3d::updateTimeManipulationConflicts(ActorPhysics3d* bodyOne, ActorPhysics3d* bodyTwo)
{
	if (this->timeManager != nullptr && this->timeManager->isRewindEnabled() == false)
	{
		if (bodyOne->isStaticObject() == false && bodyTwo->isStaticObject() == false &&
			(bodyOne->isSimulated() == true || bodyTwo->isSimulated() == true))	
		{

			if (bodyOne->isSimulated() == false)
			{
				auto function = this->timeManager->getConflictSolver()->getConflictSolverFunction(bodyOne->getOwner()->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED);

				if (function != nullptr)
				{
					nox::logic::world::timemanipulation::ConflictParameters cp;
					cp.conflictType = nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED;
					cp.conflictingActor = bodyTwo->getOwner();
					cp.affectedActor = bodyOne->getOwner();
					function(cp);					
				}
				bodyOne->conflictedActor = bodyTwo->getOwner();
				bodyOne->setSimulatedFlag(true);
				bodyOne->startSimulatingAt = timeManager->getWorldLogger()->getCurrentFrame();
				notifyFutureCollisions(bodyTwo);
				notifyFutureCollisions(bodyOne);
			}

			if (bodyTwo->isSimulated() == false)
			{
				auto function = this->timeManager->getConflictSolver()->getConflictSolverFunction(bodyTwo->getOwner()->getId(), nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED);

				if (function != nullptr)
				{
					nox::logic::world::timemanipulation::ConflictParameters cp;
					cp.conflictType = nox::logic::world::timemanipulation::ConflictType::ACTOR_INTERRUPTED;
					cp.conflictingActor = bodyOne->getOwner();
					cp.affectedActor = bodyTwo->getOwner();
					function(cp);
				}
				bodyTwo->conflictedActor = bodyOne->getOwner();
				bodyTwo->startSimulatingAt = timeManager->getWorldLogger()->getCurrentFrame();
				bodyTwo->setSimulatedFlag(true);
				notifyFutureCollisions(bodyOne);
				notifyFutureCollisions(bodyTwo);
			}
		}
	}
}

void BulletSimulation3d::myTickCallback(btDynamicsWorld *world, btScalar timeStep) {
	
	assert(world);

	BulletSimulation3d * bulletSim = static_cast<BulletSimulation3d*>(world->getWorldUserInfo());
	CollisionPairs currentTickCollisionPairs;

	btDispatcher* dispatcher = world->getDispatcher();
	for (int i = 0; i < dispatcher->getNumManifolds(); i++)
	{
		const btPersistentManifold * manifold = dispatcher->getManifoldByIndexInternal(i);
		assert(manifold);
		if (!manifold)
		{
			continue;
		}
		const btRigidBody* bodyOne = static_cast<const btRigidBody*>(manifold->getBody0());
		const btRigidBody* bodyTwo = static_cast<const btRigidBody*>(manifold->getBody1());

		const bool swapped = bodyOne > bodyTwo;

		const btRigidBody* sortedBodyA = swapped ? bodyOne : bodyTwo;
		const btRigidBody* sortedBodyB = swapped ? bodyTwo : bodyOne;

		const CollisionPair pair = std::make_pair(sortedBodyA, sortedBodyB);
		currentTickCollisionPairs.insert(pair);
		if (bulletSim->oldTickCollisionPairs.find(pair) == bulletSim->oldTickCollisionPairs.end())
		{
			//new contact
			bulletSim->sendCollisionPairAddEvent(manifold, bodyOne, bodyTwo);
		}

		auto actorPhysicsA = static_cast<TrdActorData*>(sortedBodyA->getUserPointer())->actor->findComponent<ActorPhysics3d>();
		auto actorPhysicsB = static_cast<TrdActorData*>(sortedBodyB->getUserPointer())->actor->findComponent<ActorPhysics3d>();

		bulletSim->updateTimeManipulationConflicts(actorPhysicsA, actorPhysicsB);
	}
	CollisionPairs removedCollisionPairs;

	std::set_difference(bulletSim->oldTickCollisionPairs.begin(), bulletSim->oldTickCollisionPairs.end(), currentTickCollisionPairs.begin(), 
		currentTickCollisionPairs.end(), std::inserter(removedCollisionPairs, removedCollisionPairs.begin()));

	for (CollisionPairs::const_iterator it = removedCollisionPairs.begin(),  end = removedCollisionPairs.end(); it != end; ++it)
	{
		const btRigidBody* bodyOne = it->first;
		const btRigidBody* bodyTwo = it->second;

		bulletSim->sendCollisionPairRemoveEvent(bodyOne, bodyTwo);
	}
	bulletSim->oldTickCollisionPairs = currentTickCollisionPairs;
}


void BulletSimulation3d::enableCollisionEvent(bool enabled)
{
	this->isCollisionBroadcastEnabled = enabled;
}


void BulletSimulation3d::sendCollisionPairAddEvent(const btPersistentManifold* manifold, const btRigidBody* bodyOne, const btRigidBody* bodyTwo)
{
	
	if (this->timeManager->isRewindEnabled()) 
	{
		return;
	}
	
	TrdActorData *actorOne = static_cast<TrdActorData*>(bodyOne->getUserPointer());
	TrdActorData *actorTwo = static_cast<TrdActorData*>(bodyTwo->getUserPointer());

	auto physOne = actorOne->actor->findComponent<nox::logic::physics::ActorPhysics3d>();
	auto physTwo = actorTwo->actor->findComponent<nox::logic::physics::ActorPhysics3d>();

	if (this->timeManager != nullptr && physOne->isStaticObject() == false && physTwo->isStaticObject() == false)
	{

		if (physOne->isSimulated())
		{
			nox::logic::world::timemanipulation::SavedCollision  collision;
			collision.frameNumber = this->timeManager->getWorldLogger()->getCurrentFrame();
			collision.affected = physOne;
			physTwo->collisions.push_back(collision);
		}

		if (physTwo->isSimulated())
		{
			nox::logic::world::timemanipulation::SavedCollision  collision;
			collision.frameNumber = this->timeManager->getWorldLogger()->getCurrentFrame();
			collision.affected = physTwo;
			physOne->collisions.push_back(collision);
		}
	}

	nox::logic::actor::Identifier idOne = actorOne->actor->getId();
	nox::logic::actor::Identifier idTwo = actorTwo->actor->getId();
		
	if (!idOne.isValid() || !idTwo.isValid())
	{
		// something is colliding with a non-actor.  we currently don't send events for that
		return;
	}

	// this pair of colliding objects is new.  send a collision-begun event
	std::vector<glm::vec3> collisionPoints;
	glm::vec3 sumNormalForce;
	glm::vec3 sumFrictionForce;

	for (int pointIdx = 0; pointIdx < manifold->getNumContacts(); ++pointIdx)
	{
		btManifoldPoint const & point = manifold->getContactPoint(pointIdx);

		collisionPoints.push_back(btVector3_to_Vec3(point.getPositionWorldOnB()));

		sumNormalForce += btVector3_to_Vec3(point.m_combinedRestitution * point.m_normalWorldOnB);
		sumFrictionForce += btVector3_to_Vec3(point.m_combinedFriction * point.m_lateralFrictionDir1);
	}
		
	
	if (this->isCollisionBroadcastEnabled == false)
	{
		auto bulletIt = this->collisionCallbackListStart.find(idOne);

		if (bulletIt != this->collisionCallbackListStart.end())
		{
			CollisionParameters param;
			param.actorOne = static_cast<TrdActorData*>(actorOne->mainBody->getUserPointer());
			param.actorTwo = static_cast<TrdActorData*>(actorTwo->mainBody->getUserPointer());
			param.collisionPoints = collisionPoints;
			param.sumFrictionForce = sumFrictionForce;
			param.sumNormalForce = sumNormalForce;

			//run all the different collisions for the first actor:
			for (unsigned int i = 0; i < bulletIt->second.size(); i++)
			{
				bulletIt->second[i](param);
			}
		}
	} 
	else 
	{
		// send the event for the game
		const auto collision = std::make_shared<nox::logic::actor::ActorCollision3d>(nullptr, actorOne->actor, actorTwo->actor, collisionPoints, sumNormalForce, sumFrictionForce, true);
		this->logicContext->getEventBroadcaster()->queueEvent(collision);
	}
	
}


void BulletSimulation3d::sendCollisionPairRemoveEvent(const btRigidBody* bodyOne, const btRigidBody* bodyTwo)
{
	//if (bodyOne->getUserPointer() || bodyTwo->getUserPointer())
	//{
	//	// figure out which actor is the trigger
	//	btRigidBody const * triggerBody, *otherBody;

	//	if (bodyOne->getUserPointer())
	//	{
	//		triggerBody = bodyOne;
	//		otherBody = bodyTwo;
	//	}
	//	else
	//	{
	//		otherBody = bodyOne;
	//		triggerBody = bodyTwo;
	//	}

	//	// send the trigger event.
	//	int const triggerId = *static_cast<int*>(triggerBody->getUserPointer());
	///*	shared_ptr<EvtData_PhysTrigger_Leave> pEvent(GCC_NEW EvtData_PhysTrigger_Leave(triggerId, FindActorID(otherBody)));
	//	IEventManager::Get()->VQueueEvent(pEvent);*/
	//}
	//else
	//{
		TrdActorData *actorOne = static_cast<TrdActorData*>(bodyOne->getUserPointer());
		TrdActorData *actorTwo = static_cast<TrdActorData*>(bodyTwo->getUserPointer());

		nox::logic::actor::Identifier idOne = actorOne->actor->getId();
		nox::logic::actor::Identifier idTwo = actorTwo->actor->getId();

		if (!idOne.isValid()  || !idTwo.isValid() )
		{
			// collision is ending between some object(s) that don't have actors.  we don't send events for that.
			return;
		}
		
		if (this->isCollisionBroadcastEnabled == false)
		{
			// send the event for the game
			auto bulletIt = this->collisionCallbackListStop.find(idOne);
			if (bulletIt != this->collisionCallbackListStop.end()){

				CollisionParameters param;
				param.actorOne = static_cast<TrdActorData*>(actorOne->mainBody->getUserPointer());
				param.actorTwo = static_cast<TrdActorData*>(actorTwo->mainBody->getUserPointer());

				//run all the different collisions for the first actor:
				for (unsigned int i = 0; i < bulletIt->second.size(); i++)
				{
					bulletIt->second[i](param);
				}
			}
		}
		else
		{
			const auto collision = std::make_shared<nox::logic::actor::ActorCollision3d>(nullptr, actorOne->actor, actorTwo->actor, false);
			this->logicContext->getEventBroadcaster()->queueEvent(collision);
		}
}

int BulletSimulation3d::setCallbackForActor(const nox::logic::actor::Identifier &id, std::function<void(const CollisionParameters&)> collisionCallback, CALLBACKTYPE type)
{
	if (type == CALLBACKTYPE::STARTCALLBACK)
	{
		const int cdid = collisionCallbackListStart[id].size();
		collisionCallbackListStart[id][cdid] = collisionCallback;
		return cdid;
	}
	else if (type == CALLBACKTYPE::STOPCALLBACK)
	{
		const int cdid = collisionCallbackListStop[id].size();
		collisionCallbackListStop[id][cdid] = collisionCallback;
		return cdid;
	}

	return -1;
}

bool BulletSimulation3d::removeCallbackForActor(const nox::logic::actor::Identifier id, CALLBACKTYPE type, int callbackNr)
{

	if (type == CALLBACKTYPE::STARTCALLBACK)
	{
		auto actorIt = this->collisionCallbackListStart.find(id);
		if (actorIt != this->collisionCallbackListStart.end())
		{
			for (auto collisionIt = actorIt->second.begin(); collisionIt != actorIt->second.end();)
			{
				if (collisionIt->first == callbackNr)
				{
					collisionIt = actorIt->second.erase(collisionIt);
					return true;
				}
				else
				{
					collisionIt++;
				}
			}

		}
	}
	else if (type == CALLBACKTYPE::STOPCALLBACK)
	{
		auto actorIt = this->collisionCallbackListStop.find(id);
		if (actorIt != this->collisionCallbackListStop.end())
		{
			for (auto collisionIt = actorIt->second.begin(); collisionIt != actorIt->second.end();)
			{
				if (collisionIt->first == callbackNr)
				{
					collisionIt = actorIt->second.erase(collisionIt);
					return true;
				}
				else
				{
					collisionIt++;
				}
			}
		}
	}

	return false;
}


glm::mat3x3 BulletSimulation3d::btMatrix3x3ToGlm(const btMatrix3x3& btmat)		 // TODO mooove!
{
	return glm::mat3x3(
		btmat[0][0], btmat[0][1], btmat[0][2],
		btmat[1][0], btmat[1][1], btmat[1][2],
		btmat[2][0], btmat[2][1], btmat[2][2]);
}

glm::vec3 BulletSimulation3d::btVector3_to_Vec3(btVector3 const & btvec)
{
	return glm::vec3(btvec.x(), btvec.y(), btvec.z());
}

btVector3 BulletSimulation3d::vec3_to_btVector(glm::vec3 const & glmVec3)
{
	return btVector3(glmVec3.x, glmVec3.y, glmVec3.z);
}
//TODO
btQuaternion BulletSimulation3d::quat_to_btQuaternion(glm::quat const & glmQuat)
{
	return btQuaternion();
}

glm::quat BulletSimulation3d::btQuaternion_to_quat(btQuaternion const & btQuat)
{
	btVector3 btaxis = btQuat.getAxis();
	glm::vec3 axis = glm::vec3(btaxis.getX(), btaxis.getY(), btaxis.getZ());
	float angle = btQuat.getAngle();
	glm::quat rotation(glm::angleAxis(angle, axis));
	rotation = glm::normalize(rotation);

	return rotation;
}

void BulletSimulation3d::serializePhysicsBody(const nox::logic::actor::Identifier &id, nox::logic::world::timemanipulation::PhysicalProperties* physicsState)
{
	auto actorPhysicsBody = this->actorMap[id].mainBody;

	physicsState->invInertiaTensorWorld = btMatrix3x3ToGlm(actorPhysicsBody->getInvInertiaTensorWorld());
	physicsState->linearVelocity = btVector3_to_Vec3(actorPhysicsBody->getLinearVelocity());
	physicsState->angularVelocity = btVector3_to_Vec3(actorPhysicsBody->getAngularVelocity());	
}

} } }
