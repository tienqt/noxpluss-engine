/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/physics/actor/ActorPhysics3d.h>

#include <nox/logic/IContext.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/graphics/actor/ActorGraphics3d.h>
#include <nox/logic/graphics/event/ActorGraphicsCreated3d.h>
#include <nox/util/json_utils.h>
#include <nox/logic/graphics/event/SceneNodeEdited.h>
#include <nox/logic/world/Manager.h>
#include <nox/logic/world/timeManipulation/time_manipulation_utils.h>

namespace nox { namespace logic { namespace physics
{

const ActorPhysics3d::IdType ActorPhysics3d::NAME = "Physics3d";

const ActorPhysics3d::IdType& ActorPhysics3d::getName() const
{
	return ActorPhysics3d::NAME;
}

ActorPhysics3d::ActorPhysics3d() : 
	listener(this->getName()),
	simulationLock(true),
	simulated(true),
	startSimulatingAt(-1),
	oldStartSimulatingAt(-1),
	collisionUpdated(false),
	conflictedActor(nullptr),
	conflictResolved(false)
{
}

bool ActorPhysics3d::initialize(const Json::Value& componentJsonObject) 
{
	this->listener.setup(this, this->getLogicContext()->getEventBroadcaster(), event::ListenerManager::StartListening_t());
	this->listener.addEventTypeToListenFor(nox::logic::actor::ActorGraphicsCreated3d::ID);
	this->listener.addEventTypeToListenFor(nox::logic::graphics::SceneNodeEdited::ID);
	this->listener.addEventTypeToListenFor(nox::logic::world::timemanipulation::PlayModeChange::ID);

	physicalProperties.name = componentJsonObject.get("name", "").asString();
	std::string shape = componentJsonObject.get("shape", "").asString();
	std::string type = componentJsonObject.get("type", "").asString();
	physicalProperties.shape.height = componentJsonObject.get("height", 0).asFloat();
	physicalProperties.shape.circleRadius = componentJsonObject.get("radius", 0).asFloat();
	physicalProperties.shape.size = util::parseJsonVec(componentJsonObject["size"], glm::vec3(0.0f, 0.0f, 0.0f));
	physicalProperties.position = util::parseJsonVec(componentJsonObject["position"], glm::vec3(0.0f, 0.0f, 0.0f));	// relative to actor
	physicalProperties.quick_cd = componentJsonObject.get("quick_cd", false).asBool();
	physicalProperties.friction = componentJsonObject.get("friction", 0.0f).asFloat();
	physicalProperties.shape.type = getShapeType(shape);

	if (physicalProperties.shape.type == ShapeType3d::COMPOUNDSHAPE)
	{
		
		const Json::Value& shapeArrayValue = componentJsonObject["compound"];

		for (const Json::Value& shapeValue : shapeArrayValue)
		{
			BulletBodyDefinition temp;

			temp.shape.type = getShapeType(shapeValue.get("shape","").asString());
			physicalProperties.name = componentJsonObject.get("name", "").asString();		//TEST if correct
			temp.shape.height = shapeValue.get("height", 0).asFloat();
			temp.shape.circleRadius = shapeValue.get("radius", 0).asFloat();
			temp.shape.size = util::parseJsonVec(shapeValue["size"], glm::vec3(0.0f, 0.0f, 0.0f));
			temp.position = util::parseJsonVec(shapeValue["position"], glm::vec3(0.0f, 0.0f, 0.0f));
			temp.quick_cd = shapeValue.get("quick_cd", false).asBool();
			physicalProperties.shapes.push_back(std::move(temp));

		}
	}

	if (std::strcmp(type.c_str(), "dynamic") == false)
	{
		this->physicalProperties.bodyType = PhysicalBodyType::DYNAMIC;
		physicalProperties.mass = componentJsonObject.get("mass", 1.0f).asFloat();
		physicalProperties.linearDamping = componentJsonObject.get("linearDamping", 0).asFloat();
		physicalProperties.angularDamping = componentJsonObject.get("angularDamping", 0).asFloat();
		physicalProperties.angularFactor = componentJsonObject.get("angularFactor", 0.0f).asFloat();
		physicalProperties.linearVelocity = util::parseJsonVec(componentJsonObject["linearVelocity"], glm::vec3(0, 0, 0));
		physicalProperties.angularVelocity = util::parseJsonVec(componentJsonObject["angularVelocity"], glm::vec3(0, 0, 0));
		physicalProperties.restitution = componentJsonObject.get("restitution", 0.0f).asFloat();

		physicalProperties.sleepingThresholds = util::parseJsonVec(componentJsonObject["sleepingThresholds"], glm::vec2(0.1, 0.1));		// If 0 and 0 the object will never rest
	}
	else if (std::strcmp(type.c_str(), "kinematic") == false)
	{
		// TODO
	}
	else if (std::strcmp(type.c_str(), "static") == false)
	{
		this->physicalProperties.bodyType = PhysicalBodyType::STATIC;
	}

	this->physics = this->getLogicContext()->getPhysics3d();
	
	return true;
}

ShapeType3d ActorPhysics3d::getShapeType(std::string shape)
{
	if (std::strcmp(shape.c_str(), "plane") == false)
	{
		return ShapeType3d::PLANE;
		
		//TODO: read in offset for plane shape
	}
	else if (std::strcmp(shape.c_str(), "sphere") == false)
	{
		return ShapeType3d::SPHERE;
	}
	else if (std::strcmp(shape.c_str(), "cone") == false)
	{
		return ShapeType3d::CONE;
	}
	else if (std::strcmp(shape.c_str(), "box") == false)
	{
		return ShapeType3d::BOX;
	}
	else if (std::strcmp(shape.c_str(), "capsule") == false)
	{
		return ShapeType3d::CAPSULE;
	}
	else if (std::strcmp(shape.c_str(), "cylinder") == false)
	{
		return ShapeType3d::CYLINDER;
	}
	else if (std::strcmp(shape.c_str(), "convexhull") == false)
	{
		return ShapeType3d::CONVEXHULLSHAPE;
	}
	else if (std::strcmp(shape.c_str(), "concavehull") == false)
	{
		return ShapeType3d::CONCAVEHULLSHAPE;
	}
	else if (std::strcmp(shape.c_str(), "compound") == false)
	{
		return ShapeType3d::COMPOUNDSHAPE;
	}else if (std::strcmp(shape.c_str(), "impactmesh") == false)
	{
		return ShapeType3d::GIMPACTTRIANGLEMESHSHAPE;
	}
	return ShapeType3d::NONE;
}
	 
void ActorPhysics3d::onCreate()
{
	auto transformComp = this->getOwner()->findComponent<actor::Transform3d>();
	

	if (transformComp != nullptr)
	{
		this->physicalProperties.position = transformComp->getPosition();
		this->physicalProperties.scale = transformComp->getScale();
	}
	physics->createActorBody(this->getOwner(), this->physicalProperties);
}

void ActorPhysics3d::createActorPhysicsShape()
{
	for (std::vector<BulletBodyDefinition>::iterator it = this->physicalProperties.shapes.begin(); it != this->physicalProperties.shapes.end(); ++it)
	{
		if (it->shape.type == ShapeType3d::CONVEXHULLSHAPE
			|| it->shape.type == ShapeType3d::CONCAVEHULLSHAPE || it->shape.type == ShapeType3d::GIMPACTTRIANGLEMESHSHAPE)
		{
			it->meshDatas = this->getOwner()->findComponent<nox::logic::graphics::ActorGraphics3d>()->getMeshdatas();

		}
	}
	if (this->physicalProperties.shape.type == ShapeType3d::CONVEXHULLSHAPE
		|| this->physicalProperties.shape.type == ShapeType3d::CONCAVEHULLSHAPE || this->physicalProperties.shape.type == ShapeType3d::GIMPACTTRIANGLEMESHSHAPE)

	{
		this->physicalProperties.meshDatas = this->getOwner()->findComponent<nox::logic::graphics::ActorGraphics3d>()->getMeshdatas();
	}


	///////
	auto transformComp = this->getOwner()->findComponent<actor::Transform3d>();
	if (transformComp != nullptr)
	{
		this->physicalProperties.position = transformComp->getPosition();
		this->physicalProperties.scale = transformComp->getScale();
	}
	///////
	physics->createActorBody(this->getOwner(), physicalProperties);
}
	 
	 
void ActorPhysics3d::onDestroy()
{

}
	 
	 
void ActorPhysics3d::onComponentEvent(const std::shared_ptr<event::Event>& event)
{

}
	 
	 
void ActorPhysics3d::onUpdate(const Duration& deltaTime)
{

}
	 
void ActorPhysics3d::onActivate()
{

}
	 
void ActorPhysics3d::onDeactivate()
{

}

void  ActorPhysics3d::serializePhysicsBody(world::timemanipulation::ActorPhysicsState* actorState)
{
	this->physics->serializePhysicsBody(this->getOwner()->getId(), &actorState->physicalProperties);
}

void ActorPhysics3d::serialize(Json::Value& componentObject)
{
}

void ActorPhysics3d::clearForces()
{
	this->physics->clearForces(this->getOwner()->getId());
}

void ActorPhysics3d::applyCentralForce(const glm::vec3& force)
{
	this->physics->applyCentralForce(this->getOwner()->getId(), force);
}

void ActorPhysics3d::applyCentralImpulse(const glm::vec3& impulse)
{
	this->physics->applyCentralImpulse(this->getOwner()->getId(), impulse);
}

void ActorPhysics3d::applyImpulse(const glm::vec3& impulse, glm::vec3& relativePosition)
{
	this->physics->applyImpulse(this->getOwner()->getId(), impulse, relativePosition);
}

void ActorPhysics3d::applyDamping(float timeStep)
{
	this->physics->applyDamping(this->getOwner()->getId(), timeStep);
}

void ActorPhysics3d::applyGravity()
{
	this->physics->applyGravity(this->getOwner()->getId() );
}

void ActorPhysics3d::applyTorqueImpulse(const glm::vec3& torque)
{
	this->physics->applyTorqueImpulse(this->getOwner()->getId(), torque);
}

void ActorPhysics3d::applyForce(glm::vec3& force, glm::vec3& relPos)
{
	
	this->physics->applyForce(this->getOwner()->getId(), force, relPos);
}

void ActorPhysics3d::applyTorque(glm::vec3 torque)
{
	this->physics->applyTorque(this->getOwner()->getId(), torque);
}

void ActorPhysics3d::setTransform(glm::vec3 position, glm::quat rotation, glm::vec3 scale)
{
	this->physics->setTransform(this->getOwner()->getId(), position, rotation, scale);
}

void ActorPhysics3d::setPosition(const glm::vec3 position)
{
	this->physics->setPosition(this->getOwner()->getId(), position);
}

void ActorPhysics3d::setLinearVelocity(glm::vec3 velocity)
{
	this->physics->setLinearVelocity(this->getOwner()->getId(), velocity);
}

void ActorPhysics3d::setAngularVelocity(glm::vec3 velocity)
{
	this->physics->setAngularVelocity(this->getOwner()->getId(), velocity);
}

void ActorPhysics3d::setAngularFactor(float factor)
{
	this->physics->setAngularFactor(this->getOwner()->getId(), factor);
}

void ActorPhysics3d::setSleepingThresholds(glm::vec2 angAndLin)
{
	this->physics->setSleepingThresholds(this->getOwner()->getId(), angAndLin);
}

void ActorPhysics3d::setAnisotropicFriction(const glm::vec3& anisotropicFriction)
{
	this->physics->setAnisotropicFriction(this->getOwner()->getId(), anisotropicFriction);
}

void ActorPhysics3d::setCenterOfMassTransform(const btTransform& xform)
{
	this->physics->setCenterOfMassTransform(this->getOwner()->getId(), xform);
}

void ActorPhysics3d::setCollisionFlags(int flags)
{
	this->physics->setCollisionFlags(this->getOwner()->getId(), flags);
}

void ActorPhysics3d::setDamping(float linearDamping, float angularDamping)
{
	this->physics->setDamping(this->getOwner()->getId(), linearDamping, angularDamping);
}

void ActorPhysics3d::setFriction(float friction)
{
	this->physics->setFriction(this->getOwner()->getId(), friction);
}

void ActorPhysics3d::setGravity(const glm::vec3& acceleration)
{
	this->physics->setGravity(this->getOwner()->getId(), acceleration);
}

void ActorPhysics3d::setIgnoreCollisionCheck(bool ignore)
{
	this->physics->setIgnoreCollisionCheck(this->getOwner()->getId(), ignore);
}

void ActorPhysics3d::setInterpolationAngularVelocity(glm::vec3& angularVelocity)
{
	this->physics->setInterpolationAngularVelocity(this->getOwner()->getId(), angularVelocity);
}

void ActorPhysics3d::setInterpolationLinearVelocity(glm::vec3& linearVelocity)
{
	this->physics->setInterpolationLinearVelocity(this->getOwner()->getId(), linearVelocity);
}

void ActorPhysics3d::setInterpolationWorldTransform(btTransform& transform)
{
	this->physics->setInterpolationWorldTransform(this->getOwner()->getId(), transform);
}

void ActorPhysics3d::setLinearFactor(glm::vec3& linearFactor)
{
	this->physics->setLinearFactor(this->getOwner()->getId(), linearFactor);
}

void ActorPhysics3d::setRollingFriction(float friction)
{
	this->physics->setRollingFriction(this->getOwner()->getId(), friction);
}

void ActorPhysics3d::setRestitution(float restitution)
{
	this->physics->setRestitution(this->getOwner()->getId(), restitution);
}

void ActorPhysics3d::startSimulation()
{
	this->physics->startSimulation(getOwner()->getId());
}

void ActorPhysics3d::stopSimulation()
{
	this->physics->stopSimulation(getOwner()->getId());
}

float ActorPhysics3d::getDistanceAboveGround()
{
	return this->physics->distanceAboveGround(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getLinearVelocity()
{
	return this->physics->getLinearVelocity(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getAngularVelocity()
{
	return this->physics->getAngularVelocity(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getTotalForces()
{
	return this->physics->getTotalForces(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getTotalTorque()
{
	return this->physics->getTotalTorque(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getAngularFactor()
{
	return this->physics->getAngularFactor(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getAnisotropicFriction()
{
	return this->physics->getAnisotropicFriction(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getCenterOfMassPosition()
{
	return this->physics->getCenterOfMassPosition(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getGravity()
{
	return this->physics->getGravity(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getInterpolationAngularVelocity()
{
	return this->physics->getInterpolationAngularVelocity(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getInterpolationLinearVelocity()
{
	return this->physics->getInterpolationLinearVelocity(this->getOwner()->getId());
}

glm::vec3 ActorPhysics3d::getLinearFactor()
{
	return this->physics->getLinearFactor(this->getOwner()->getId());
}

btTransform ActorPhysics3d::getCenterOfMassTransform()
{
	return this->physics->getCenterOfMassTransform(this->getOwner()->getId());
}

btTransform ActorPhysics3d::getInterpolationWorldTransform()
{
	return this->physics->getInterpolationWorldTransform(this->getOwner()->getId());
}

glm::quat ActorPhysics3d::getOrientation()
{
	return this->physics->getOrientation(this->getOwner()->getId());
}

int ActorPhysics3d::getCollisionFlags()
{
	return this->physics->getCollisionFlags(this->getOwner()->getId());
}

float ActorPhysics3d::getAngularSleepingThreshold()
{
	return this->physics->getAngularSleepingThreshold(this->getOwner()->getId());
}

float ActorPhysics3d::getAngularDamping()
{
	return this->physics->getAngularDamping(this->getOwner()->getId());
}

float ActorPhysics3d::getFriction()
{
	return this->physics->getFriction(this->getOwner()->getId());
}

float ActorPhysics3d::getLinearDamping()
{
	return this->physics->getLinearDamping(this->getOwner()->getId());
}

float ActorPhysics3d::getRestitution()
{
	return this->physics->getRestitution(this->getOwner()->getId());
}

float ActorPhysics3d::getRollingFriction()
{
	return this->physics->getRollingFriction(this->getOwner()->getId());
}

bool ActorPhysics3d::isInWorld()
{
	return this->physics->isInWorld(this->getOwner()->getId());
}

bool ActorPhysics3d::isKinematicObject()
{
	return this->physics->isKinematicObject(this->getOwner()->getId());
}

bool ActorPhysics3d::isStaticObject()
{
	return this->physics->isStaticObject(this->getOwner()->getId());
}

bool ActorPhysics3d::isStaticOrKinematicObject()
{
	return this->physics->isStaticOrKinematicObject(this->getOwner()->getId());
}

bool ActorPhysics3d::isSimulated()
{
	return this->simulated;
}

void ActorPhysics3d::setSimulatedFlag(bool simulated)
{
	this->simulated = simulated;
}

void ActorPhysics3d::onEvent(const std::shared_ptr<nox::logic::event::Event>& event)
{
	if (event->isType(nox::logic::graphics::SceneNodeEdited::ID))
	{
		auto sceneNodeEdited = static_cast<nox::logic::graphics::SceneNodeEdited*>(event.get());

		if (sceneNodeEdited->getActorId() == this->getOwner()->getId())
		{
			if (simulationLock == false && sceneNodeEdited->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_ADD)
			{
				//this->getLogicContext()->getWorldManager()->findActor(sceneNodeEdited->getActorId);

				startSimulation();
				simulationLock = true;
			}
			else if (simulationLock == true && sceneNodeEdited->getEditAction() == nox::logic::graphics::SceneNodeEdited::Action::TIME_REMOVE)
			{
				stopSimulation();
				simulationLock = false;
			}
		}
	}
	else if (event->isType(nox::logic::world::timemanipulation::PlayModeChange::ID))
	{
		auto playMode = static_cast<nox::logic::world::timemanipulation::PlayModeChange*>(event.get());

		if (playMode->isPlay())
		{
			
		}
		else
		{
			simulated = false;
			collisionUpdated = false;
			conflictResolved = false;
			//collisions.clear();
		}
	}

	/*if (event->isType(nox::logic::actor::TrdActorGraphicsCreated::ID))
	{
		auto actor = static_cast<nox::logic::actor::TrdActorGraphicsCreated*>(event.get())->getActor();

		if (actor->getId() == this->getOwner()->getId())
		{
			for (std::vector<BulletBodyDefinition>::iterator it = this->physicalProperties.shapes.begin(); it != this->physicalProperties.shapes.end(); ++it)
			{
				if (it->shape.type == ShapeType3d::CONVEXHULLSHAPE
					|| it->shape.type == ShapeType3d::CONCAVEHULLSHAPE)
				{
					it->meshDatas = this->getOwner()->findComponent<nox::logic::graphics::TrdActorGraphics>()->getMeshdatas();

				}
			}
			if (this->physicalProperties.shape.type == ShapeType3d::CONVEXHULLSHAPE
				|| this->physicalProperties.shape.type == ShapeType3d::CONCAVEHULLSHAPE)
			{
				this->physicalProperties.meshDatas = this->getOwner()->findComponent<nox::logic::graphics::TrdActorGraphics>()->getMeshdatas();
			}

			physics->createActorBody(this->getOwner(), physicalProperties);
		}
	}*/
}


} } }
