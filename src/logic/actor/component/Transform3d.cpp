/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <nox/logic/IContext.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/util/json_utils.h>

#include <glm/gtx/transform.hpp>

namespace nox { namespace logic { namespace actor
{

const Component::IdType Transform3d::NAME = "Transform3d";

Transform3d::~Transform3d() = default;

bool Transform3d::initialize(const Json::Value& componentJsonObject)
{
	this->position = util::parseJsonVec(componentJsonObject["position"], glm::vec3(0.0f, 0.0f, 0.0f));
	this->scale = util::parseJsonVec(componentJsonObject["scale"], glm::vec3(1.0f, 1.0f, 1.0f));
	this->rotation = util::parseJsonVec(componentJsonObject["rotation"], glm::vec3(0.0f, 0.0f, 0.0f));

	return true;
}

void Transform3d::onCreate()
{
	this->broadcastTransformChange();
}

const glm::mat4 Transform3d::getTransformMatrix() const
{
	glm::mat4 positionMatrix = glm::translate(this->position);
	glm::mat4 scaleMatrix = glm::scale(this->scale);
	
	glm::mat4 rotationMatrix = glm::mat4_cast(qRotation);

//	glm::mat4 rotationMatrix = glm::rotate(this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) *
//		glm::rotate(this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
//		glm::rotate(this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

	return positionMatrix * rotationMatrix * scaleMatrix;
}

const Component::IdType& Transform3d::getName() const
{
	return NAME;
}

void Transform3d::setPosition(const glm::vec3& position)
{
	this->position = position;

	this->broadcastTransformChange();
}

void Transform3d::setScale(const glm::vec3& scale)
{
	this->scale = scale;

	this->broadcastTransformChange();
}

void Transform3d::setRotation(const glm::vec3& rotation)
{
	this->rotation = rotation;

	this->broadcastTransformChange();
}

void Transform3d::setRotation(const glm::quat& rotation)
{
	this->qRotation = rotation;

	this->broadcastTransformChange();
}

void Transform3d::setTransform(const glm::vec3& position, const glm::vec3& rotation, const glm::vec3& scale)
{
	this->position = position;
	this->scale = scale;
	this->rotation = rotation;

	this->broadcastTransformChange();
}

void Transform3d::broadcastTransformChange()
{
	assert(this->getOwner() != nullptr);

	const auto transformEvent = std::make_shared<TransformChange3d>(this->getOwner(), this->position, this->rotation,this->qRotation, this->scale);

	this->getOwner()->broadCastComponentEvent(transformEvent);
	this->getLogicContext()->getEventBroadcaster()->queueEvent(transformEvent);
}

void Transform3d::serialize(Json::Value& componentObject)
{
	//componentObject["position"] = util::writeJsonVec(this->position);
	componentObject["scale"] = util::writeJsonVec(this->scale);
	componentObject["rotation"] = util::writeJsonVec(this->rotation);
}

} } }
