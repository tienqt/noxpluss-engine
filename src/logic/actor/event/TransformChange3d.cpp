

#include <nox/logic/actor/event/TransformChange3d.h>
#include <glm/gtx/transform.hpp>

namespace nox {	namespace logic { namespace actor
{

	const event::Event::IdType TransformChange3d::ID = "nox.actor.Transform3d";

	TransformChange3d::TransformChange3d(Actor* transformedActor, const glm::vec3& position, const glm::vec3 rotation, const glm::quat qRotation, const glm::vec3& scale) :
		Event(ID, transformedActor),
		position(position),
		scale(scale),
		rotation(rotation),
		qRotation(qRotation)
	{
				
	}

			

	TransformChange3d::~TransformChange3d() = default;

	const glm::vec3& TransformChange3d::getPosition() const
	{
		return this->position;
	}

	const glm::vec3& TransformChange3d::getScale() const
	{
		return this->scale;
	}

	const glm::vec3& TransformChange3d::getRotation() const
	{
		return this->rotation;
	}

	const glm::quat TransformChange3d::getQRotation() const
	{
		return this->qRotation;
	}

	glm::mat4 TransformChange3d::getTransformMatrix() const
	{
		glm::mat4 positionMatrix = glm::translate(this->position);
		glm::mat4 scaleMatrix = glm::scale(this->scale);
		glm::mat4 rotationMatrix = glm::rotate(this->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::rotate(this->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f)) *
			glm::rotate(this->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

		return positionMatrix * rotationMatrix * scaleMatrix;
	}

}}}
