

#include "nox/logic/actor/event/ActorCollision3d.h"

namespace nox { namespace logic { namespace actor{

const event::Event::IdType ActorCollision3d::ID = "nox.actor.collision";

ActorCollision3d::ActorCollision3d(Actor* transformedActor, Actor* actorOne, Actor* actorTwo, std::vector<glm::vec3> collisionPoints, glm::vec3 sumNormalForce, glm::vec3 sumFrictionForce, bool started) :
	Event(ID, transformedActor),
	firstActor(actorOne),
	secondActor(actorTwo),
	collisionPoints(collisionPoints),
	sumFrictionForce(sumFrictionForce),
	sumNormalForce(sumNormalForce),
	isStarting(started)
	{
	}

ActorCollision3d::ActorCollision3d(Actor* transformedActor, Actor* actorOne, Actor* actorTwo, bool started) :
	Event(ID, transformedActor),
	firstActor(actorOne),
	secondActor(actorTwo),
	isStarting(started)
	{
	}

ActorCollision3d::~ActorCollision3d() = default;


bool ActorCollision3d::isStartingCollision()
{
	return this->isStarting;
}
std::vector<glm::vec3> ActorCollision3d::getCollisionPoints()
{
	return this->collisionPoints;
}
glm::vec3 ActorCollision3d::getSumNormalForce()
{
	return this->sumNormalForce;
}
glm::vec3 ActorCollision3d::getSumFrictionForce()
{
	return this->sumFrictionForce;
}
Actor* ActorCollision3d::getActorOne()
{
	return this->firstActor;
}
Actor* ActorCollision3d::getActorTwo()
{
	return this->secondActor;
}
}}}

