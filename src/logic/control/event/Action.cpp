/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <glm/geometric.hpp>
#include <nox/logic/control/event/Action.h>

namespace nox { namespace logic { namespace control
{

const Action::IdType Action::ID = "nox.control.actor_control";

Action::Action(actor::Actor* controledActor, const std::string& controlActionName, const glm::vec3& controlVector):
	actor::Event(ID, controledActor),
	controlActionName(controlActionName),
	controlVector(controlVector)
{
}

Action::Action(actor::Actor* controledActor, const std::string& controlActionName, float controlStrength):
	actor::Event(ID, controledActor),
	controlActionName(controlActionName),
	controlVector(0.0f, controlStrength, 0.0f)
{
}

Action::Action(actor::Actor* controledActor, const std::string& controlActionName, bool on):
	actor::Event(ID, controledActor),
	controlActionName(controlActionName)
{
	if (on == true)
	{
		this->controlVector = glm::vec3(0.0f, 1.0f, 0.0f);
	}
	else
	{
		this->controlVector = glm::vec3(0.0f, 0.0f, 0.0f);
	}
}

Action::Action(actor::Actor* controledActor, const std::string& controlName):
	actor::Event(ID, controledActor),
	controlActionName(controlName)
{
}

const std::string& Action::getControlName() const
{
	return this->controlActionName;
}

const glm::vec2& Action::getControlVector() const
{
	return glm::vec2(this->controlVector);
}

const glm::vec3& Action::getControlVector3d() const
{
	return this->controlVector;
}

bool Action::isSwitchedOn() const
{
	if (this->controlVector.x != 0.0f || this->controlVector.y != 0.0f)
	{
		return true;
	}

	return false;
}

float Action::getControlStrength() const
{
	return this->controlVector.y;
}

} } }
