/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/control/actor/ActorRotationControl3d.h>
#include <nox/logic/actor/Actor.h>


#include <cassert>

namespace nox { namespace logic { namespace control
{

	const ActorRotationControl3d::IdType ActorRotationControl3d::NAME = "3dRotationControl";

	const ActorRotationControl3d::IdType& ActorRotationControl3d::getName() const
{
	return NAME;
}

	bool ActorRotationControl3d::initialize(const Json::Value& componentJson)
{
	if (this->ActorControl::initialize(componentJson) == false)
	{
		return false;
	}

	this->rotationForce = componentJson.get("rotationForce", 1.0f).asFloat();
	this->rotationSpeed = componentJson.get("rotationSpeed", 1.0f).asFloat();

	this->currentRotationDirection = glm::vec3(0.0f, 0.0f, 0.0f);

	return true;
}

	void ActorRotationControl3d::serialize(Json::Value& componentJson)
{
	componentJson["rotationForce"] = this->rotationForce;
	componentJson["rotationSpeed"] = this->rotationSpeed;
}

void ActorRotationControl3d::onCreate()
{
	this->ActorControl::onCreate();

	this->actorPhysics = this->getOwner()->findComponent<physics::ActorPhysics3d>();
	assert(this->actorPhysics != nullptr);
}

bool ActorRotationControl3d::handleControl(const std::shared_ptr<Action>& controlEvent)
{
	if (controlEvent->getControlName() == "rotate")
	{
		this->currentRotationDirection = controlEvent->getControlVector3d();

		return true;
	}

	return false;
}

void ActorRotationControl3d::onUpdate(const nox::Duration& deltaTime)
{
	this->ActorControl::onUpdate(deltaTime);


	if (this->currentRotationDirection.x != 0.0f || this->currentRotationDirection.y != 0.0f || this->currentRotationDirection.z != 0.0f)
	{
		this->actorPhysics->setAngularVelocity(this->currentRotationDirection * this->rotationSpeed);
		//this->actorPhysics->applyTorque(this->currentRotationDirection * this->rotationForce);
	}
	else
	{
		this->actorPhysics->setAngularVelocity(glm::vec3(0, 0, 0));
	}
}

} } }
