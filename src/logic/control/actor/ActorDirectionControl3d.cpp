/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <nox/logic/control/actor/ActorDirectionControl3d.h>
#include <nox/logic/actor/Actor.h>
#include <nox/logic/actor/component/Transform3d.h>
#include <nox/logic/actor/event/TransformChange3d.h>
#include <nox/logic/physics/actor/ActorPhysics3d.h>
#include <nox/app/graphics/Camera3d.h>

#include <glm/common.hpp>

namespace nox { namespace logic { namespace control
{

	const ActorDirectionControl3d::IdType ActorDirectionControl3d::NAME = "3dDirectionControl";

const ActorDirectionControl3d::IdType& ActorDirectionControl3d::getName() const
{
	return NAME;
}

bool ActorDirectionControl3d::initialize(const Json::Value& componentJson)
{
	if (this->ActorControl::initialize(componentJson) == false)
	{
		return false;
	}

	camera = nullptr;

	this->movementForce = componentJson.get("movementForce", 1).asFloat();
	this->movementSpeed = componentJson.get("movementSpeed", 1).asFloat();
	this->relativeToCamera = componentJson.get("relativeToCamera", true).asBool();
	this->relativeToRotation = componentJson.get("relativeToRotation", false).asBool();

	return true;
}

void ActorDirectionControl3d::serialize(Json::Value& componentJson)
{
}

void ActorDirectionControl3d::onCreate()
{
	this->ActorControl::onCreate();

	this->actorTransform = this->getOwner()->findComponent<actor::Transform3d>();
	assert(this->actorTransform != nullptr);

	this->actorPhysics = this->getOwner()->findComponent<physics::ActorPhysics3d>();
	assert(this->actorPhysics != nullptr);
}

bool ActorDirectionControl3d::handleControl(const std::shared_ptr<Action>& controlEvent)
{
	if (controlEvent->getControlName() == "move")
	{
		glm::vec3 tmpControlInput = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 viewDirection = this->camera->getViewDirection();

		glm::vec3 xDirectionVector = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 yDirectionVector = glm::vec3(0.0f, 0.0f, 0.0f);
		glm::vec3 zDirectionVector = glm::vec3(0.0f, 0.0f, 0.0f);

		if (relativeToRotation)
		{
			tmpControlInput = glm::vec3(glm::mat4_cast(actorTransform->getQuaternion()) * glm::vec4(controlEvent->getControlVector3d(), 1));
		}
		else
		{
			tmpControlInput = controlEvent->getControlVector3d();
		}

		if (relativeToCamera)
		{
			if (tmpControlInput.x > 0)
			{
				xDirectionVector = glm::cross(viewDirection, glm::vec3(0.0f, 1.0f, 0.0f));
			}
			else if (tmpControlInput.x < 0)
			{
				xDirectionVector = -glm::cross(viewDirection, glm::vec3(0.0f, 1.0f, 0.0f));
			}

			if (std::abs(tmpControlInput.y) > 0)
			{
				yDirectionVector.y = tmpControlInput.y;
			}

			if (tmpControlInput.z > 0)
			{
				zDirectionVector = -viewDirection;
			}
			else if (tmpControlInput.z < 0)
			{
				zDirectionVector = viewDirection;
			}
		}

		zDirectionVector.y = 0;
		this->controlInput = xDirectionVector + yDirectionVector + zDirectionVector;

		return true;
	}
	return false;
}

void ActorDirectionControl3d::onUpdate(const nox::Duration& deltaTime)
{
	this->ActorControl::onUpdate(deltaTime);
	
	//float aboveGnd = this->actorPhysics->getDistanceAboveGround();
	{
		//if (aboveGnd < 0.5 && (controlInput.x != 0 || controlInput.y != 0 || controlInput.z != 0))
		{
			this->actorPhysics->setLinearVelocity(this->controlInput * this->movementSpeed);
			//this->actorPhysics->applyForce(this->controlInput * this->movementForce);
		}
		//else if (aboveGnd < 0.5)
		{
		//	float currentY = this->actorPhysics->getLinearVelocity().y;
		//	this->actorPhysics->setLinearVelocity(glm::vec3(0, currentY, 0));
		}
		//else
		{

		}
	}
}

void ActorDirectionControl3d::onComponentEvent(const std::shared_ptr<event::Event>& event)
{
	if (this->relativeToRotation == true && event->isType(actor::TransformChange3d::ID))
	{
		const auto transformEvent = static_cast<actor::TransformChange3d*>(event.get());
		const auto rotation = transformEvent->getRotation();

		this->currentMovementDirection = glm::vec3(glm::mat4_cast(transformEvent->getQRotation()) * glm::vec4(this->controlInput, 1.0f));
	}
}

} } }
